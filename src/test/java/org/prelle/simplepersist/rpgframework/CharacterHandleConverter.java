package org.prelle.simplepersist.rpgframework;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.TextNode;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

//-------------------------------------------------------------------
public class CharacterHandleConverter implements XMLElementConverter<CharacterHandle> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, CharacterHandle value) throws Exception {
		TextNode name = new TextNode("charref",null, value.getOwner().getId()+"/"+value.getName());
		node.getChildren().add(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public CharacterHandle read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
//		logger.debug("read "+ev);
		
		XMLEvent charRefEv = evRd.nextEvent();
		if (charRefEv.isCharacters())
			charRefEv = evRd.nextEvent();
		if (!charRefEv.isStartElement() || !charRefEv.asStartElement().getName().getLocalPart().equals("charref"))
			throw new SerializationException("Expected <charref> event after "+ev+" but got "+charRefEv);
		
		// Next event should be a character event
		XMLEvent charEv = evRd.nextEvent();
		if (!charEv.isCharacters())
			throw new SerializationException("Expected CDATA event after "+charRefEv+" but got "+charEv);
		
		
		String id = charEv.asCharacters().getData();
//		logger.debug("Search "+id);
		
		StringTokenizer tok = new StringTokenizer(id, "/");
		if (tok.countTokens()<2)
			throw new SerializationException("Invalid player ID: '"+id+"'.  Must be in format player-id/char-id");
		String playerID = tok.nextToken();
		String charID   = tok.nextToken();
		
		// Read over charref end element
		evRd.nextEvent();
		
		Player player = SimpleDatabase.getPlayer(playerID);
		if (player==null)
			throw new NoSuchElementException("Unknown player '"+playerID+"'");
		
		for (CharacterHandle handle : SimpleDatabase.getCharacters(player)) {
//			logger.debug("    ID = "+handle);
			if (handle.getName().equals(charID)) {
//				logger.debug("found character "+handle);
				return handle;
			}
		}

		throw new NoSuchElementException("No character named '"+charID+"' found for player "+player.getId());
	}
	
}