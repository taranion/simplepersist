/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
public interface Player{
	
	public String getId();
	public String getName();
	
}
