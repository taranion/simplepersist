/**
 * 
 */
package org.prelle.simplepersist.rpgframework;


/**
 * @author prelle
 *
 */
public interface Adventure extends Comparable<Adventure> {

	//----------------------------------------------------------------
	public void setTitle(String title);

	//----------------------------------------------------------------
	public String getTitle();

	//----------------------------------------------------------------
	public String getDescription();

	//----------------------------------------------------------------
	public void setDescription(String descr);

	//----------------------------------------------------------------
	public RoleplayingSystem getRules();

	//----------------------------------------------------------------
	public void setCover(byte[] image);

	//----------------------------------------------------------------
	public byte[] getCover();

	//----------------------------------------------------------------
	public String getId();

}
