/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
public interface SessionContext extends Comparable<SessionContext> {

	//--------------------------------------------------------------------
	public Adventure getAdventure();
	
	//--------------------------------------------------------------------
	public Group getGroup();
	
	//--------------------------------------------------------------------
	public void setPlayedCharacter(Player player, CharacterHandle handle);
	
}
