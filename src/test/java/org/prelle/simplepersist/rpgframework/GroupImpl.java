/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="group")
@ElementList(entry="playerref",type=PlayerImpl.class,convert=PlayerConverter.class)
public class GroupImpl extends ArrayList<Player> implements Group {

	private transient Path groupDir;
	private transient Path groupFile;
	
	@ElementList(type=SessionContextImpl.class,entry="session",required=false)
	private ArrayList<SessionContext> sessions;
	
	@Attribute(required=true)
	private String name;
	@Attribute(required=false)
	private String email;

	//--------------------------------------------------------------------
	public GroupImpl(@Attribute(name="name") String name, Path groupDir	) {
		this.name = name;
		this.groupDir = groupDir;
		sessions = new ArrayList<>();
	}

	//--------------------------------------------------------------------
	public GroupImpl() {
		sessions = new ArrayList<>();
	}
	
	//--------------------------------------------------------------------
	public String toString() {
		return name+"  with "+sessions.size()+" sessions and players = "+super.toString();
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Group other) {
		return name.compareTo(other.getName());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Group#addAdventure(org.prelle.rpgtool.base.SessionContext)
	 */
	@Override
	public void addAdventure(SessionContext session) {
		sessions.add(session);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Group#getAdventures()
	 */
	@Override
	public List<SessionContext> getAdventures() {
		return sessions;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Group#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Group#setName(java.lang.String)
	 */
	@Override
	public void setName(String name) {
		if (name.equals(this.name))
			return;
	}

	//--------------------------------------------------------------------
	public Path getGroupDir() {
		return groupDir;
	}

	//--------------------------------------------------------------------
	public Path getGroupFile() {
		return groupFile;
	}

	//--------------------------------------------------------------------
	public void setGroupFile(Path groupFile) {
		this.groupFile = groupFile;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Group#getEmail()
	 */
	@Override
	public String getEmail() {
		return email;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Group#setEmail(java.lang.String)
	 */
	@Override
	public void setEmail(String email) {
		this.email = email;
	}

}
