/**
 *
 */
package org.prelle.simplepersist.rpgframework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Field;
import java.util.ArrayList;

import org.junit.Test;
import org.prelle.simplepersist.unmarshal.TreeBuilder;
import org.prelle.simplepersist.unmarshal.XMLTreeNode;

/**
 * @author prelle
 *
 */
public class RPGTreeBuilderTests {

	//-------------------------------------------------------------------
	public RPGTreeBuilderTests() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void testGroup() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(GroupImpl.class);

//		logger.debug(ret.dump());

		assertNotNull(ret);
		assertEquals("group",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("playerref"));
		assertNotNull(ret.getChild("sessions"));
		assertNotNull(ret.getChild("name"));
		assertNotNull(ret.getChild("email"));
		assertEquals(4, ret.getChildren().size());

		Field fieldSess = GroupImpl.class.getDeclaredField("sessions");

		XMLTreeNode elemPRef = (XMLTreeNode)ret.getChild("playerref");
		XMLTreeNode elemSess = (XMLTreeNode)ret.getChild("sessions");
		assertNotNull(elemPRef);
		assertNotNull(elemSess);
		assertEquals(PlayerImpl.class, elemPRef.getType());
		assertEquals(ArrayList.class, elemSess.getType());
		assertNull(elemPRef.getField());
		assertEquals(fieldSess, elemSess.getField());
		assertEquals(ret, elemPRef.getParent());
		assertEquals(ret, elemSess.getParent());
		assertNotNull(elemPRef.getElementConverter());
		assertEquals(PlayerConverter.class, elemPRef.getElementConverter().getClass());

//		logger.debug("  "+elemPRef.dumpData());
	}

}
