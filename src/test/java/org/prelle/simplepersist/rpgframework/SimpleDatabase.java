/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class SimpleDatabase {

	private static Map<String, PlayerImpl> player;
	private static Map<String, AdventureImpl> adventure;
	private static Map<Player, List<CharacterHandle>> charsByPlayer;
	
	//-------------------------------------------------------------------
	static {
		player = new HashMap<>();
		adventure = new HashMap<>();
		charsByPlayer = new HashMap<>();
	}
	
	//-------------------------------------------------------------------
	public static Player getPlayer(String key) {
		return player.get(key);
	}
	
	//-------------------------------------------------------------------
	public static void addPlayer(PlayerImpl toAdd) {
		player.put(toAdd.getId(), toAdd);
	}
	
	//-------------------------------------------------------------------
	public static Collection<Player> getPlayers() {
		return new ArrayList<Player>(player.values());
	}

	//-------------------------------------------------------------------
	public static void addCharToPlayer(Player player, CharacterHandle handle) {
		List<CharacterHandle> list = charsByPlayer.get(player);
		if (list==null) {
			list = new ArrayList<>();
			charsByPlayer.put(player, list);
		}
		if (!list.contains(handle))
			list.add(handle);
	}
	
	//-------------------------------------------------------------------
	public static Collection<CharacterHandle> getCharacters(Player player) {
		if (!charsByPlayer.containsKey(player))
			return new ArrayList<>();
		return new ArrayList<>(charsByPlayer.get(player));
	}
	
	//-------------------------------------------------------------------
	public static Adventure getAdventure(String key) {
		return adventure.get(key);
	}

	//-------------------------------------------------------------------
	public static void addAdventure(AdventureImpl toAdd) {
		adventure.put(toAdd.getId(), toAdd);
	}

}
