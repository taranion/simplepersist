/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
public class SimpleCharacterHandle implements CharacterHandle {
	
	private String name;
	private Player owner;
	private RoleplayingSystem rules;

	//-------------------------------------------------------------------
	public SimpleCharacterHandle(String name, Player player, RoleplayingSystem rules) {
		this.name = name;
		this.owner = player;
		this.rules = rules;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.rpgframework.CharacterHandle#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.rpgframework.CharacterHandle#getOwner()
	 */
	@Override
	public Player getOwner() {
		return owner;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.rpgframework.CharacterHandle#getRuleIdentifier()
	 */
	@Override
	public RoleplayingSystem getRuleIdentifier() {
		return rules;
	}

}
