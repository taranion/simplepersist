package org.prelle.simplepersist.rpgframework;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

public class AdventureConverter implements XMLElementConverter<AdventureImpl> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, AdventureImpl value) throws Exception {
		XmlNode advRef = new XmlNode("adventure", null);
		advRef.setAttribute("ref", value.getId());
		
		node.getChildren().add(advRef);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public AdventureImpl read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		String advID = ev.getAttributeByName(new QName("ref")).getValue();
		if (advID==null)
			throw new SerializationException("Missing 'ref' attribute in "+ev);
		
		System.out.println("*****Search "+advID);
		return (AdventureImpl) SimpleDatabase.getAdventure(advID);
	}

}
