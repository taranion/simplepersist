/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="adventure")
public class AdventureImpl implements Adventure {

	@Attribute
	private String id;
	@Attribute
	private RoleplayingSystem rules;
    @ElementList(entry="advcontent", type=LocalizedAdventureContent.class, inline=true)
	private List<LocalizedAdventureContent> content;
	
	//--------------------------------------------------------------------
	public AdventureImpl() {
//		logger.debug("<init>()");
		content = new ArrayList<LocalizedAdventureContent>();
	}
	
	//--------------------------------------------------------------------
	public AdventureImpl(String id, RoleplayingSystem rules) {
//		logger.debug("<init>(String, RoleplayingSystem)");
		this.id    = id;
		this.rules = rules;
		content = new ArrayList<LocalizedAdventureContent>();
	}
	
	//--------------------------------------------------------------------
	public AdventureImpl(String id, Locale locale, RoleplayingSystem rules) {
//		logger.debug("<init>(id, Locale, RoleplayingSystem)");
		this.id    = id;
		this.rules = rules;
		content = new ArrayList<LocalizedAdventureContent>();
		content.add(new LocalizedAdventureContent(locale));
	}
	
	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof AdventureImpl) {
			AdventureImpl other = (AdventureImpl)o;
			if (!id.equals(other.getId())) return false;
			if (rules!=other.getRules()) return false;
			return content.equals(other.getContent());
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Adventure other) {
		return getTitle().compareTo(other.getTitle());
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ADV:"+getTitle();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Adventure#getRules()
	 */
	@Override
	public RoleplayingSystem getRules() {
		return rules;
	}

	//--------------------------------------------------------------------
	public void addContent(LocalizedAdventureContent toAdd) {
		content.add(toAdd);
	}

	//--------------------------------------------------------------------
	public LocalizedAdventureContent getLocalizedContent(Locale loc) {
		for (LocalizedAdventureContent tmp : content) {
			if (tmp.getLocale()==loc)
				return tmp;
		}
		return null;
	}

	//--------------------------------------------------------------------
	private LocalizedAdventureContent getFirstContent() {
		Locale loc = Locale.getDefault();
		LocalizedAdventureContent firstBest = null;
		for (LocalizedAdventureContent tmp : content) {
			if (firstBest==null) firstBest = tmp;
			if (tmp.getLocale()==loc)
				return tmp;
		}
		return firstBest;
	}

	//--------------------------------------------------------------------
	public List<LocalizedAdventureContent> getContent() {
		return new ArrayList<LocalizedAdventureContent>(content);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgtool.base.Adventure#getTitle()
	 */
	@Override
	public String getTitle() {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		if (getFirstContent()!=null)
			return getFirstContent().getTitle();
		return "Empty AdventureImpl";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#setTitle(java.lang.String)
	 */
	@Override
	public void setTitle(String title) {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		getFirstContent().setTitle(title);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#getId()
	 */
	@Override
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#getDescription()
	 */
	@Override
	public String getDescription() {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		return getFirstContent().getDescription();
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#setDescription(java.lang.String)
	 */
	@Override
	public void setDescription(String descr) {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		getFirstContent().setDescription(descr);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#setCover(byte[])
	 */
	@Override
	public void setCover(byte[] image) {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		getFirstContent().setCover(image);
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Adventure#getCover()
	 */
	@Override
	public byte[] getCover() {
//		if (content==null || content.isEmpty())
//			content.add(new LocalizedAdventureContent(Locale.getDefault()));
		return getFirstContent().getCover();
	}

}
