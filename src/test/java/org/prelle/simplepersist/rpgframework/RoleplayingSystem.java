/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
public enum RoleplayingSystem {

	DSA,
	SPLITTERMOND,
	SHADOWRUN,
	CTHULHU,
	HEX,
	SPACE1889,
	STARWARS,
	DEADLANDS,
	DRESDEN_FILES,
	;
	
}
