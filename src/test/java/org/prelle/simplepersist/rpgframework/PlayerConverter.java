package org.prelle.simplepersist.rpgframework;

import java.util.NoSuchElementException;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

//-------------------------------------------------------------------
public class PlayerConverter implements XMLElementConverter<PlayerImpl>, StringValueConverter<PlayerImpl> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, PlayerImpl value) throws Exception {
		node.setAttribute("idref", write(value));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.ValueConverterTest#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public PlayerImpl read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		Attribute idAttr = ev.getAttributeByName(new QName("idref"));
		if (idAttr==null)
			throw new SerializationException("Missing attribute 'idref' in "+ev);
		String id = idAttr.getValue();
		
		return read(id);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(PlayerImpl value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public PlayerImpl read(String id) throws Exception {
		PlayerImpl player = (PlayerImpl) SimpleDatabase.getPlayer(id);
		if (player!=null) {
			return player;
		}
		throw new NoSuchElementException("No player named '"+id+"' found");
	}
	
}