/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
public interface CharacterHandle {

	public String getName();
	
	public Player getOwner();
	
	public RoleplayingSystem getRuleIdentifier();
	
}
