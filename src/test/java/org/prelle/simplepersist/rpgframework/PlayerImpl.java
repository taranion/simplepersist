/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

/**
 * @author prelle
 *
 */
//@Root(name="player")
public class PlayerImpl implements Player {
	
	private String id;
	private String name;
	
	//--------------------------------------------------------------------
	public PlayerImpl() {
	}
	
	//--------------------------------------------------------------------
	public PlayerImpl(String id, String name) {
		this.id    = id;
		this.name = name;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.Player#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

}
