/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import java.util.HashMap;
import java.util.Map;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@Root(name="session")
public class SessionContextImpl implements SessionContext {

//	private transient Path groupDir;
//	private transient Path groupFile;
	
	private Group group;
	@Element(name="adventure")
	@ElementConvert(AdventureConverter.class)
	private Adventure adventureResolved;
	@Element
	@MapConvert(keyConvert=PlayerConverter.class,valConvert=CharacterHandleConverter.class)
	private Map<Player, CharacterHandle> characters;
	
	//--------------------------------------------------------------------
	public SessionContextImpl() {
		characters = new HashMap<Player, CharacterHandle>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "SCI(grp="+group+", adv="+adventureResolved+", chars="+characters+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getAdventure()
	 */
	@Override
	public Adventure getAdventure() {
		if (adventureResolved!=null)
			return adventureResolved;
		
//		for (Adventure adv : RPGFrameworkLoader.getInstance().getSessionService().getAdventures()) {
//			if (adv.g)
//		}
		
		return null;
	}

	//-------------------------------------------------------------------
	public void setAdventure(Adventure adv) {
		adventureResolved = adv;
	}

	//-------------------------------------------------------------------
	public void setGroup(Group grp) {
		this.group = grp;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.core.SessionContext#getGroup()
	 */
	@Override
	public Group getGroup() {
		return group;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(SessionContext other) {
		// TODO Auto-generated method stub
		System.err.println("TODO: compare sessions by time");
		return 0;
	}
	
	//--------------------------------------------------------------------
	public void setPlayedCharacter(Player player, CharacterHandle handle) {
		characters.put(player, handle);
	}

}
