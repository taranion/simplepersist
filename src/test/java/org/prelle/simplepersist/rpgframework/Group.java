/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import java.util.List;

/**
 * Models a group of players that gather regular for roleplaying.
 * 
 * @author prelle
 */
public interface Group extends List<Player>, Comparable<Group> {
	
	//----------------------------------------------------------------
	public String getName();

	//----------------------------------------------------------------
	public void setName(String name);
	
	//----------------------------------------------------------------
	public String getEmail();

	//----------------------------------------------------------------
	public void setEmail(String mail);
	
	//----------------------------------------------------------------
	/**
	 * @return the adventures
	 */
	public List<SessionContext> getAdventures();

	//----------------------------------------------------------------
	public void addAdventure(SessionContext advSess);
	
}
