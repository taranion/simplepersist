/**
 * 
 */
package org.prelle.simplepersist.rpgframework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.Locale;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class TestAdventure {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<adventure id=\"dasGeheimnisDesKrähenwassers\" rules=\"SPLITTERMOND\">"
			+SEP+"   <advcontent lang=\"de\">"
			+SEP+"      <title>Das Geheimnis des Krähenwassers</title>"
			+SEP+"      <description>Dies ist die Beschreibung</description>"
			+SEP+"   </advcontent>"
			+SEP+"</adventure>"+SEP;
	static Adventure DATA;
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA = new AdventureImpl("dasGeheimnisDesKrähenwassers", RoleplayingSystem.SPLITTERMOND);
		LocalizedAdventureContent content = new LocalizedAdventureContent(Locale.GERMAN);
		((AdventureImpl)DATA).addContent(content);
		content.setTitle("Das Geheimnis des Krähenwassers");
		content.setDescription("Dies ist die Beschreibung");
	}
	
	//-------------------------------------------------------------------
	public TestAdventure() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
//	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			AdventureImpl read = serializer.read(AdventureImpl.class, XML);
			System.out.println(read);
			
			assertNotNull(read);
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
