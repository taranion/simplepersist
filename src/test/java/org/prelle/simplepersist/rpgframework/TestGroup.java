/**
 *
 */
package org.prelle.simplepersist.rpgframework;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.Locale;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class TestGroup {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<group email=\"splimo@foo\" name=\"Mittwochsrunde\">"
			+SEP+"   <sessions>"
			+SEP+"      <session>"
			+SEP+"         <adventure ref=\"dasGeheimnisDesKrähenwassers\"/>"
			+SEP+"         <characters>"
			+SEP+"            <element>"
			+SEP+"               <key>AnjaPrelle</key>"
			+SEP+"               <value type=\"org.prelle.simplepersist.rpgframework.SimpleCharacterHandle\">"
			+SEP+"                  <charref>AnjaPrelle/Amberion</charref>"
			+SEP+"               </value>"
			+SEP+"            </element>"
			+SEP+"         </characters>"
			+SEP+"      </session>"
			+SEP+"   </sessions>"
			+SEP+"   <playerref idref=\"AnjaPrelle\"/>"
			+SEP+"</group>"+SEP;

	static AdventureImpl ADV;
	static PlayerImpl ANJA;
	static CharacterHandle AMBERION;
	static SessionContextImpl SESS1;
	static GroupImpl DATA;

	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		ADV = new AdventureImpl("dasGeheimnisDesKrähenwassers", RoleplayingSystem.SPLITTERMOND);
		LocalizedAdventureContent content = new LocalizedAdventureContent(Locale.GERMAN);
		((AdventureImpl)ADV).addContent(content);
		content.setTitle("Das Geheimnis des Krähenwassers");
		content.setDescription("Dies ist die Beschreibung");
		SimpleDatabase.addAdventure(ADV);

		ANJA = new PlayerImpl("AnjaPrelle", "Anja Prelle");
		SimpleDatabase.addPlayer(ANJA);

		AMBERION = new SimpleCharacterHandle("Amberion",ANJA,RoleplayingSystem.SPLITTERMOND);
		SimpleDatabase.addCharToPlayer(ANJA, AMBERION);

		SESS1 = new SessionContextImpl();
		SESS1.setAdventure(ADV);
		SESS1.setPlayedCharacter(ANJA, AMBERION);

		DATA = new GroupImpl("Mittwochsrunde", null);
		DATA.setEmail("splimo@foo");
		DATA.add(ANJA);
		DATA.addAdventure(SESS1);
	}

	//-------------------------------------------------------------------
	public TestGroup() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());

			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			GroupImpl read = serializer.read(GroupImpl.class, XML);
			System.out.println("Read  : "+read);

			assertNotNull(read);
			System.out.println("Expect: "+DATA);
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
