package org.prelle.simplepersist.example;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

@Root(name="item")
public class ItemElement  {
	
	@Attribute
	private int weight;
	@Element
	private String text;
//	@ElementList(type=ItemDataElement.class)
	@ElementListUnion({
			@ElementList(entry="weapon",type=WeaponElement.class),
			@ElementList(entry="shield",type=ShieldElement.class)
	})
	private List<ItemDataElement> data;
	@ElementList(entry="item",type=ItemElement.class)
	private List<ItemElement> accessories;
	
	//-------------------------------------------------------------------
	public ItemElement() {
		data = new ArrayList<ItemDataElement>();
		accessories = new ArrayList<ItemElement>();
	}
	
	//-------------------------------------------------------------------
	public ItemElement(String text) {
		data = new ArrayList<ItemDataElement>();
		accessories = new ArrayList<ItemElement>();
		this.text = text;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return text+"("+accessories+")";
	}

	//-------------------------------------------------------------------
	public void add(ItemDataElement elem) {
		data.add(elem);
	}

	//-------------------------------------------------------------------
	public ItemElement addAcccessory(ItemElement elem) {
		accessories.add(elem);
		return elem;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	//--------------------------------------------------------------------
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (!(o instanceof ItemElement))
			return false;
		
		ItemElement other = (ItemElement)o;
		if (weight!=other.getWeight()) return false;
		if (!text.equals(other.getText())) return false;
		// Accessories
		if (accessories!=null && other.accessories==null) return false;
		if (accessories==null && other.accessories!=null) return false;
		if (accessories!=null && other.accessories!=null && !accessories.equals(other.accessories)) return false; 
		
		// Compare lists
//		System.out.println("comp "+data+" with "+other.getTypeData());
		
		return data.equals(other.getTypeData());
	}

	//--------------------------------------------------------------------
	public List<ItemDataElement> getTypeData() {
		return new ArrayList<>(data);
	}
}
