package org.prelle.simplepersist.example;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;

public class EverythingElement {
	
	@Attribute
	private int textAttrOhne;
	@Attribute(name="attr",required=true)
	private String textAttr;
	
	@Element
	private String textElemOhne;
	@Element(name="text",required=true)
	private String textElem;

	@ElementList(type=BasicAttrElement.class)
	private List<BasicAttrElement> list1;

	@ElementList(type=BasicAttrElement.class,entry="basicattr")
	private List<BasicAttrElement> list2;

	@ElementList(type=BasicPlainElement.class,inline=true)
	private List<BasicPlainElement> inline1;

	@ElementList(type=BasicPlainElement.class,inline=true,entry="mybasic")
	private List<BasicPlainElement> inline2;

	@ElementListUnion({
		@ElementList(entry="weapon",type=WeaponElement.class),
		@ElementList(entry="shield",type=ShieldElement.class)
	})
	private List<ItemDataElement> union;
	
	//-------------------------------------------------------------------
	public EverythingElement() {
		inline1 = new ArrayList<BasicPlainElement>();
		inline2 = new ArrayList<BasicPlainElement>();
		list1  = new ArrayList<BasicAttrElement>();
		list2  = new ArrayList<BasicAttrElement>();
		union  = new ArrayList<ItemDataElement>();
	}

}
