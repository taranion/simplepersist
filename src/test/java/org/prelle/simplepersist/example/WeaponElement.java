/**
 * 
 */
package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class WeaponElement extends ItemDataElement {

	@Attribute
	private int damage;
	
	//--------------------------------------------------------------------
	public WeaponElement() {
		type = 1;
	}
	
	//--------------------------------------------------------------------
	public WeaponElement(int dmg) {
		this();
		this.damage = dmg;
	}
	
	//--------------------------------------------------------------------
	public String toString() {
		return "Weapon(DMG="+damage+" "+super.toString()+")";
	}
	
	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof WeaponElement) {
			WeaponElement other = (WeaponElement)o;
			if (damage!=other.getDamage()) return false;
			return super.equals(other);
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the damage
	 */
	public int getDamage() {
		return damage;
	}

}
