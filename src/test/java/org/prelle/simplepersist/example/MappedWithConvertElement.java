package org.prelle.simplepersist.example;

import java.util.HashMap;
import java.util.Map;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.Root;

@Root(name="mapped")
public class MappedWithConvertElement {
	
	@Element
	@MapConvert(keyConvert=MultiplyConverter.class,valConvert=TwoParamConverter.class)
	private Map<Integer, String> children = new HashMap<Integer, String>();
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof MappedWithConvertElement) {
			MappedWithConvertElement other = (MappedWithConvertElement)o;
			if (!children.equals(other.getChildren())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Mapped "+children;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the child
	 */
	public Map<Integer, String> getChildren() {
		return children;
	}

}
