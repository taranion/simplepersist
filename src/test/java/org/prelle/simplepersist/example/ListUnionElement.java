package org.prelle.simplepersist.example;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

@Root(name="listunion")
public class ListUnionElement {
	
	@ElementListUnion({
	    @ElementList(entry="basicplain", type=BasicPlainElement.class),
	    @ElementList(entry="basicattr", type=BasicAttrElement.class),
	    @ElementList(entry="simple", type=SimpleElement.class),
	 })
	private List<Object> children = new ArrayList<Object>();
	@Attribute 
	private int zahl;
	
	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ListUnionElement) {
			ListUnionElement other = (ListUnionElement)o;
			if (!children.equals(other.getChildren())) return false;
			return true;
		}
		return false;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "LPL(,children="+children+",zahl="+zahl+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the children
	 */
	public List<Object> getChildren() {
		return children;
	}

	//-------------------------------------------------------------------
	/**
	 * @param children the children to set
	 */
	public void setChildren(List<Object> children) {
		this.children = children;
	}

}
