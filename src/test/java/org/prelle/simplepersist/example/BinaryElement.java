package org.prelle.simplepersist.example;

import java.util.Arrays;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="binary")
public class BinaryElement {

	@Element
	private byte[] data;
	
	//-------------------------------------------------------------------
	public String toString() {
		return "data="+data;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof BinaryElement) {
			BinaryElement other = (BinaryElement)o;
			if ((data==null) && (other.getData()!=null)) return false;
			if ((data!=null) && (other.getData()==null)) return false;
			return Arrays.equals(data, other.getData());
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	//-------------------------------------------------------------------
	/**
	 * @param data the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

}
