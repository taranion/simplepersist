package org.prelle.simplepersist.example;

import java.util.List;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="listunion")
public class ListUnionClassFieldElement {
	
	@Element
	private ListUnionClassElement children = new ListUnionClassElement();
	@Attribute 
	private int zahl;
	
	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ListUnionClassFieldElement) {
			ListUnionClassFieldElement other = (ListUnionClassFieldElement)o;
			if (!children.equals(other.getChildren())) return false;
			return true;
		}
		return false;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "LUCF(,children="+children+",zahl="+zahl+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the children
	 */
	public List<Object> getChildren() {
		return children;
	}


}
