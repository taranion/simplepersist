package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Root;

@Root(name="basicplain")
public class BasicPlainElement {

	public boolean equals(Object o) {
		return o instanceof BasicPlainElement;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "BPE";
	}
}
