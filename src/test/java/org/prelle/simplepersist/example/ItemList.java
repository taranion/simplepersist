/**
 * 
 */
package org.prelle.simplepersist.example;

import java.util.ArrayList;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="data")
@ElementListUnion({
	@ElementList(entry="weapon",type=WeaponElement.class),
	@ElementList(entry="shield",type=ShieldElement.class)
})
public class ItemList extends ArrayList<ItemDataElement> {

	@Attribute
	private int zahl;
	@Element
	private String text;

	//-------------------------------------------------------------------
	/**
	 */
	public ItemList() {
		// TODO Auto-generated constructor stub
	}

}
