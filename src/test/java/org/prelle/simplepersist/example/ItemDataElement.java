/**
 * 
 */
package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public abstract class ItemDataElement {

	@Attribute
	protected int type;
	
	//--------------------------------------------------------------------
	/**
	 */
	public ItemDataElement() {
		// TODO Auto-generated constructor stub
	}

	//--------------------------------------------------------------------
	public String toString() {
		return "TYPE="+type;
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof ItemDataElement) {
			return type==((ItemDataElement)o).getType();
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}
	
}
