package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="derived")
public class DerivedElement extends SimpleElement {

	@Element
	private String dertxt;
	@Attribute
	private int derzahl;
	
//	//-------------------------------------------------------------------
//	public String toString() {
//		return super.toString()+",dertext='"+dertxt+"',derzahl="+derzahl;
//	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof DerivedElement) {
			DerivedElement other = (DerivedElement)o;
			if (!dertxt.equals(other.getDerText())) return false;
			if (derzahl!=other.getDerZahl()) return false;
			return super.equals(other);
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dertxt
	 */
	public String getDerText() {
		return dertxt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dertxt the dertxt to set
	 */
	public void setDerText(String dertxt) {
		this.dertxt = dertxt;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the derzahl
	 */
	public int getDerZahl() {
		return derzahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param derzahl the derzahl to set
	 */
	public void setDerZahl(int derzahl) {
		this.derzahl = derzahl;
	}
}
