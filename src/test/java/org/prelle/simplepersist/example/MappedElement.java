package org.prelle.simplepersist.example;

import java.util.HashMap;
import java.util.Map;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="mapped")
public class MappedElement {
	
	@Element
	private Map<String, BasicPlainElement> children = new HashMap<String, BasicPlainElement>();
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof MappedElement) {
			MappedElement other = (MappedElement)o;
			if (!children.equals(other.getChild())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "Mapped "+children;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the child
	 */
	public Map<String, BasicPlainElement> getChild() {
		return children;
	}

	//-------------------------------------------------------------------
	/**
	 * @param child the child to set
	 */
	public void setChildren(Map<String, BasicPlainElement> child) {
		this.children = child;
	}

}
