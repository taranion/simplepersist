package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="foldedplain")
public class FoldedPlainElement {
	
	@Element
	private BasicPlainElement child;
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof FoldedPlainElement) {
			FoldedPlainElement other = (FoldedPlainElement)o;
			if (!child.equals(other.getChild())) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the child
	 */
	public BasicPlainElement getChild() {
		return child;
	}

	//-------------------------------------------------------------------
	/**
	 * @param child the child to set
	 */
	public void setChild(BasicPlainElement child) {
		this.child = child;
	}

}
