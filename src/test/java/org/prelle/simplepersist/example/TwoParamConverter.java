/**
 * 
 */
package org.prelle.simplepersist.example;

import java.util.StringTokenizer;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

/**
 * @author prelle
 *
 */
public class TwoParamConverter implements XMLElementConverter<String> {

	//-------------------------------------------------------------------
	/**
	 */
	public TwoParamConverter() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, String value) throws Exception {
		StringTokenizer tok = new StringTokenizer(value);
		node.setAttribute("fact1", tok.nextToken());
		node.setAttribute("fact2", tok.nextToken());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.XMLElementConverter#read(org.prelle.simplepersist.unmarshal.XMLTreeItem, javax.xml.stream.events.StartElement)
	 */
	@Override
	public String read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		System.out.println("Read from "+ev);
		String f1 = ev.getAttributeByName(new QName("fact1")).getValue();
		String f2 = ev.getAttributeByName(new QName("fact2")).getValue();
		return f1+" "+f2;
	}

}
