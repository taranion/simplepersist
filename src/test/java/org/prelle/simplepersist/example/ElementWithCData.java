package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.CData;
import org.prelle.simplepersist.Root;

@Root(name="derived")
public class ElementWithCData {

	@CData
	private String dertxt;
	@Attribute
	private int derzahl;

	//-------------------------------------------------------------------
	public String toString() {
		return super.toString()+",cdata='"+dertxt+"',derzahl="+derzahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ElementWithCData) {
			ElementWithCData other = (ElementWithCData)o;
			if (!dertxt.equals(other.getCData())) return false;
			if (derzahl!=other.getDerZahl()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the dertxt
	 */
	public String getCData() {
		return dertxt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dertxt the dertxt to set
	 */
	public void setCData(String dertxt) {
		this.dertxt = dertxt;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the derzahl
	 */
	public int getDerZahl() {
		return derzahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param derzahl the derzahl to set
	 */
	public void setDerZahl(int derzahl) {
		this.derzahl = derzahl;
	}
}
