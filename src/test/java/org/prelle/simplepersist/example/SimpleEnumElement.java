package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.EnumValue;
import org.prelle.simplepersist.Root;

@Root(name="simpleenum")
public class SimpleEnumElement {
	
	public enum MyEnumAttribute {
		@EnumValue("1V")
		V1,
		@EnumValue("2V")
		V2
	}
	
	public enum MyEnumElement {
		@EnumValue("Hello")
		HALLO,
		@EnumValue("World")
		WELT
	}

	@Element
	private MyEnumElement text;
	@Attribute
	private MyEnumAttribute zahl;
	
	//-------------------------------------------------------------------
	public String toString() {
		return "text='"+text+"',zahl="+zahl;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof SimpleEnumElement) {
			SimpleEnumElement other = (SimpleEnumElement)o;
			if ((text==null) && (other.getText()!=null)) return false;
			if (text!=null && !text.equals(other.getText())) return false;
			if (zahl!=other.getZahl()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public MyEnumElement getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(MyEnumElement text) {
		this.text = text;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public MyEnumAttribute getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(MyEnumAttribute zahl) {
		this.zahl = zahl;
	}

}
