package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

@Root(name="basicattr")
public class BasicAttrElement {

	@Attribute
	private String text;
	@Attribute
	private int zahl;
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof BasicAttrElement) {
			BasicAttrElement other = (BasicAttrElement)o;
			if ((text==null) && (other.getText()!=null)) return false;
			if (text!=null && !text.equals(other.getText())) return false;
			if (zahl!=other.getZahl()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "BPI("+zahl+","+text+")";
	}
	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}
}
