/**
 * 
 */
package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class ShieldElement extends ItemDataElement {

	@Attribute
	private int handicap;
	
	//--------------------------------------------------------------------
	public ShieldElement() {
		type = 2;
	}
	
	//--------------------------------------------------------------------
	public ShieldElement(int hc) {
		this();
		handicap = hc;
	}
	
	//--------------------------------------------------------------------
	public String toString() {
		return "Shield(HC="+handicap+" "+super.toString()+")";
	}
	
	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof ShieldElement) {
			ShieldElement other = (ShieldElement)o;
			if (handicap!=other.getHandicap()) return false;
			return super.equals(other);
		}
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the handicap
	 */
	public int getHandicap() {
		return handicap;
	}

}
