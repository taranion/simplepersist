package org.prelle.simplepersist.example;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

@Root(name="listplain")
public class ListPlainElement {
	
	@Element 
	private String before;
	@ElementList(type=BasicPlainElement.class)
	private List<BasicPlainElement> children = new ArrayList<BasicPlainElement>();
	@Element 
	private String text;
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ListPlainElement) {
			ListPlainElement other = (ListPlainElement)o;
			if (!children.equals(other.getChildren())) return false;
			return true;
		}
		return false;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "LPL(bef="+before+",children="+children+",text="+text+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the children
	 */
	public List<BasicPlainElement> getChildren() {
		return children;
	}

	//-------------------------------------------------------------------
	/**
	 * @param children the children to set
	 */
	public void setChildren(List<BasicPlainElement> children) {
		this.children = children;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the before
	 */
	public String getBefore() {
		return before;
	}

	//-------------------------------------------------------------------
	/**
	 * @param before the before to set
	 */
	public void setBefore(String before) {
		this.before = before;
	}

}
