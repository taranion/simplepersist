package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

@Root(name="other")
public class OtherDerivedElement extends SimpleElement {

	@Attribute
	private String othertxt;
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof OtherDerivedElement) {
			OtherDerivedElement other = (OtherDerivedElement)o;
			if (!othertxt.equals(other.getOtherText())) return false;
			return super.equals(other);
		}
		return false;
	}

	//-------------------------------------------------------------------
	public String getOtherText() {
		return othertxt;
	}

	//-------------------------------------------------------------------
	/**
	 * @param dertxt the dertxt to set
	 */
	public void setOtherText(String dertxt) {
		this.othertxt = dertxt;
	}

}
