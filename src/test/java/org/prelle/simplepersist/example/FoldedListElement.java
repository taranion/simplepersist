package org.prelle.simplepersist.example;

import java.util.ArrayList;

import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

@SuppressWarnings("serial")
@Root(name="foldedlist")
@ElementList(type=BasicPlainElement.class)
public class FoldedListElement extends ArrayList<BasicPlainElement> {
	
	@Element
	private String text;
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof FoldedListElement) {
			FoldedListElement other = (FoldedListElement)o;
			if (!text.equals(other.getText())) 
				return false;
			return super.equals(other);
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

}
