package org.prelle.simplepersist.example;

import java.util.ArrayList;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;

@SuppressWarnings("serial")
@Root(name="listunionclass")
@ElementListUnion({
    @ElementList(entry="basicplain", type=BasicPlainElement.class),
    @ElementList(entry="basicattr", type=BasicAttrElement.class),
    @ElementList(entry="simple", type=SimpleElement.class),
 })
public class ListUnionClassElement extends ArrayList<Object> {
	
	@Attribute 
	private int zahl;
	
	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ListUnionClassElement) {
			ListUnionClassElement other = (ListUnionClassElement)o;
			if (zahl!=other.getZahl()) return false;
			return super.equals(o);
		}
		return false;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "LUC(,children="+super.toString()+",zahl="+zahl+")";
	}

}
