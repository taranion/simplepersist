/**
 * 
 */
package org.prelle.simplepersist.example;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class MultiplyConverter implements StringValueConverter<Integer> {

	//-------------------------------------------------------------------
	/**
	 */
	public MultiplyConverter() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(java.lang.Object)
	 */
	@Override
	public String write(Integer value) throws Exception {
		return String.valueOf(value*2);
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(java.lang.String)
	 */
	@Override
	public Integer read(String v) throws Exception {
		return Integer.valueOf(v)/2;
	}

}
