package org.prelle.simplepersist.example;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

@Root(name="convlist")
public class ListWithConverterElement {
	
	@ElementList(type=String.class, entry="twoparam", convert=TwoParamConverter.class)
	private List<String> children = new ArrayList<String>();
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ListWithConverterElement) {
			ListWithConverterElement other = (ListWithConverterElement)o;
			if (!children.equals(other.getChildren())) return false;
			return true;
		}
		return false;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		return "LWC(children="+children+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the children
	 */
	public List<String> getChildren() {
		return children;
	}

}
