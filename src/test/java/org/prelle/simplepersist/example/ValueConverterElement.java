package org.prelle.simplepersist.example;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Root;

@Root(name="convert")
public class ValueConverterElement {

	@Attribute
	@AttribConvert(MultiplyConverter.class)
	private int zahl;
	
	//-------------------------------------------------------------------
	public String toString() {
		return "zahl="+zahl;
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof ValueConverterElement) {
			ValueConverterElement other = (ValueConverterElement)o;
			if (zahl!=other.getZahl()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}
}
