package org.prelle.simplepersist.example;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Root;

@Root(name="simple")
public class SimpleElement {

	@Element
	private String text;
	@Element(name="override")
	private Boolean bool;
	@Attribute
	private int zahl;
	
	//-------------------------------------------------------------------
	public String toString() {
		return "SI(text='"+text+"',zahl="+zahl+")";
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof SimpleElement) {
			SimpleElement other = (SimpleElement)o;
			if ((text==null) && (other.getText()!=null)) return false;
			if (text!=null && !text.equals(other.getText())) return false;
			if (bool!=null && !bool.equals(other.bool)) return false;
			if (zahl!=other.getZahl()) return false;
			return true;
		}
		return false;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the zahl
	 */
	public int getZahl() {
		return zahl;
	}

	//-------------------------------------------------------------------
	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	//-------------------------------------------------------------------
	/**
	 * @param zahl the zahl to set
	 */
	public void setZahl(int zahl) {
		this.zahl = zahl;
	}

	//-------------------------------------------------------------------
	public Boolean getBool() {
		return bool;
	}

	//-------------------------------------------------------------------
	public void setBool(Boolean bool) {
		this.bool = bool;
	}
}
