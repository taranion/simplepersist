/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.ItemDataElement;
import org.prelle.simplepersist.example.ItemElement;
import org.prelle.simplepersist.newtests.ClassWithElements.ClassWithAttributes;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class DeepRecursionTest {

	@Root(name="item")
	public static class ItemElement  {
		@Element
		private String text;
		@ElementList(entry="item",type=ItemElement.class)
		private List<ItemElement> accessories;
		//-------------------------------------------------------------------
		public ItemElement() {
			accessories = new ArrayList<ItemElement>();
		}
		//-------------------------------------------------------------------
		public ItemElement(String text) {
			accessories = new ArrayList<ItemElement>();
			this.text = text;
		}
		//-------------------------------------------------------------------
		public ItemElement addAcccessory(ItemElement elem) {
			accessories.add(elem);
			return elem;
		}
		public String toString() {
			return "("+text+",acc="+accessories+")";
		}
		public boolean equals(Object o) {
			return toString().equals(String.valueOf(o));
		}
	}
	
	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD+  SEP
			+"<item>\n" + 
			"   <text>1</text>\n" + 
			"   <accessories>\n" + 
			"      <item>\n" + 
			"         <text>2</text>\n" + 
			"         <accessories>\n" + 
			"            <item>\n" + 
			"               <text>3</text>\n" + 
			"               <accessories>\n" + 
			"                  <item>\n" + 
			"                     <text>4</text>\n" + 
			"                     <accessories>\n" + 
			"                        <item>\n" + 
			"                           <text>5</text>\n" + 
			"                           <accessories>\n" + 
			"                              <item>\n" + 
			"                                 <text>6</text>\n" + 
			"                              </item>\n" + 
			"                           </accessories>\n" + 
			"                        </item>\n" + 
			"                     </accessories>\n" + 
			"                  </item>\n" + 
			"               </accessories>\n" + 
			"            </item>\n" + 
			"         </accessories>\n" + 
			"      </item>\n" + 
			"   </accessories>\n" + 
			"</item>\n";
	final static ItemElement DATA = new ItemElement("1");
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.addAcccessory(new ItemElement("2")).addAcccessory(new ItemElement("3")).addAcccessory(new ItemElement("4")).addAcccessory(new ItemElement("5")).addAcccessory(new ItemElement("6"));
	}

	//-------------------------------------------------------------------
	public DeepRecursionTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	public static void dump(Map<String,ByNameInfo>  map) {
		List<String> sorted = new ArrayList<String>(map.keySet());
		Collections.sort(sorted);
		for (String key: sorted) {
			ByNameInfo info = map.get(key);
			assertNotNull("No ByNameInfo '"+key+"'",info);
			assertNotNull("No field in info for '"+key+"'",info.field);
			String msg = String.format("  %12s  type %20s into field %s", info.elementName,info.valueType.getSimpleName(), info.field.getName());
			System.out.println(msg);
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ItemElement.class);
		assertNotNull(map);
		dump(map);
		assertEquals(2, map.size());
		assertTrue(map.keySet().contains("accessories"));
		assertTrue(map.keySet().contains("text"));

		assertFalse(map.keySet().contains("byteNorm"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ItemElement read = serializer.read(ItemElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
