/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.newtests.ClassWithAttribs.ClassWithAttributes;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ClassWithElements {
	
	@Root(name="myclass")
	public static class ClassWithAttributes {
		
		@Element
		private byte[] data;
		@Element
		private int intNorm;
		@Element
		private Integer intCls;
		@Element(name = "other")
		private byte byteNorm;
		@Element
		private Byte byteCls;
		@Element
		private boolean boolNorm;
		@Element
		private Boolean boolCls;
		@Element
		private long longNorm;
		@Element
		private Long longCls;
		@Element
		private String string;
		@Element
		private String string2;
		@Element
		private float floatNorm;
		@Element
		private Float floatCls;
		@Element
		private UUID uuid;
		
		public ClassWithAttributes() {}
		
		public boolean equals(Object o) {
			if (!(o instanceof ClassWithAttributes))
				return false;
			ClassWithAttributes other = (ClassWithAttributes)o;
			if (intNorm!=other.intNorm) return false;
			if (!intCls.equals(other.intCls)) return false;
			if (byteNorm!=other.byteNorm) return false;
			if (!byteCls.equals(other.byteCls)) return false;
			if (boolNorm!=other.boolNorm) return false;
			if (!boolCls.equals(other.boolCls)) return false;
			if (longNorm!=other.longNorm) return false;
			if (!longCls.equals(other.longCls)) return false;
			if (!string.equals(other.string)) return false;
			if (!Arrays.equals(data,other.data)) return false;
			if (floatNorm!=other.floatNorm) return false;
			if (!floatCls.equals(other.floatCls)) return false;
			if (!uuid.equals(other.uuid)) return false;
			return true;
		}
		public String toString() {
			return "ClassWithAttribs("+boolCls+","+boolNorm+","+byteCls+","+byteNorm+","+data+","+intCls+","+intNorm+","+longCls+","+longNorm+","+string+","+floatNorm+","+floatCls+")";
		}
	}
	

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<myclass>\n" + 
					"   <data>AQID</data>\n" + 
					"   <intNorm>500</intNorm>\n" + 
					"   <intCls>600</intCls>\n" + 
					"   <other>12</other>\n" + 
					"   <byteCls>13</byteCls>\n" + 
					"   <boolNorm>false</boolNorm>\n" + 
					"   <boolCls>true</boolCls>\n" + 
					"   <longNorm>500</longNorm>\n" + 
					"   <longCls>600</longCls>\n" + 
					"   <string>test</string>\n"+
					"   <string2>H&lt;äll&amp;o&gt; W&#x0022;ölt</string2>\n"+
					"   <floatNorm>1.24</floatNorm>\n" + 
					"   <floatCls>2.48</floatCls>\n" + 
					"   <uuid>3a26c167-ceb6-42a7-9242-3cc57f3b640f</uuid>\n" +
					"</myclass>"+SEP;
	final static ClassWithAttributes DATA = new ClassWithAttributes();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.intNorm = 500;
		DATA.intCls = 600;
		DATA.byteNorm = 12;
		DATA.byteCls = 13;
		DATA.boolNorm= false;
		DATA.boolCls = true;
		DATA.longNorm = 500L;
		DATA.longCls = 600L;
		DATA.string = "test";
		DATA.string2 = "H<äll&o> W\"ölt";
		DATA.data  = new byte[] {1,2,3};
		DATA.floatNorm = 1.24f;
		DATA.floatCls = 2.48f;
		DATA.uuid = UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f");
	}

	//-------------------------------------------------------------------
	public ClassWithElements() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ClassWithAttributes.class);
		assertNotNull(map);
//		dump(map);
		assertEquals(14, map.size());
		assertTrue(map.keySet().contains("intNorm"));
		assertTrue(map.keySet().contains("intCls"));
		assertTrue(map.keySet().contains("other"));
		assertTrue(map.keySet().contains("byteCls"));
		assertTrue(map.keySet().contains("boolNorm"));
		assertTrue(map.keySet().contains("boolCls"));
		assertTrue(map.keySet().contains("longNorm"));
		assertTrue(map.keySet().contains("longCls"));
		assertTrue(map.keySet().contains("string"));
		assertTrue(map.keySet().contains("string2"));
		assertTrue(map.keySet().contains("data"));
		assertTrue(map.keySet().contains("floatNorm"));
		assertTrue(map.keySet().contains("floatCls"));
		assertTrue(map.keySet().contains("uuid"));

		assertFalse(map.keySet().contains("byteNorm"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ClassWithAttributes read = serializer.read(ClassWithAttributes.class, XML);
			System.out.println(read);
			
			assertEquals(500, read.intNorm);
			assertEquals((Integer)600, read.intCls);
			assertEquals(12, read.byteNorm);
			assertEquals((Byte)(byte)13, read.byteCls);
			assertEquals(500L, read.longNorm);
			assertEquals((Long)600L, read.longCls);
			assertEquals(Arrays.toString(new byte[] {1,2,3}), Arrays.toString(read.data));
			assertEquals("test", read.string);
			assertEquals("H<äll&o> W\"ölt", read.string2);
			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
