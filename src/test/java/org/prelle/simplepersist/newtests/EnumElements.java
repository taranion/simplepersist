/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class EnumElements {
	
	public static enum SpellType {
		TEST,
		BLUBB
	}
	
	@Root(name="spell")
	public static class TestClass  {
		
		@ElementList(entry="type",type=SpellType.class,inline=true)
		private List<SpellType> type;
		
		public TestClass() {
			type = new ArrayList();
		}
		
		public boolean equals(Object o) {
			if (!(o instanceof TestClass))
				return false;
			TestClass other = (TestClass)o;
			return type.equals(other.type);
//			return String.valueOf(type).equals(String.valueOf(other.type));
		}
		public String toString() {
			return "Spell("+type+")";
		}
		public void add(SpellType val) { type.add(val); }
	}
	

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<spell>\n" + 
					"   <type>BLUBB</type>\n" + 
					"   <type>TEST</type>\n" + 
					"</spell>"+SEP;
	final static TestClass DATA = new TestClass();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.add(SpellType.BLUBB);
		DATA.add(SpellType.TEST);
	}

	//-------------------------------------------------------------------
	public EnumElements() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(TestClass.class);
		assertNotNull(map);
//		dump(map);
		assertEquals(1, map.size());
		assertTrue(map.keySet().contains("type"));

		assertFalse(map.keySet().contains("byteNorm"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			TestClass read = serializer.read(TestClass.class, XML);
			System.out.println("Found: "+read);
			System.out.println("Expec: "+DATA);
			
//			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
