/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ListInline {
	@Root(name="basicplain")
	public static class BasicPlainElement {

		public boolean equals(Object o) {
			return o instanceof BasicPlainElement;
		}
		public String toString() { return "BPE"; }
	}

	@Root(name="listplain")
	public static class ListInlineElement {
		
		@Element 
		private String before;
		@ElementList(type=BasicPlainElement.class,inline = true)
		private List<BasicPlainElement> children = new ArrayList<BasicPlainElement>();
		@Element 
		private String text;
		
		//-------------------------------------------------------------------
		public boolean equals(Object o) {
			if (o instanceof ListInlineElement) {
				ListInlineElement other = (ListInlineElement)o;
				if (!children.equals(other.getChildren())) return false;
				return true;
			}
			return false;
		}
		//-------------------------------------------------------------------
		public String toString() {
			return "LIL(bef="+before+",children="+children+",text="+text+")";
		}
		//-------------------------------------------------------------------
		public List<BasicPlainElement> getChildren() {
			return children;
		}
		//-------------------------------------------------------------------
		public void setChildren(List<BasicPlainElement> children) {
			this.children = children;
		}
		//-------------------------------------------------------------------
		public String getText() { return text;}
		public void setText(String text) {  this.text = text; }
		public String getBefore() { return before; }
		public void setBefore(String before) { this.before = before;}

	}
	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<listplain>"
			+SEP+"   <before>Yummy</before>"
			+SEP+"   <basicplain/>"
			+SEP+"   <basicplain/>"
			+SEP+"   <text>Dummy</text>"
			+SEP+"</listplain>"+SEP;
	final static ListInlineElement DATA = new ListInlineElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChildren().add(new BasicPlainElement());
		DATA.getChildren().add(new BasicPlainElement());
		DATA.setText("Dummy");
		DATA.setBefore("Yummy");
	}

	//-------------------------------------------------------------------
	public ListInline() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	public static void dump(Map<String,ByNameInfo>  map) {
		List<String> sorted = new ArrayList<String>(map.keySet());
		Collections.sort(sorted);
		for (String key: sorted) {
			ByNameInfo info = map.get(key);
			assertNotNull("No ByNameInfo '"+key+"'",info);
			assertNotNull("No field in info for '"+key+"'",info.field);
			String msg = String.format("  %12s  type %20s into field %s", info.elementName,info.valueType.getSimpleName(), info.field.getName());
			System.out.println(msg);
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ListInlineElement.class);
		assertNotNull(map);
		dump(map);
		assertEquals(3, map.size());
		assertTrue(map.keySet().contains("before"));
		assertTrue(map.keySet().contains("basicplain"));
		assertTrue(map.keySet().contains("text"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ListInlineElement read = serializer.read(ListInlineElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
