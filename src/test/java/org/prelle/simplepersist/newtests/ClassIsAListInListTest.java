/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ClassIsAListInListTest {

	public static enum PriorityType {
		METATYPE,
		MAGIC,
	}
	public static abstract class PriorityOption implements Comparable<PriorityOption> {
		//--------------------------------------------------------------------
		protected PriorityOption() {
		}
	}
	public enum Priority {
		A,
		B,
	}
	public static class MetaTypeOption extends PriorityOption {
		@Attribute(required=true)
		private String type;
		@Attribute(name="spec")
		private int specialAttributePoints;
		@Attribute(name="karma")
		private int additionalKarmaKost;
		//--------------------------------------------------------------------
		public MetaTypeOption() { }
		public MetaTypeOption(String type) { this.type = type; }
	    public int compareTo(PriorityOption o) {
	    	if (o instanceof MetaTypeOption) {
	    		MetaTypeOption other = (MetaTypeOption)o;
	    		int cmp = ((Integer)specialAttributePoints).compareTo(other.specialAttributePoints);
	    		if (cmp!=0) return -cmp;
	    		return type.compareTo(other.type);
	    	}
	    	return 0;
	    }
		public String toString() {return "MTO("+type+","+specialAttributePoints+","+additionalKarmaKost+")";}
	}
	public static class MagicOrResonanceOption extends PriorityOption {
		@Attribute(required=true)
		private String type;
		@Attribute
		private int value;
		//-------------------------------------------------------------------
		public MagicOrResonanceOption() { }
		public MagicOrResonanceOption(String type, int value) {this.type = type; this.value  = value;}
		public int compareTo(PriorityOption o) {
	    	if (o instanceof MagicOrResonanceOption) {
	    		MagicOrResonanceOption other = (MagicOrResonanceOption)o;
	     		return type.compareTo(other.type);
	    	}
	    	return 0;
		}
		public String toString() {return "MRO("+type+","+value+")";}
	}
	
	@SuppressWarnings("serial")
	@Root(name="priotableentry")
	@ElementListUnion({
			@ElementList(entry="metaopt", type=MetaTypeOption.class),
			@ElementList(entry="magicopt", type=MagicOrResonanceOption.class)
	})
	public static class PriorityTableEntry extends ArrayList<PriorityOption> {

		@Attribute(required=true)
		protected PriorityType type;
		@Attribute(name="prio",required=true)
		protected Priority priority;
		@Attribute(name="adj")
		protected int adjustmentPoints;

		//--------------------------------------------------------------------
		public PriorityTableEntry() {}
		public PriorityTableEntry(PriorityType type, Priority prio, int adj) {
			this.type = type;
			this.priority = prio;
			this.adjustmentPoints = adj;
		}
		public String toString() {return "PTE("+type+","+priority+","+adjustmentPoints+":"+super.toString()+")";}
	}
	@SuppressWarnings("serial")
	@Root(name="priotable")
	@ElementList(entry="priotableentry",type=PriorityTableEntry.class)
	public static class PriorityTableEntryList extends ArrayList<PriorityTableEntry> {
		public String toString() {return "PTEL("+super.toString()+")";}
	}	

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<priotable>\n" + 
					"   <priotableentry adj=\"13\" prio=\"A\" type=\"METATYPE\">\n" + 
					"      <metaopt type=\"dwarf\"/>\n" + 
					"      <metaopt type=\"ork\"/>\n" + 
					"   </priotableentry>\n" + 
					"   <priotableentry prio=\"A\" type=\"MAGIC\">\n" + 
					"      <magicopt type=\"magician\" value=\"4\"/>\n" + 
					"      <magicopt type=\"adept\" value=\"5\"/>\n" + 
					"   </priotableentry>\n" + 
					"</priotable>"+SEP;
	final static PriorityTableEntryList DATA = new PriorityTableEntryList();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		PriorityTableEntry entry1 = new PriorityTableEntry(PriorityType.METATYPE, Priority.A, 13);
		entry1.add(new MetaTypeOption("dwarf"));
		entry1.add(new MetaTypeOption("ork"));
		DATA.add(entry1);
		PriorityTableEntry entry2 = new PriorityTableEntry(PriorityType.MAGIC, Priority.A, 0);
		entry2.add(new MagicOrResonanceOption("magician",4));
		entry2.add(new MagicOrResonanceOption("adept",5));
		DATA.add(entry2);
	}

	//-------------------------------------------------------------------
	public ClassIsAListInListTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(PriorityTableEntryList.class);
		assertNotNull(map);
//		dump(map);
		assertEquals(1, map.size());
		assertTrue(map.keySet().contains("priotableentry"));

		map = ClassHelperTools.getElementMap(PriorityTableEntry.class);
		assertNotNull(map);
		System.out.println(ClassHelperTools.dump(map));
		assertEquals(2, map.size());
		assertTrue(map.keySet().contains("magicopt"));

	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			PriorityTableEntryList read = serializer.read(PriorityTableEntryList.class, XML);
			System.out.println("Found: "+read);
			System.out.println("Expec: "+DATA);
			
//			assertEquals("test", read.string);
//			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);

			assertEquals(DATA.toString(), read.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
