/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.InputStream;
import java.util.Base64;

import org.junit.Test;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class LongBase64Test {
	
	@Root(name="sr6char")
	public static class ClassWithAttributes {
		
		@Element
		private byte[] image;
		
		public ClassWithAttributes() {}
		
	}
	final static String MESS = "VO8DwPz8uH3FJJo1wqoesqYdTi9Kll/vXBn/H5z32GA3dey7PPvc5QsYwoJSwvL2NZcPPNN9Oxuxw7dowszbUhfhiQigIZ+cxVqVQwDANFlhkbG4Ekh3sVTcWStdXBW5IcQj/3KdIN2REAABkaSURBVFZVmU6nQxRFWGYJRVFYWVpmqbHYM47IUDWFbdu3c/PNN7B122a63S4vvvQaJ46f5M0TEwR+j/0hiCiytnoLSbKAImn4vo/rOz05bYofBchxHiGrSQpB4PZ08VK+B7nCt4rjmEyGz33uczzy4INkachKq0kQ54xSzRSIPJPA9fnKf/oLDr/yGr/+mU+wbfsmfuvzn6ZSN/jPf/Q/aXYdBvordJwWFy9Oc3lqAVVV2bV7Ozv37OD1I6cplUq0V7okscryUptLkwsIQplymtAVIJFyenPcC5o3EKggsayldDwXuabQP1ijWCjgdDxmG00cDwaGDW659WaGhwc5fPgwJ09cJItBVCGMY6IgZWCgwErLZmR0gJnZBoNDFTpOl+WpgE07R/nNf/cb6IrEKy8eQtMUNL3Ahs2bcdwQTYtQVRFd1FeZrkYhV6Hpuo5p9a+iM57TZXFxkdbyEk7XptNpoSkFVppNrILB0NAQy0tLFEyDeq1GdEUG3Gsp4igfWrMs6cGgKbog0Y1SbC/EiyCTJFJRwE1jHAcqMuhFC6tcIhbBC1yiMCSKY9I4oVS0kEyJvnJ5FWovFEyKJQtd1xkabvHyC6d5/tk32bR5jN/+wq9xfmKWxkIbTQfXdtm5cy233XaAcydPcvzE20iq2rOIzZe9nW4XURKoVCropkalUmKgr06z2VwVWpGmxHGCrKnoZCRJF2KBLAmQZDDNKpEfMXHpElHkoesyQRixadNadu/ezfbt2/HDgMcff4Zjx04yOTWDlysIkETQNB1Z0nsLxiw/ZBKJJHQRsgxVyLPRZUUhCUMQYgzDII5ScFJMU8MLA+QrxgNXUkdTIeOavddww0038dwzzyBLsHP3NsI45uix8+gimLqF63V46rEXOPL6YX7tM5/irnvv4JO/8iliV+Qv//tfs7DYQlUV2i2Xo28d49SpU9x3z11s3LyOUjk/gR0npL+/j1NvnyEK8tPWM2KKYQJpRmCCIIhUghQtTWgqKSgKgZswMtTPyLoR2kstlhoN7HZKsWKxbcdW9u/fz2uvvs4PH30BSwfV1ElTkTgOeNe7ttF1bILY5d733sPRo0c5dOgYSQy33noNH/y1jxK4Dk888TSaqjJQ70dV84AWq2xCkss5s94/WlMUjGJpNbIuCkVc36PdbWF3u7RarXygVBWK5QqdZo6AlSuVfC9iGFTLJUqFAtNTlzEMA1XViKKEdqu7avkqSRJxEmKqJlm0mDNbVXDlDNcPiCUo6RojQ3XiLCUlF3OJQm7Zo0gClXKFscFBFCWn2Vw5GHOkKCLyPUZHBrjzrhGe/NFzfPe7j3Lgzlv4d791P//5T/4KpyujKnDj9fvp6+vjoRNv4zgBtXqlB6/LpHGG47n0Dw1gmBqWZdHf34/v+8iSsGpU4QYuSZKg62aOXmVp77aL0DSDwPHIYpGBvkEWGpeRFZHr3vUudm+7CknVOHv2LC/9/FVOn50jJSdXyqqK+g5yrRd6q4gYyD0vrARDV5GEDFkRKJUKlIomAwN1xteMMtg/sOrL21haRPryl7/8lSs/RBTF3rUjIEoi3//+DxBl+PgnPsoddx3gwsW3UWUJ3+ugyCK1aol2y+blQ2/w9slTbN26g9/89Gdwg4CXD72S2zpmKYKUMTY+woZN6/CCkJ/+7Mc05peJA4HBgTqXLs0ThSppJpBYEXISYaQpliJhSRJamuFnKU0xI4ti1m0cYWzdOO1mh/mpOTorPrIAW7Zs5o57D9BqNnn2medpL7voukiaZnhhQF9/lXvuug1REtmweSO1eh+dbpu5+WkO3n4DH/vYR+m4Ng99/19Iwpird19NoVCkWK0SBCGCJKPKOa9IkTXUHi0/SwWSOCMMYrIsJey5LMqyjK7rvdiEIoqq4XRsRsZGieOIdrtNtVJBVRS6rTaKKFGtlPH9gJWVJp7r5/QbWeo5qISIcY7a6JqKVjAJspRYSCmXy4wMDZGSEqcRcZrfGGmcoKsKw0O5dqVSLOYGfmLeckjSLytHvSDBNBWKBYuLE/P4nsNtB67FdjqcOTmPrqX8zu/8Nooi8Y8PfI9Ox8MqlAnjEEWVsLs2kiJRrdWwLJNKpYRlWfiegyRKZELuoUUGuqahyBKu7dLpdnsbdZkwiFAEmTiM6XSajI2NcOttN7Jt+1Zc1+Wll37O08+8xPyCjW4oWIUioqzmy8Mo7MHCeUSFLEu9cNlc66SICQXL7Ml8JUpFk3LFwrJ0NE3Bbi8xPFxjfN0It932LmRRFH9JFOX5HqZucPDgQUbGBlheaXDX3Xdz9d6r0HSRz3/uj6iVVXwnpttuM9A/SGNpgdmpBf76L/8O1+vyqV//BO12l8OvHObt4yeYmJxl4uJFXDdkZHyc/qEqUxNTlA2F2ekZdEWjFUdYlkKW2NgaiCmMJQKqILCiCnRScvauKbJ+/Vq6YcLF85cJOy5JBP2DRfZfv4fhgWH+6bvfY+rCEmvH+/K+PwjxQ7ju5n3s2HM1C80mg8NDVOt9bNi8ieuv20etVOb4m6/z+NPPsnHzJvZffz2qmccUh1GEZRWIoohCua/3wecnkuu6Oe1eMVAUSDN/VaoqCgKhKOK5AZ5t0+12KVcrXPnMy+UyuqYRug5xHKNKMouLizSbOV/JMCwkScH389NW1XJNhmnqkMbYfgBCQqlaplouY0gyjXYL0qzXrwuUSwUG6v1Uq9V8nokTkiTK6RmijKZpyHKeiy4IErPzy3STBlftXMvU1DzPPvsyO/es5cCBW3nx+ZNU+8rs3HEVhw+/wvmLE2haCchVk57n4Poe4+PjSLJAsVSiUqkQeA5qz+FeSPJ5QJUlsgzsTpdOu4ksQsEqk6bg2UtEcQiZz+hIndvvuJ2hkUEOv36Yw68eYWamgeeDpouIskSr1SYB5F5EeF7wam/ZmJAkrJqRxFlGmEQYcr5MRRRwXZelpQbHjztUyhaNlQayLLJx44acavJOUZTZw6QNs8BVO3ZSKhvcfvA2ZCFj3/69/PqnP8g3v/4ItYKCoql4HZtapYpl6IwOD/DDJ54kTTPWbBjG97dQqehcuHCBk2+f5szp8wytHWHtxnGOHp4iTSQWFxZIMh0BFUUVkSLIZEhkAU8QiEQBXxBQJIWipDC0sQ8n9JicatDtRgiJjGbBpp0b2bxrHSeOnuLc6QvoKuiKjid7CFHIxu0D3HLwRrwg5AePPMnV1+3hC1/4PMWihd91eOzRRzh59AShDZWdBcZH1zDfXkFUReQswXNtNq1dS9PxiKKIJLlyqgcoSg43ZllGEksostiTLwskYYSY/SKxStdNVlZWkFQJRVCYm5tDSGKKuonvebTbbRRFoVSqoKkGUZRrsa8kuTrdDl27TdRxUBSZeqmEWSsjiSJus50fdmnelhULJkP9A1QqFbIswXMdNKOIpGaIWYKiyrBKKY/IUoFy0URUNVqdFXZes4Gnn5rn2FtzKHur6LrLzqv2IIrwxhtvEQYx5bJF0qPWNBcbmGYR3TQxLZ1C0USSJKIrbY6YIiQ5BUVIc1Wf27URMygVihiGjuNkSJKMa7fZu2s7Bw8eZHF5he9//yFOn7/A0nIuzNJMiTSF0PORFTBVEVnJuXjvNBBN09yuSBQUNE0HcplFFAXoRl5EVkFHN4ooqk4qSrQ7MaomsdBoIr5T83GlUMIwzPn/usZHPv4xvDBgcnqS6elpbr79IB/68F10uxHtro9uqJQKOhMXZ3juucdZWGzw8quHWLd+DElNCUKbvr4aU1NTXDx/CUSZkZFhNE0jTfKHyHVcTNXEtjtkskglltAFiUYBZvUEKUqpJip6ocjg6ABziw0Wllsg6bhhQmWgzs5rd4GZ8eJPX6S94rJufC1BENHtuvghfPijH2Lj1g08+9xPCFK4+pp9tLo2MzMzTE5M0JibJw2hqKhcPj/J4cOvISoKS60mkixTr9eYnZnGc20cu4Pn2gikGLqKaWhkaUy301q9PRRRQMwgChOintuZpujYto1ZLFAqlZBleVV34TgOnU6HYrFIvV7HMAzs3q2jqiqapuF5HpdmppifbyGmKetGxti4dg3VSglJIHdA77nR9NUqrB0bp17rI0vS3PhBziOQBUlEVhUkWQZBICXL9y2SiKZDEsnIsoxhRaxZU+PU8Xkmzi/SdVO2bdtCs9nk3Llz6Lq+Si0RRRHXi6jUaiRpRL2/H1VV80PXNFcVqUKaIcQpkiCiygqmYdBXq1GyCiRhhOsnpCncc89dvPd97+bsuRM89NA/c/r0aRoLuTuOIClESR4Oa5oG9b4ipZKOKMSYloiqgaqBbggYZt5KZVmev5JkKUGUrjIKuq5Ds23nAIheQBAs5hptmi0fx46RJTHHiDudFqVSgQyROAtptxYoF0Xuuv1mtFRgea7B4uVZbjl4N+vH1nDdddfy2GOPc+jnJ1irVijrYM/GvPWzo2BnXLVhC7cduItjJ07z9unzJCG8+NPDXHPzTsZHtyCqAa4ToIQDuR+RYCNlWd5alAuIQobsJwgkRGKC3K+xY8cm5tsrrCy5eEshmSjQ12ewecsY+66+jkcee5JjJy6yZ/s6vI6L67rYIdx813527Lma9nKHqYtTDFZKlEyDwOtSK5c5d/Q4W9duoDW1RGlkHLs5yyvPPoaoyVx30wE8r4nbXkKKIuS0gFQyyQoSkiRghglRx8ZLU/SSQRqbiEKMHbg4ro0f+yRKjJDlHCK1ZCClIGUpRV0D02LZd4nSmHK5iKxBt9Mhy0TSHi/JcfKbZWmpgRyH9A2X6R8cRKvVyESByHVIybD6KpTJH+5KpUKpWiFJEmw7H1YLhQJJEiH37JzSOP2lZZwgZoSxgChliIKKJFbYvmVdHl1wIaZcKbB1x2YajQYT5yZRBRUZEUNXuTQ7RblWQ1MzRobqmJpEHEf5HCT0UD0/IMuEPEEsSwmSkGp/HcfOnUwayy5StsCHP/Be9uzZz+M//BHff/AHOeQtKZhmiufFFIsSxWoRTVORFHEVTdRNjcCNyeQcyo7jOD+YVBFZyheXSQS6KiEJMmEQ51qnzM+VhoKAoUlkqkhB1+h2HOSV5gKmaVIqlbBtG0nLE2nfeOtN+gcGqPTgOatcYX5lhfNnL3DP3e/mlhsPcPNNt/PFL3yRY2+eob9qEfgOnY7H/Pw8L7/8Mnfdeyef+tSnmJ78C+amO7z2+qucPXMrpqYzMDDARLOROwemGUKUYBZUKmWTOA5JkhRRgiyFtevXsnHjejzPY2mxjev6SKoEQoxmKBy88yAXJi/xwk9eZHS0jOd5tFotVrohw+Nl9u7dSxzHdFpNJiYmuGbfXoIo5PTZs+zdtYuxdWs5/eYxCrUClZULTFdGuDjRIH3ycXbtHqZ/dDvzjoBZV1hJ59DFBKmdEScCrqKBWkLKBJRYwg+bZHGM6AdoQYSOhCQVkNJ8O9yRErIkJfQDfLuLa9skcT4zhHFE3I1QVY2Z6VlAxLU9FhaWqddLmJZO1epD0zTq9TqVahU38EHMEbWuY6NrOvV6HV3X6Xa7JElCsVhcvaUsy1jlN13ZeEuShNwTKF25Da5woq5Q1+fn5xkaGmJocITz5yfx/RBNNRGEDM9zgBTDMCgUCr/wFRZFELPVAgTQdSVPP3YdLLNIt9UmzsBxbdLE55Of+CR7r9nPA9/5Jx74ziNYBRlVVWl2QnQdxsf7ejB6zuVKsvgXqtdMXOWAXUGirrzHXBULaa+Ycm2JsNoeX9HZi6KMYUk9q1Mf6fAbL3wlI2F0dJxarR8BgemZCb785S9x9733sGfXNSCJOJ5LRMLWNRtQFQ0vCNmyaSuqqvP8T56n2/UYGR2jY3cQpYxNmzdyaWqK/dddT8G0OHv6DLMzTfoGTQbqA8zPNZi6NAtICBlIYkqpYiFkUW4y53lousq6zRtZv2kToqowPTfH5YuLdG0PQchQdLj9jpu45557+Jd/eZi3j02wccM4YRDQXOmABPtv3s+Oq7dTKJjMTU/z0x8dYteeqyhWSzTbLWr1Pmp9NVRNJxXghdNz3GbZjA2UeL2dcuTIAmpnGano4EoxerFKv1qjnJUgVrEllY4Ebhji+w74KySRn9Ow05QkE4iSDNsLaTo2K50mWRQT+QGdVgen2yFNUxRFRVFU7K7H3NwCi4srzM02ydKISqWAKMHQ0AC6orJhwwYGBgdx/PxmiZM4j39TVfr7+lcBhHc6k1wBBgShZ9eTpv9XcQiCsBpJduVmKRSL2LbNpUtzHDhwK/e99z6efeZZjhw5ilUsoSoazU6LNMu5dMODeXt4pUiEnvnDlYc0zUKCMKJYqBAECXGc4gcurfYS/+bTn+L6fTfxrW99l+/90w/IUshIiaKE9WuHGOyvY1pmD4XLdSZp9gvvLgGRNPmFbVXubXyFHdwzn5DUd5hRZD3tPSRJTBiG6JpGoVAkjOI8NauxPPuVR37wNC++9BNmZuZoLy3y3/7rn3LolZf44u98gTAMGawP01epEiUhQ5UKYRSgKLlj3datWzl79hzHj54mSUMQEwpFi/veey9P/egpisUi99x1NxcvXOTC+WkkKWTt2rUkccLszDx+ECMKIEoZmioBIXGSIKkS9cE6W6/aTqVWZfLSZS5NXWZxtotu6KRCRKmq8Vuf/yzLKy2+8b8eYHhomC1bN+I7LnPzLfoGTPbs24OoyIyNjHDm5EnePHKCrds2MzgyTBAHLK8sU631sW79OorlEsFiyqnZWT4uKnwlg2BmjqcmzvLG9DxHJ8+yvmBR0HXqAzVKg2W0IhAtk9lTSM4sqVwhSUX8OKYTBKz4HouuzazbYtZpIXkxkiihKRqqLCEiEgQhy8tLzM3NsdhoMnVpEV1XUFWJUrFIpVIGEgYG+tm6eQvVapWl5gqtVitHwgyDOI4pFUtYpoVt5yle+U5FJQhy2Nk0TTzPXb0lZFnuFY3wSw4mV4okTVN0Q8f3fS5dmuOTn/woN914Ew98+wEuX56jYJVJspR2p4VVsBgZHWJwoL76M/OHVlgtEFEUyYSINBXw/RhdM+naXRrzc3zyVz7GHXfeyt/85dd54IGHiGLoq1tUqiXWb1jLwEA/xZJF1+n2DL2TvDh6dPgrL0gREFd/Z/66YqP7ywWSf805X0EQ4XkBmqKgazqyIucGch/56Lt56snnOPzaW7z22lvUFI04DRhb28efffnLLCy2uHbffn7zs/+Wffv3MX/5HLX+Oqqucf7iRYIgYsvWtRhmbhmZCle2mCrlcoVXXz3MjdffwL59+3jhx4dYWWmysLBAqVKk2ldieWkaMhFTlnGdNn31EivNNlt2bGRoZJiEDD+K6bRdFhutvDeWJFIR9ly7m9GxNfz93/4vfAeuPrib8TVDnD11mjSDYrmAIEu4bi4Bba+0iCIolaurvl9dx2ZmYQ5BEhnftIn7//0aHvqfXR4+f4ZKRebrhRIzTsp/PTXP8zWVnx97kDfWjFLcsJ7q6Bi1chVLFDGNASoDG4kSkTiKULDB66BEGcU0o66YpIoBikFzeYWZiUs0l1dwbRvPc/A8jyDI0DWFwcFyD841aTVXWGkusX//tYyODaPJBrOzs7RaLSzLQlaVvDWWJCzLotPu5LeJoqwCBqZp9mYZZ5Uk+M5T+MqNciWo1fNyYmIYhgRBgKqqlEoK4+PjJEnK3EKjFxiUi98ASpUiZtFE1/Vf8la7oii8YqCHqCIJAhoK0zOXCYKAT3/m17n7ngP81V/9Bd/6ziO9qL8SffUag4P91Gt9NJtNXNdF19Xe39prpXrExiutlCgIBEHuEfwL69y4VyjxL71XesVL9gtjkpV2B0nVGB6s58/Nxz/xK2zasoNHfvAYbx05RxgFjK2pkWUxLx96Hd+D40fP//9dnb1uE0EUhb/xembX+Ad77bUdrx0qGij8AqSkRomoLFGmggdAFCgNJRUST0ALyhNEUAQUlAYJpMjgYIXYxomJFkOWeG3PUti7/NRTjXTvmSPNvd/h1csd1tZucPfeJgWnDBisXqnz4vk25/4PHCdP/9gD448C2XaJ/f23dLtdarUqhYLJhT9hOBjiNupIUzLTISqpSVkmwXQxr2Q7eVbcGpnLWQyZ5PTkG0fdIwI/pGzb9EcDKnWT9Y3bHPeH7L7eo1TJcO36VT68P+CwM6C8kqG+2sCyLNKZHEIL5lMNS4vheR5GQuKUKiilGI9/YhiSUj5La/Mmb3Zy3N97x+OMz8N8ikcnKR6cSZ6pX3z+1OGg3aEtYKosLmSaoGQja2WMxBmSBOY8hMmEcDIhoRdRq1pr+l9GoEMMIZAkEMsUX9NMkc0qgiCgYOfxfZ/B1z6WkjSbTZrNJsH0go/tQzzPw3Ec0uk0vV4PgGKxGDeAUgvObfwfsyzS2WwWn0UCESlpjBZd2pXIes2XWfWVSgXXdTkdnS0YxUZyCWALUCmTfCFH6pL6BzIYFWw0nbsI3pGEeoZ37pFMQqt1h/WNW2xtbfH0yTaWCa5bpbri0Gi4hFozHntkc2mkMsiIbLwbIoRgrokbObpftD8ihIYwGb+GC1xR+B9aV/yV7JzguzfBMs+p1apIKfkNkEQowvy/+jIAAAAASUVORK5CYII=";
	
	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<myclass>\n" + 
					"   <data>AQID</data>\n" + 
					"   <intNorm>500</intNorm>\n" + 
					"   <intCls>600</intCls>\n" + 
					"   <other>12</other>\n" + 
					"   <byteCls>13</byteCls>\n" + 
					"   <boolNorm>false</boolNorm>\n" + 
					"   <boolCls>true</boolCls>\n" + 
					"   <longNorm>500</longNorm>\n" + 
					"   <longCls>600</longCls>\n" + 
					"   <string>test</string>\n"+
					"   <floatNorm>1.24</floatNorm>\n" + 
					"   <floatCls>2.48</floatCls>\n" + 
					"   <uuid>3a26c167-ceb6-42a7-9242-3cc57f3b640f</uuid>\n" +
					"</myclass>"+SEP;
	final static ClassWithAttributes DATA = new ClassWithAttributes();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
	}

	//-------------------------------------------------------------------
	public LongBase64Test() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			InputStream in = ClassLoader.getSystemResourceAsStream("Nina.xml");
			assertNotNull(in);
			ClassWithAttributes read = serializer.read(ClassWithAttributes.class, in);
			
//			System.out.println(read);
//			
//			assertEquals(500, read.intNorm);
//			assertEquals((Integer)600, read.intCls);
//			assertEquals(12, read.byteNorm);
//			assertEquals((Byte)(byte)13, read.byteCls);
//			assertEquals(500L, read.longNorm);
//			assertEquals((Long)600L, read.longCls);
//			assertEquals(Arrays.toString(new byte[] {1,2,3}), Arrays.toString(read.data));
//			assertEquals("test", read.string);
//			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);
//
//			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
