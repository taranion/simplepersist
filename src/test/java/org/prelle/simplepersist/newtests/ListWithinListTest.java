/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ListWithinListTest {

	public static interface Modification {

	}

	public static class AttributeModification implements Modification {
		@Attribute
		private String attr;
		@Attribute private int val;
		public AttributeModification() {  }
		public AttributeModification(String attr, int val) { this.attr=attr; this.val=val; }
		public boolean equals(Object o) {
			if (o instanceof AttributeModification) {
				AttributeModification other = (AttributeModification)o;
				if (val!=other.val) return false;
				if (!attr.equals(other.attr)) return false;
				return true;
			}
			return false;
		}
		public String toString() { return "amod("+attr+"="+val+")"; }
	}	
	public static class SkillModification implements Modification {
		@Attribute private String ref;
		@Attribute private int value;
		public SkillModification() {  }
		public SkillModification(String attr, int val) { this.ref=attr; this.value=val; }
		public String toString() { return "smod("+ref+"="+value+")"; }
		public boolean equals(Object o) {
			if (o instanceof SkillModification) {
				SkillModification other = (SkillModification)o;
				if (value!=other.value) return false;
				if (!ref.equals(other.ref)) return false;
				return true;
			}
			return false;
		}
	}	
	public static class MastershipModification implements Modification {	}
	public static class PointsModification implements Modification {	}

	@Root(name = "selmod")
	public static class ModificationChoice implements Modification {

		@ElementListUnion({
			@ElementList(entry="attrmod", type=AttributeModification.class),
			@ElementList(entry="mastermod", type=MastershipModification.class),
			@ElementList(entry="pointsmod", type=PointsModification.class),
			@ElementList(entry="skillmod", type=SkillModification.class),
		})
		protected List<Modification> optionList;
		@Attribute(name="num",required=false)
		protected int numberOfChoices; 
		public ModificationChoice() {  }
		public ModificationChoice(int noOfChoices) {
			optionList   = new ArrayList<Modification>();
			this.numberOfChoices = noOfChoices;
		}
	    public void add(Modification mod) { optionList.add(mod);}
		public String toString() { return "choi("+numberOfChoices+"="+optionList+")"; }
		public boolean equals(Object o) {
			if (o instanceof ModificationChoice) {
				ModificationChoice other = (ModificationChoice)o;
				if (numberOfChoices!=other.numberOfChoices) return false;
				return optionList.equals(other.optionList);
			}
			return false;
		}
	}

	@Root(name="creaturemodule")
	public static class CreatureModule  {

		@ElementListUnion(inline=true,value={
				@ElementList(entry="attrmod", type=AttributeModification.class),
				@ElementList(entry="mastermod", type=MastershipModification.class),
				@ElementList(entry="selmod", type=ModificationChoice.class),
		})
		private List<Modification> modifications;

		public CreatureModule() {
			modifications = new ArrayList();
		}

		public boolean equals(Object o) {
			if (!(o instanceof CreatureModule))
				return false;
			CreatureModule other = (CreatureModule)o;
			return modifications.equals(other.modifications);
			//			return String.valueOf(type).equals(String.valueOf(other.type));
		}
		public String toString() {
			return "CreatMod("+modifications+")";
		}
		public void add(Modification val) { modifications.add(val); }
	}


	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<creaturemodule>\n" + 
			"   <attrmod attr=\"AGILITY\" val=\"2\"/>\n" + 
			"   <selmod num=\"1\">\n" + 
			"      <skillmod ref=\"dexterity\" value=\"10\"/>\n" + 
			"      <skillmod ref=\"performance\" value=\"10\"/>\n" + 
			"   </selmod>\n" + 
			"</creaturemodule>"+SEP;
	final static CreatureModule DATA = new CreatureModule();

	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.add(new AttributeModification("AGILITY",2));
		ModificationChoice selmod = new ModificationChoice(1);
		selmod.add(new SkillModification("dexterity", 10));
		selmod.add(new SkillModification("performance", 10));
		DATA.add(selmod);
	}

	//-------------------------------------------------------------------
	public ListWithinListTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(CreatureModule.class);
		assertNotNull(map);
		System.out.println(ClassHelperTools.dump(map));
		assertEquals(3, map.size());
		assertTrue(map.keySet().contains("attrmod"));
		assertTrue(map.keySet().contains("mastermod"));
		assertTrue(map.keySet().contains("selmod"));

		assertFalse(map.keySet().contains("skillmod"));
		assertFalse(map.keySet().contains("pointsmod"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());

			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			CreatureModule read = serializer.read(CreatureModule.class, XML);
			System.out.println("Found: "+read);
			System.out.println("Expec: "+DATA);

			//			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
