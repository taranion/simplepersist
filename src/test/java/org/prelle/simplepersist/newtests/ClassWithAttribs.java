/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ClassWithAttribs {
	
	@Root(name="myclass")
	public static class ClassWithAttributes {
		
		@Attribute
		private byte[] data;
		@Attribute
		private int intNorm;
		@Attribute
		private Integer intCls;
		@Attribute(name = "other")
		private byte byteNorm;
		@Attribute
		private Byte byteCls;
		@Attribute
		private boolean boolNorm;
		@Attribute
		private Boolean boolCls;
		@Attribute
		private long longNorm;
		@Attribute
		private Long longCls;
		@Attribute
		private String string;
		@Attribute
		private String string2;
		@Attribute
		private float floatNorm;
		@Attribute
		private Float floatCls;
		@Attribute
		private UUID uuid;
		
		public ClassWithAttributes() {}
		
		public boolean equals(Object o) {
			if (!(o instanceof ClassWithAttributes))
				return false;
			ClassWithAttributes other = (ClassWithAttributes)o;
			if (intNorm!=other.intNorm) return false;
			if (!intCls.equals(other.intCls)) return false;
			if (byteNorm!=other.byteNorm) return false;
			if (!byteCls.equals(other.byteCls)) return false;
			if (boolNorm!=other.boolNorm) return false;
			if (!boolCls.equals(other.boolCls)) return false;
			if (longNorm!=other.longNorm) return false;
			if (!longCls.equals(other.longCls)) return false;
			if (!string.equals(other.string)) return false;
			if (!Arrays.equals(data,other.data)) return false;
			if (floatNorm!=other.floatNorm) return false;
			if (!floatCls.equals(other.floatCls)) return false;
			if (!uuid.equals(other.uuid)) return false;
			return true;
		}
		public String toString() {
			return "ClassWithAttribs("+boolCls+","+boolNorm+","+byteCls+","+byteNorm+","+data+","+intCls+","+intNorm+","+longCls+","+longNorm+","+string+","+floatNorm+","+floatCls+")";
		}
	}
	

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<myclass boolCls=\"true\" byteCls=\"13\" data=\"AQID\" floatCls=\"2.48\" floatNorm=\"1.24\" intCls=\"600\" intNorm=\"500\" longCls=\"600\" longNorm=\"500\" other=\"12\" string=\"test\" string2=\"H&lt;äll&amp;o&gt; W&#x0022;ölt\" uuid=\"3a26c167-ceb6-42a7-9242-3cc57f3b640f\"/>"+SEP;
	final static ClassWithAttributes DATA = new ClassWithAttributes();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.intNorm = 500;
		DATA.intCls = 600;
		DATA.byteNorm = 12;
		DATA.byteCls = 13;
		DATA.boolNorm= false;
		DATA.boolCls = true;
		DATA.longNorm = 500L;
		DATA.longCls = 600L;
		DATA.string = "test";
		DATA.string2 = "H<äll&o> W\"ölt";
		DATA.data  = new byte[] {1,2,3};
		DATA.floatNorm = 1.24f;
		DATA.floatCls = 2.48f;
		DATA.uuid = UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f");
	}

	//-------------------------------------------------------------------
	public ClassWithAttribs() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	public static void dump(Map<String,ByNameInfo>  map) {
		List<String> sorted = new ArrayList<String>(map.keySet());
		Collections.sort(sorted);
		for (String key: sorted) {
			ByNameInfo info = map.get(key);
			assertNotNull("No ByNameInfo '"+key+"'",info);
			assertNotNull("No field in info for '"+key+"'",info.field);
			String msg = String.format("  %12s  type %20s into field %s", info.elementName,info.valueType.getSimpleName(), info.field.getName());
			System.out.println(msg);
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ClassWithAttributes.class);
		assertNotNull(map);
//		dump(map);
		assertEquals(0, map.size());
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ClassWithAttributes read = serializer.read(ClassWithAttributes.class, XML);
			System.out.println(read);
			
			assertEquals(500, read.intNorm);
			assertEquals((Integer)600, read.intCls);
			assertEquals(12, read.byteNorm);
			assertEquals((Byte)(byte)13, read.byteCls);
			assertEquals(500L, read.longNorm);
			assertEquals((Long)600L, read.longCls);
			assertEquals(Arrays.toString(new byte[] {1,2,3}), Arrays.toString(read.data));
			assertEquals("test", read.string);
			assertEquals("H<äll&o> W\"ölt", read.string2);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
