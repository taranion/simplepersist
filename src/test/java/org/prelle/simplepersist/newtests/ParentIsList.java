/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.UUID;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.newtests.ClassWithAttribs.ClassWithAttributes;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ParentIsList {
	
	public static class ItemClass {
		@Attribute
		private int number;
		public ItemClass(int i) { this.number = i;}
		public ItemClass() { 		}
		public String toString() { return ""+number; }
		public boolean equals(Object o) {
			if (o instanceof ItemClass)
				return number==((ItemClass)o).number;
			return false;
		}
	}
	
	@Root(name="myclass")
	@ElementList(entry="item",type = ItemClass.class)
	public static class TestClass extends ArrayList<ItemClass> {
		
		@Element
		private String string;
		
		public TestClass() {}
		
		public boolean equals(Object o) {
			if (!(o instanceof TestClass))
				return false;
			TestClass other = (TestClass)o;
			if (!string.equals(other.string)) return false;
			return super.equals(other);
		}
		public String toString() {
			return "TestClass("+string+","+super.toString()+")";
		}
	}
	

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<myclass>\n" + 
					"   <string>test</string>\n" + 
					"   <item number=\"5\"/>\n" + 
					"   <item number=\"3\"/>\n" + 
					"   <item number=\"1\"/>\n" +
					"</myclass>"+SEP;
	final static TestClass DATA = new TestClass();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.string = "test";
		DATA.add(new ItemClass(5));
		DATA.add(new ItemClass(3));
		DATA.add(new ItemClass(1));
	}

	//-------------------------------------------------------------------
	public ParentIsList() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void testClassHelper() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(TestClass.class);
		assertNotNull(map);
//		dump(map);
		assertEquals(2, map.size());
		assertTrue(map.keySet().contains("string"));

		assertFalse(map.keySet().contains("byteNorm"));
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			TestClass read = serializer.read(TestClass.class, XML);
			System.out.println(read);
			
			assertEquals("test", read.string);
//			assertEquals(UUID.fromString("3a26c167-ceb6-42a7-9242-3cc57f3b640f"), read.uuid);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
