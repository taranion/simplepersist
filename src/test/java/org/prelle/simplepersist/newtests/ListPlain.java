/**
 * 
 */
package org.prelle.simplepersist.newtests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.Serializer;

/**
 * @author prelle
 *
 */
public class ListPlain {
	@Root(name="basicplain")
	public static class BasicPlainElement {

		public boolean equals(Object o) {
			return o instanceof BasicPlainElement;
		}
	}

	@Root(name="listplain")
	public static class ListPlainElement {
		
		@Element 
		private String before;
		@ElementList(type=BasicPlainElement.class)
		private List<BasicPlainElement> children = new ArrayList<BasicPlainElement>();
		@Element 
		private String text;
		
		//-------------------------------------------------------------------
		public boolean equals(Object o) {
			if (o instanceof ListPlainElement) {
				ListPlainElement other = (ListPlainElement)o;
				if (!children.equals(other.getChildren())) return false;
				return true;
			}
			return false;
		}
		//-------------------------------------------------------------------
		public String toString() {
			return "LPL(bef="+before+",children="+children+",text="+text+")";
		}
		//-------------------------------------------------------------------
		public List<BasicPlainElement> getChildren() {
			return children;
		}
		//-------------------------------------------------------------------
		public void setChildren(List<BasicPlainElement> children) {
			this.children = children;
		}
		//-------------------------------------------------------------------
		public String getText() { return text;}
		public void setText(String text) {  this.text = text; }
		public String getBefore() { return before; }
		public void setBefore(String before) { this.before = before;}

	}
	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<listplain>"
			+SEP+"   <before>Yummy</before>"
			+SEP+"   <children>"
			+SEP+"      <basicplain/>"
			+SEP+"      <basicplain/>"
			+SEP+"   </children>"
			+SEP+"   <text>Dummy</text>"
			+SEP+"</listplain>"+SEP;
	final static ListPlainElement DATA = new ListPlainElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChildren().add(new BasicPlainElement());
		DATA.getChildren().add(new BasicPlainElement());
		DATA.setText("Dummy");
		DATA.setBefore("Yummy");
	}

	//-------------------------------------------------------------------
	public ListPlain() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ListPlainElement read = serializer.read(ListPlainElement.class, XML);
			System.out.println("FOUND: "+read);
			System.out.println("EXPEC: "+DATA);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
