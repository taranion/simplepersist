/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.StringReader;
import java.util.List;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;

import org.junit.Test;
import org.prelle.simplepersist.example.BasicAttrElement;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.DerivedElement;
import org.prelle.simplepersist.example.FoldedListElement;
import org.prelle.simplepersist.example.FoldedPlainElement;
import org.prelle.simplepersist.example.ItemDataElement;
import org.prelle.simplepersist.example.ItemElement;
import org.prelle.simplepersist.example.ListInlineElement;
import org.prelle.simplepersist.example.ListPlainElement;
import org.prelle.simplepersist.example.MappedElement;
import org.prelle.simplepersist.example.ShieldElement;
import org.prelle.simplepersist.example.SimpleElement;
import org.prelle.simplepersist.example.ValueConverterElement;
import org.prelle.simplepersist.example.WeaponElement;
import org.prelle.simplepersist.unmarshal.Unmarshaller2;

/**
 * @author prelle
 *
 */
public class UnmarshallerTest {
	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";

	private Unmarshaller2 m = new Unmarshaller2();

	//-------------------------------------------------------------------
	public UnmarshallerTest() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void testBasicPlain() throws Exception {
		String XML = HEAD
				+SEP+"<basicplain/>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		BasicPlainElement data = m.read(BasicPlainElement.class, evRd);
		assertNotNull(data);
		
	}

	//-------------------------------------------------------------------
	@Test
	public void testBasicAttr() throws Exception {
		String XML = HEAD
				+SEP+"<basicattr text=\"Foo\" zahl=\"5\"/>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		BasicAttrElement data = m.read(BasicAttrElement.class, evRd);
		assertNotNull(data);
		assertEquals("Foo", data.getText());
		assertEquals(5, data.getZahl());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimple() throws Exception {
		String XML = HEAD
				+SEP+"<simple zahl=\"5\">"
				+SEP+"   <text>Foo</text>"
				+SEP+"</simple>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		SimpleElement data = m.read(SimpleElement.class, evRd);
		assertNotNull(data);
		assertEquals(5, data.getZahl());
		assertEquals("Foo", data.getText());
	}

	//-------------------------------------------------------------------
	@Test
	public void testDerived() throws Exception {
		String XML = HEAD
				+SEP+"<derived derzahl=\"6\" zahl=\"5\">"
				+SEP+"   <dertxt>Blah</dertxt>"
				+SEP+"   <text>Foo</text>"
				+SEP+"</derived>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		DerivedElement data = m.read(DerivedElement.class, evRd);
		assertNotNull(data);
		assertEquals(5, data.getZahl());
		assertEquals(6, data.getDerZahl());
		assertEquals("Foo", data.getText());
		assertEquals("Blah", data.getDerText());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFoldedPlain() throws Exception {
		String XML = HEAD
				+SEP+"<foldedplain>"
				+SEP+"   <child/>"
				+SEP+"</foldedplain>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		FoldedPlainElement data = m.read(FoldedPlainElement.class, evRd);
		assertNotNull(data);
		assertNotNull(data.getChild());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListPlain() throws Exception {
		String XML = HEAD
				+SEP+"<listplain>"
				+SEP+"   <before>Yummy</before>"
				+SEP+"   <children>"
				+SEP+"      <basicplain/>"
				+SEP+"      <basicplain/>"
				+SEP+"   </children>"
				+SEP+"   <text>Dummy</text>"
				+SEP+"</listplain>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		ListPlainElement data = m.read(ListPlainElement.class, evRd);
		assertNotNull(data);
		assertEquals("Yummy", data.getBefore());
		assertEquals("Dummy", data.getText());
		List<BasicPlainElement> list = data.getChildren();
		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals(BasicPlainElement.class, list.get(0).getClass());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListInline() throws Exception {
		String XML = HEAD
				+SEP+"<listinline>"
				+SEP+"   <before>Yummy</before>"
				+SEP+"   <basicplain/>"
				+SEP+"   <basicplain/>"
				+SEP+"   <text>Dummy</text>"
				+SEP+"</listinline>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		ListInlineElement data = m.read(ListInlineElement.class, evRd);
		assertNotNull(data);
		assertEquals("Yummy", data.getBefore());
		assertEquals("Dummy", data.getText());
		List<BasicPlainElement> list = data.getChildren();
		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals(BasicPlainElement.class, list.get(0).getClass());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFoldedList() throws Exception {
		String XML = HEAD
				+SEP+"<foldedlist>"
				+SEP+"   <text>Dummy</text>"
				+SEP+"   <basicplain/>"
				+SEP+"   <basicplain/>"
				+SEP+"</foldedlist>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		FoldedListElement data = m.read(FoldedListElement.class, evRd);
		assertNotNull(data);
		assertEquals("Dummy", data.getText());
		List<BasicPlainElement> list = data;
		assertNotNull(list);
		assertEquals(2, list.size());
		assertEquals(BasicPlainElement.class, list.get(0).getClass());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMapped() throws Exception {
		String XML = HEAD
				+SEP+"<mapped>"
				+SEP+"   <children>"
				+SEP+"      <element>"
				+SEP+"         <key>key1</key>"
				+SEP+"         <value type=\"example.BasicPlainElement\">"
				+SEP+"            <basicplain/>"
				+SEP+"         </value>"
				+SEP+"      </element>"
				+SEP+"      <element>"
				+SEP+"         <key>key2</key>"
				+SEP+"         <value type=\"example.BasicPlainElement\">"
				+SEP+"            <basicplain/>"
				+SEP+"         </value>"
				+SEP+"      </element>"
				+SEP+"   </children>"
				+SEP+"</mapped>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		MappedElement data = m.read(MappedElement.class, evRd);
		assertNotNull(data);
		assertNotNull(data.getChild());
		Map<String,BasicPlainElement> map = data.getChild();
		assertEquals(2, map.size());
		assertTrue(map.containsKey("key1"));
		assertTrue(map.containsKey("key2"));
		assertEquals(BasicPlainElement.class, map.get("key1").getClass());
		assertEquals(BasicPlainElement.class, map.get("key2").getClass());
	}

	//-------------------------------------------------------------------
	@Test
	public void testWrappedInlineList() throws Exception {
		String XML = HEAD
				+SEP+"<item weight=\"2\">"
				+SEP+"   <text>Dummy</text>"
				+SEP+"   <weapon damage=\"2\" type=\"1\"/>"
				+SEP+"   <shield handicap=\"1\" type=\"2\"/>"
				+SEP+"</item>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		ItemElement data = m.read(ItemElement.class, evRd);
		assertNotNull(data);
		assertEquals(2, data.getWeight());
		assertEquals("Dummy", data.getText());

		List<ItemDataElement> list = data.getTypeData();
		assertEquals(2, list.size());
		assertEquals(WeaponElement.class, list.get(0).getClass());
		assertEquals(ShieldElement.class, list.get(1).getClass());
		
		WeaponElement weapon = (WeaponElement)list.get(0);
		ShieldElement shield = (ShieldElement)list.get(1);
		assertEquals(2, weapon.getDamage());
		assertEquals(1, weapon.getType());
		assertEquals(1, shield.getHandicap());
		assertEquals(2, shield.getType());
	}

	//-------------------------------------------------------------------
	@Test
	public void testConverter() throws Exception {
		String XML = HEAD
				+SEP+"<convert zahl=\"8\"/>"+SEP;

		XMLInputFactory inputFactory = XMLInputFactory.newInstance();
		XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( XML ) );

		ValueConverterElement data = m.read(ValueConverterElement.class, evRd);
		assertNotNull(data);
		assertEquals(4, data.getZahl());
//		assertEquals((char)'F', (char)data.getText().get(0));
//		assertEquals((char)'o', (char)data.getText().get(1));
//		assertEquals((char)'o', (char)data.getText().get(2));
	}

}
