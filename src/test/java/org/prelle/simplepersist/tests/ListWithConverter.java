/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.ListWithConverterElement;

/**
 * @author prelle
 *
 */
public class ListWithConverter {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<convlist>"
			+SEP+"   <children>"
			+SEP+"      <twoparam fact1=\"Hallo\" fact2=\"Welt\"/>"
			+SEP+"      <twoparam fact1=\"Alles\" fact2=\"gut?\"/>"
			+SEP+"   </children>"
			+SEP+"</convlist>"+SEP;
	final static ListWithConverterElement DATA = new ListWithConverterElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChildren().add("Hallo Welt");
		DATA.getChildren().add("Alles gut?");
	}

	//-------------------------------------------------------------------
	public ListWithConverter() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

//	//-------------------------------------------------------------------
//	@Test
//	public void deserialize() {
//		try {
//			ListWithConverterElement read = serializer.read(ListWithConverterElement.class, XML);
//			System.out.println(read);
//			
//			assertEquals(DATA, read);
//		} catch (Exception e) {
//			e.printStackTrace();
//			fail(e.toString());
//		}
//	}

}
