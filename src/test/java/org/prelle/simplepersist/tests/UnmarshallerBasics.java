/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.EverythingElement;
import org.prelle.simplepersist.example.ItemList;
import org.prelle.simplepersist.example.ListPlainElement;
import org.prelle.simplepersist.example.ShieldElement;
import org.prelle.simplepersist.example.WeaponElement;
import org.prelle.simplepersist.unmarshal.ByNameInfo;
import org.prelle.simplepersist.unmarshal.Unmarshaller;

/**
 * @author prelle
 *
 */
public class UnmarshallerBasics {

	//-------------------------------------------------------------------
	public UnmarshallerBasics() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void foo() throws Exception {
		Map<String,ByNameInfo> ret = Unmarshaller.determineExpectedElements(ListPlainElement.class);
		assertTrue(ret.containsKey("before"));
		assertTrue(ret.containsKey("children"));
		assertTrue(ret.containsKey("text"));
	}

	//-------------------------------------------------------------------
	@Test
	public void detectAttribute1() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("textAttrOhne");
		
		ByNameInfo info = Unmarshaller.determineExpectationForAttributeField(field);
		assertNotNull(info);
		assertEquals("textAttrOhne", info.elementName);
		assertSame(int.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectAttribute2() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("textAttr");
		
		ByNameInfo info = Unmarshaller.determineExpectationForAttributeField(field);
		assertNotNull(info);
		assertEquals("attr", info.elementName);
		assertSame(String.class, info.cls);
		assertSame(field, info.field);
		assertEquals(true, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectElement1() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("textElemOhne");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertEquals("textElemOhne", info.elementName);
		assertSame(String.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectElement2() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("textElem");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertEquals("text", info.elementName);
		assertSame(String.class, info.cls);
		assertSame(field, info.field);
		assertEquals(true, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectList1() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("list1");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertEquals("list1", info.elementName);
		assertSame(ArrayList.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectList2() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("list2");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertEquals("list2", info.elementName);
		assertSame(ArrayList.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertNull(info.listInstance);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectInlineList1() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("inline1");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertNotNull(info.listInstance);
		assertEquals("basicplain", info.elementName);
		assertSame(BasicPlainElement.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertTrue(info.isInlineList);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectInlineList2() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("inline2");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(1,ret.size());
		ByNameInfo info = ret.get(0);
		assertNotNull(info.listInstance);
		assertEquals("mybasic", info.elementName);
		assertSame(BasicPlainElement.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		assertTrue(info.isInlineList);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectInlineListUnion() throws Exception {
		Field field = EverythingElement.class.getDeclaredField("union");
		
		List<ByNameInfo> ret = Unmarshaller.determineExpectationForElementField(field);
		assertEquals(2,ret.size());
		
		ByNameInfo info = ret.get(0);
		assertNotNull(info.listInstance);
		assertEquals("weapon", info.elementName);
		assertSame(WeaponElement.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
		
		info = ret.get(1);
		assertNotNull(info.listInstance);
		assertEquals("shield", info.elementName);
		assertSame(ShieldElement.class, info.cls);
		assertSame(field, info.field);
		assertEquals(false, info.required);
//		assertTrue(info.isInlineList);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectClassAttributes() throws Exception {
		Field field = ItemList.class.getDeclaredField("zahl");

		List<ByNameInfo> ret = Unmarshaller.determineExpectationForClassAttributes(ItemList.class);
		assertEquals(1,ret.size());
		
		ByNameInfo info = ret.get(0);
		assertNull(info.listInstance);
		assertEquals("zahl", info.elementName);
		assertSame(int.class, info.cls);
		assertEquals(field, info.field);
		assertEquals(false, info.required);
	}

	//-------------------------------------------------------------------
	@Test
	public void detectClassElements() throws Exception {
		Field field = ItemList.class.getDeclaredField("text");

		List<ByNameInfo> ret = Unmarshaller.determineExpectationForClassElements(ItemList.class);
		assertEquals(3,ret.size());
		
		ByNameInfo info = ret.get(0);
		assertNotNull(info.listInstance);
		assertEquals("weapon", info.elementName);
		assertSame(WeaponElement.class, info.cls);
		assertNull(info.field);
		assertEquals(false, info.required);
		
		info = ret.get(1);
		assertNotNull(info.listInstance);
		assertEquals("shield", info.elementName);
		assertSame(ShieldElement.class, info.cls);
		assertNull(info.field);
		assertEquals(false, info.required);
		
		info = ret.get(2);
		assertNull(info.listInstance);
		assertEquals("text", info.elementName);
		assertSame(String.class, info.cls);
		assertEquals(field, info.field);
		assertEquals(false, info.required);
	}

}
