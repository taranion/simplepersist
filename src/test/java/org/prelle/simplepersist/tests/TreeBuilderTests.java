/**
 *
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import org.prelle.simplepersist.MapEntry;
import org.prelle.simplepersist.example.BasicAttrElement;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.BinaryElement;
import org.prelle.simplepersist.example.DerivedElement;
import org.prelle.simplepersist.example.FoldedListElement;
import org.prelle.simplepersist.example.FoldedPlainElement;
import org.prelle.simplepersist.example.ItemElement;
import org.prelle.simplepersist.example.ListInlineElement;
import org.prelle.simplepersist.example.ListPlainElement;
import org.prelle.simplepersist.example.ListUnionClassElement;
import org.prelle.simplepersist.example.ListUnionClassFieldElement;
import org.prelle.simplepersist.example.ListWithConverterElement;
import org.prelle.simplepersist.example.MappedElement;
import org.prelle.simplepersist.example.MultiplyConverter;
import org.prelle.simplepersist.example.SimpleElement;
import org.prelle.simplepersist.example.SimpleEnumElement;
import org.prelle.simplepersist.example.ValueConverterElement;
import org.prelle.simplepersist.example.WeaponElement;
import org.prelle.simplepersist.example.SimpleEnumElement.MyEnumAttribute;
import org.prelle.simplepersist.example.SimpleEnumElement.MyEnumElement;
import org.prelle.simplepersist.unmarshal.TreeBuilder;
import org.prelle.simplepersist.unmarshal.XMLBinaryLeaf;
import org.prelle.simplepersist.unmarshal.XMLTextLeaf;
import org.prelle.simplepersist.unmarshal.XMLTreeAttributeLeaf;
import org.prelle.simplepersist.unmarshal.XMLTreeNode;

/**
 * @author prelle
 *
 */
public class TreeBuilderTests {

	//-------------------------------------------------------------------
	public TreeBuilderTests() throws Exception {
	}

	//-------------------------------------------------------------------
	@Test
	public void testBasicPlain() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(BasicPlainElement.class);
		assertNotNull(ret);
		assertEquals("basicplain",ret.getName());
		assertNull(ret.getParent());

		assertTrue(ret.getChildren().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testBasicAttr() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(BasicAttrElement.class);
		assertNotNull(ret);
		assertEquals("basicattr",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("text"));
		assertNotNull(ret.getChild("zahl"));
		assertEquals(2, ret.getChildren().size());

		Field fieldText = BasicAttrElement.class.getDeclaredField("text");
		Field fieldZahl = BasicAttrElement.class.getDeclaredField("zahl");

		XMLTreeAttributeLeaf attText = (XMLTreeAttributeLeaf)ret.getChild("text");
		XMLTreeAttributeLeaf attZahl = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(attText);
		assertNotNull(attZahl);
		assertEquals(String.class, attText.getType());
		assertEquals(int.class, attZahl.getType());
		assertEquals(fieldText, attText.getField());
		assertEquals(fieldZahl, attZahl.getField());
		assertEquals(ret, attText.getParent());
		assertEquals(ret, attZahl.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimple() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(SimpleElement.class);
		assertNotNull(ret);
		assertEquals("simple",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("text"));
		assertNotNull(ret.getChild("zahl"));
		assertEquals(3, ret.getChildren().size());

		Field fieldText = SimpleElement.class.getDeclaredField("text");
		Field fieldZahl = SimpleElement.class.getDeclaredField("zahl");

		XMLTextLeaf elemText = (XMLTextLeaf)ret.getChild("text");
		XMLTreeAttributeLeaf attZahl = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(elemText);
		assertNotNull(attZahl);
		assertEquals(String.class, elemText.getType());
		assertEquals(int.class, attZahl.getType());
		assertEquals(fieldText, elemText.getField());
		assertEquals(fieldZahl, attZahl.getField());
		assertEquals(ret, elemText.getParent());
		assertEquals(ret, attZahl.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testDerived() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(DerivedElement.class);

		assertNotNull(ret);
		assertEquals("derived",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("text"));
		assertNotNull(ret.getChild("zahl"));
		assertNotNull(ret.getChild("dertxt"));
		assertNotNull(ret.getChild("derzahl"));
		assertEquals(5, ret.getChildren().size());

		XMLTextLeaf elemText = (XMLTextLeaf)ret.getChild("text");
		XMLTreeAttributeLeaf attZahl = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(elemText);
		assertNotNull(attZahl);
		assertEquals(String.class, elemText.getType());
		assertEquals(int.class, attZahl.getType());
		assertEquals(ret, elemText.getParent());
		assertEquals(ret, attZahl.getParent());
		XMLTextLeaf elemDerText = (XMLTextLeaf)ret.getChild("dertxt");
		XMLTreeAttributeLeaf attDerZahl = (XMLTreeAttributeLeaf)ret.getChild("derzahl");
		assertNotNull(elemDerText);
		assertNotNull(attDerZahl);
		assertEquals(String.class, elemDerText.getType());
		assertEquals(int.class, attDerZahl.getType());
		assertEquals(ret, elemDerText.getParent());
		assertEquals(ret, attDerZahl.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFoldedPlain() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(FoldedPlainElement.class);

		assertNotNull(ret);
		assertEquals("foldedplain",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("child"));
		assertEquals(1, ret.getChildren().size());

		XMLTreeNode elemText = (XMLTreeNode)ret.getChild("child");
		assertNotNull(elemText);
		assertEquals(BasicPlainElement.class, elemText.getType());
		assertEquals(ret, elemText.getParent());
		assertTrue(elemText.getChildren().isEmpty());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListPlain() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ListPlainElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("listplain",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("before"));
		assertNotNull(ret.getChild("children"));
		assertNotNull(ret.getChild("text"));
		assertEquals(3, ret.getChildren().size());

		Field fieldBefor = ListPlainElement.class.getDeclaredField("before");
		Field fieldText  = ListPlainElement.class.getDeclaredField("text");
		Field fieldChild = ListPlainElement.class.getDeclaredField("children");

		XMLTextLeaf before = (XMLTextLeaf)ret.getChild("before");
		XMLTextLeaf text   = (XMLTextLeaf)ret.getChild("text");
		assertEquals(String.class, before.getType());
		assertEquals(String.class, text  .getType());
		assertEquals(ret, before.getParent());
		assertEquals(ret, text.getParent());
		assertEquals(fieldBefor, before.getField());
		assertEquals(fieldText, text.getField());

		XMLTreeNode children = (XMLTreeNode)ret.getChild("children");
		assertEquals(ArrayList.class, children.getType());
		assertEquals(ret, children.getParent());
		assertEquals(fieldChild, children.getField());
		assertFalse(children.isInlineList());


	}

	//-------------------------------------------------------------------
	@Test
	public void testListInline() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ListInlineElement.class);
//
//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("listinline",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("before"));
		assertNotNull(ret.getChild("basicplain"));
		assertNotNull(ret.getChild("text"));
		assertEquals(3, ret.getChildren().size());

		Field fieldBefor = ListInlineElement.class.getDeclaredField("before");
		Field fieldText  = ListInlineElement.class.getDeclaredField("text");
		Field fieldChild = ListInlineElement.class.getDeclaredField("children");

		XMLTextLeaf before = (XMLTextLeaf)ret.getChild("before");
		XMLTextLeaf text   = (XMLTextLeaf)ret.getChild("text");
		assertEquals(String.class, before.getType());
		assertEquals(String.class, text  .getType());
		assertEquals(ret, before.getParent());
		assertEquals(ret, text.getParent());
		assertEquals(fieldBefor, before.getField());
		assertEquals(fieldText, text.getField());

		XMLTreeNode basic = (XMLTreeNode)ret.getChild("basicplain");
		assertEquals(BasicPlainElement.class, basic.getType());
		assertEquals(ret, basic.getParent());
		assertEquals(fieldChild, basic.getField());
		assertTrue(basic.isInlineList());
	}

	//-------------------------------------------------------------------
	@Test
	public void testFoldedList() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(FoldedListElement.class);
//
//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("foldedlist",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("basicplain"));
		assertNotNull(ret.getChild("text"));
		assertEquals(2, ret.getChildren().size());

		XMLTextLeaf text   = (XMLTextLeaf)ret.getChild("text");
		assertEquals(String.class, text  .getType());
		assertEquals(ret, text.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testMapped() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(MappedElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("mapped",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("children"));
		assertEquals(1, ret.getChildren().size());

		XMLTreeNode children = (XMLTreeNode)ret.getChild("children");
		assertEquals(HashMap.class, children.getType());
		assertEquals(ret, children.getParent());

		XMLTreeNode element = (XMLTreeNode)children.getChild("element");
		assertNotNull(element);
		assertEquals(MapEntry.class, element.getType());
		assertEquals(children, element.getParent());

		assertNotNull(element.getChild("key"));
		assertNotNull(element.getChild("value"));
		assertEquals(2, element.getChildren().size());

		Field fieldKey = MapEntry.class.getDeclaredField("key");
		Field fieldVal = MapEntry.class.getDeclaredField("value");

		XMLTextLeaf key = (XMLTextLeaf)element.getChild("key");
		XMLTreeNode val = (XMLTreeNode)element.getChild("value");
		assertEquals(String.class, key.getType());
		assertEquals(BasicPlainElement.class, val.getType());
		assertEquals(fieldKey, key.getField());
		assertEquals(fieldVal, val.getField());

		assertNotNull(val.getChild("basicplain"));
		XMLTreeNode basic = (XMLTreeNode)val.getChild("basicplain");
		assertEquals(BasicPlainElement.class, basic.getType());
	}

	//-------------------------------------------------------------------
	@Test
	public void testItemInline() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ItemElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("item",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("weight"));
		assertNotNull(ret.getChild("text"));
		assertNotNull(ret.getChild("weapon"));
		assertNotNull(ret.getChild("shield"));
		assertEquals(5, ret.getChildren().size());

		XMLTreeAttributeLeaf attWeight = (XMLTreeAttributeLeaf)ret.getChild("weight");
		assertNotNull(attWeight);
		assertEquals(int.class, attWeight.getType());
		assertEquals(ret, attWeight.getParent());

		XMLTextLeaf elemText = (XMLTextLeaf)ret.getChild("text");
		assertNotNull(elemText);
		assertEquals(String.class, elemText.getType());
		assertEquals(ret, elemText.getParent());

		XMLTreeNode weapon = (XMLTreeNode)ret.getChild("weapon");
		assertNotNull(weapon);
		assertEquals(WeaponElement.class, weapon.getType());
		assertEquals(ret, weapon.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListUnionClass() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ListUnionClassElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("listunionclass",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("zahl"));
		assertNotNull(ret.getChild("basicattr"));
		assertNotNull(ret.getChild("basicplain"));
		assertNotNull(ret.getChild("simple"));
		assertEquals(4, ret.getChildren().size());

		XMLTreeAttributeLeaf attWeight = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(attWeight);
		assertEquals(int.class, attWeight.getType());
		assertEquals(ret, attWeight.getParent());

		XMLTreeNode bplain = (XMLTreeNode)ret.getChild("basicplain");
		assertNotNull(bplain);
		assertEquals(BasicPlainElement.class, bplain.getType());
		assertEquals(ret, bplain.getParent());

		XMLTreeNode battr = (XMLTreeNode)ret.getChild("basicattr");
		assertNotNull(battr);
		assertEquals(BasicAttrElement.class, battr.getType());
		assertEquals(ret, battr.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListUnionClassField() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ListUnionClassFieldElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("listunion",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("zahl"));
		assertNotNull(ret.getChild("children"));
		assertEquals(2, ret.getChildren().size());

		XMLTreeAttributeLeaf attWeight = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(attWeight);
		assertEquals(int.class, attWeight.getType());
		assertEquals(ret, attWeight.getParent());

		XMLTreeNode children = (XMLTreeNode)ret.getChild("children");
		assertNotNull(children);
		assertEquals(ListUnionClassElement.class, children.getType());
		assertEquals(ret, children.getParent());

		assertNotNull(children.getChild("basicattr"));
		assertNotNull(children.getChild("basicplain"));
		assertNotNull(children.getChild("simple"));

		XMLTreeNode bplain = (XMLTreeNode)children.getChild("basicplain");
		assertNotNull(bplain);
		assertEquals(BasicPlainElement.class, bplain.getType());
		assertEquals(children, bplain.getParent());

		XMLTreeNode battr = (XMLTreeNode)children.getChild("basicattr");
		assertNotNull(battr);
		assertEquals(BasicAttrElement.class, battr.getType());
		assertEquals(children, battr.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testConverter() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ValueConverterElement.class);

//		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("convert",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("zahl"));
		assertEquals(1, ret.getChildren().size());

		XMLTreeAttributeLeaf attWeight = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(attWeight);
		assertEquals(int.class, attWeight.getType());
		assertEquals(ret, attWeight.getParent());
		assertNotNull(attWeight.getValueConverter());
		assertEquals(MultiplyConverter.class, attWeight.getValueConverter().getClass());
	}

	//-------------------------------------------------------------------
	@Test
	public void testBinary() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(BinaryElement.class);
		assertNotNull(ret);
		assertEquals("binary",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("data"));
		assertEquals(1, ret.getChildren().size());

		Field fieldData = BinaryElement.class.getDeclaredField("data");

		XMLBinaryLeaf elemData = (XMLBinaryLeaf)ret.getChild("data");
		assertNotNull(elemData);
		assertEquals(byte[].class, elemData.getType());
		assertEquals(fieldData, elemData.getField());
		assertEquals(ret, elemData.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testSimpleEnum() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(SimpleEnumElement.class);
		assertNotNull(ret);
		assertEquals("simpleenum",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("text"));
		assertNotNull(ret.getChild("zahl"));
		assertEquals(2, ret.getChildren().size());

		Field fieldText = SimpleEnumElement.class.getDeclaredField("text");
		Field fieldZahl = SimpleEnumElement.class.getDeclaredField("zahl");

		XMLTextLeaf elemText = (XMLTextLeaf)ret.getChild("text");
		XMLTreeAttributeLeaf attZahl = (XMLTreeAttributeLeaf)ret.getChild("zahl");
		assertNotNull(elemText);
		assertNotNull(attZahl);
		assertEquals(MyEnumElement.class, elemText.getType());
		assertEquals(MyEnumAttribute.class, attZahl.getType());
		assertEquals(fieldText, elemText.getField());
		assertEquals(fieldZahl, attZahl.getField());
		assertEquals(ret, elemText.getParent());
		assertEquals(ret, attZahl.getParent());
	}

	//-------------------------------------------------------------------
	@Test
	public void testListWithConverter() throws Exception {
		XMLTreeNode ret = TreeBuilder.convertToTree(ListWithConverterElement.class);

		System.out.println(ret.dump());

		assertNotNull(ret);
		assertEquals("convlist",ret.getName());
		assertNull(ret.getParent());
		assertNotNull(ret.getChild("children"));
		assertEquals(1, ret.getChildren().size());

		Field fieldChild = ListWithConverterElement.class.getDeclaredField("children");

		XMLTreeNode children = (XMLTreeNode)ret.getChild("children");
		assertEquals(ArrayList.class, children.getType());
		assertEquals(ret, children.getParent());
		assertEquals(fieldChild, children.getField());
		assertEquals(1, children.getChildren().size());

		XMLTextLeaf twoparam = (XMLTextLeaf)children.getChild("twoparam");
		assertEquals(String.class, twoparam.getType());
		assertEquals(children, twoparam.getParent());
//		assertNotNull(twoparam.getElementConverter());


	}

}
