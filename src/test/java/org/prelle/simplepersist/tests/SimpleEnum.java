/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.SimpleEnumElement;
import org.prelle.simplepersist.example.SimpleEnumElement.MyEnumAttribute;
import org.prelle.simplepersist.example.SimpleEnumElement.MyEnumElement;

/**
 * @author prelle
 *
 */
public class SimpleEnum {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<simpleenum zahl=\"1V\">"
			+SEP+"   <text>World</text>"
			+SEP+"</simpleenum>"+SEP;
	final static SimpleEnumElement DATA = new SimpleEnumElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setText(MyEnumElement.WELT);
		DATA.setZahl(MyEnumAttribute.V1);
	}

	//-------------------------------------------------------------------
	public SimpleEnum() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			SimpleEnumElement read = serializer.read(SimpleEnumElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
