/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.MappedWithConvertElement;

/**
 * @author prelle
 *
 */
public class MappedWithConvert {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<mapped>"
			+SEP+"   <children>"
			+SEP+"      <element>"
			+SEP+"         <key>4</key>"
			+SEP+"         <value fact1=\"Hallo\" fact2=\"Welt\" type=\"java.lang.String\"/>"
			+SEP+"      </element>"
			+SEP+"      <element>"
			+SEP+"         <key>6</key>"
			+SEP+"         <value fact1=\"Wie\" fact2=\"gehts\" type=\"java.lang.String\"/>"
			+SEP+"      </element>"
			+SEP+"   </children>"
			+SEP+"</mapped>"+SEP;
	final static MappedWithConvertElement DATA = new MappedWithConvertElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChildren().put(2, "Hallo Welt");
		DATA.getChildren().put(3, "Wie gehts");
	}

	//-------------------------------------------------------------------
	public MappedWithConvert() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
//	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			MappedWithConvertElement read = serializer.read(MappedWithConvertElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
