/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicAttrElement;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.ItemElement;
import org.prelle.simplepersist.example.ListUnionClassElement;
import org.prelle.simplepersist.example.SimpleElement;

/**
 * @author prelle
 *
 */
public class DeepRecursionTest {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD+  SEP
			+"<item>\n" + 
			"   <text>1</text>\n" + 
			"   <accessories>\n" + 
			"      <item>\n" + 
			"         <text>2</text>\n" + 
			"         <accessories>\n" + 
			"            <item>\n" + 
			"               <text>3</text>\n" + 
			"               <accessories>\n" + 
			"                  <item>\n" + 
			"                     <text>4</text>\n" + 
			"                     <accessories>\n" + 
			"                        <item>\n" + 
			"                           <text>5</text>\n" + 
			"                           <accessories>\n" + 
			"                              <item>\n" + 
			"                                 <text>6</text>\n" + 
			"                              </item>\n" + 
			"                           </accessories>\n" + 
			"                        </item>\n" + 
			"                     </accessories>\n" + 
			"                  </item>\n" + 
			"               </accessories>\n" + 
			"            </item>\n" + 
			"         </accessories>\n" + 
			"      </item>\n" + 
			"   </accessories>\n" + 
			"</item>\n";
	final static ItemElement DATA = new ItemElement("1");
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.addAcccessory(new ItemElement("2")).addAcccessory(new ItemElement("3")).addAcccessory(new ItemElement("4")).addAcccessory(new ItemElement("5")).addAcccessory(new ItemElement("6"));
	}

	//-------------------------------------------------------------------
	public DeepRecursionTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ItemElement read = serializer.read(ItemElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
