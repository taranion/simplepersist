/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.ListPlainElement;

/**
 * @author prelle
 *
 */
public class ListPlain {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<listplain>"
			+SEP+"   <before>Yummy</before>"
			+SEP+"   <children>"
			+SEP+"      <basicplain/>"
			+SEP+"      <basicplain/>"
			+SEP+"   </children>"
			+SEP+"   <text>Dummy</text>"
			+SEP+"</listplain>"+SEP;
	final static ListPlainElement DATA = new ListPlainElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChildren().add(new BasicPlainElement());
		DATA.getChildren().add(new BasicPlainElement());
		DATA.setText("Dummy");
		DATA.setBefore("Yummy");
	}

	//-------------------------------------------------------------------
	public ListPlain() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ListPlainElement read = serializer.read(ListPlainElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
