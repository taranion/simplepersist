/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.ItemElement;
import org.prelle.simplepersist.example.ShieldElement;
import org.prelle.simplepersist.example.WeaponElement;

/**
 * @author prelle
 *
 */
public class WrappedInlineList {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<item weight=\"2\">"
			+SEP+"   <text>Dummy</text>"
			+SEP+"   <weapon damage=\"2\" type=\"1\"/>"
			+SEP+"   <shield handicap=\"1\" type=\"2\"/>"
			+SEP+"</item>"+SEP;
	final static ItemElement DATA = new ItemElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setWeight(2);
		DATA.add(new WeaponElement(2));
		DATA.add(new ShieldElement(1));
		DATA.setText("Dummy");
	}

	//-------------------------------------------------------------------
	public WrappedInlineList() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() throws SerializationException, IOException {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ItemElement read = serializer.read(ItemElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
