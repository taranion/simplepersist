/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicAttrElement;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.ListUnionClassFieldElement;
import org.prelle.simplepersist.example.SimpleElement;

/**
 * @author prelle
 *
 */
public class ListUnionClassField {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<listunion zahl=\"2\">"
			+SEP+"   <children>"
			+SEP+"      <basicplain/>"
			+SEP+"      <basicattr/>"
			+SEP+"      <basicplain/>"
			+SEP+"      <simple zahl=\"3\"/>"
			+SEP+"   </children>"
			+SEP+"</listunion>"+SEP;
	final static ListUnionClassFieldElement DATA = new ListUnionClassFieldElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		SimpleElement simple = new SimpleElement();
		simple.setZahl(3);
		DATA.getChildren().add(new BasicPlainElement());
		DATA.getChildren().add(new BasicAttrElement());
		DATA.getChildren().add(new BasicPlainElement());
		DATA.getChildren().add(simple);
		DATA.setZahl(2);
	}

	//-------------------------------------------------------------------
	public ListUnionClassField() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ListUnionClassFieldElement read = serializer.read(ListUnionClassFieldElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
