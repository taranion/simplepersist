/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.example.ListPlainElement;
import org.prelle.simplepersist.example.ListUnionElement;
import org.prelle.simplepersist.unmarshal2.ByNameInfo;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class ClassHelperTest {
	
	class ClassA {
		@Element(required=true)
		private String foo;
		@Element(name = "hallo")
		private String bar;
		@Attribute
		private String blubb;
	}

	@Root(name = "BB")
	class ClassB {
		@Element
		private int foo;
	}

	class ClassC {
		@Element
		private ClassA wrapped;
	}

	class ListClassA {
		@ElementList(inline = true, entry="item",type = ClassA.class)
		private List<ClassA> inline;
	}

	class ListClassB {
		@ElementList(inline = false, entry="item",type = ClassA.class)
		private List<ClassA> children;
	}

	//-------------------------------------------------------------------
	public static void dump(Map<String,ByNameInfo>  map) {
		List<String> sorted = new ArrayList<String>(map.keySet());
		Collections.sort(sorted);
		for (String key: sorted) {
			ByNameInfo info = map.get(key);
			assertNotNull("No ByNameInfo '"+key+"'",info);
			assertNotNull("No field in info for '"+key+"'",info.field);
			String msg = String.format("  %12s  type %20s into field %s", info.elementName,info.valueType.getSimpleName(), info.field.getName());
			System.out.println(msg);
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void test1() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ClassA.class);
		assertNotNull(map);
		assertEquals(2, map.size());
		assertTrue(map.keySet().contains("foo"));
		assertTrue(map.keySet().contains("hallo"));
		assertFalse(map.keySet().contains("bar"));
		
		dump(map);
	}

	//-------------------------------------------------------------------
	@Test
	public void test2() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ClassC.class);
		assertNotNull(map);
		assertEquals(1, map.size());
		assertTrue(map.keySet().contains("wrapped"));
		
		dump(map);
	}

	//-------------------------------------------------------------------
	@Test
	public void testList1() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ListClassA.class);
		assertNotNull(map);
		assertEquals(1, map.size());
		assertTrue(map.keySet().contains("item"));
		ByNameInfo info = map.get("item");
		assertEquals(false, info.required);
		assertEquals(ClassA.class, info.valueType);
		
		dump(map);
	}

	//-------------------------------------------------------------------
	@Test
	public void testList2() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ListClassB.class);
		assertNotNull(map);
		assertEquals(1, map.size());
		assertTrue(map.keySet().contains("children"));
		ByNameInfo info = map.get("children");
//		assertEquals(MethodType.SET, info.type);
		assertEquals(false, info.required);
		assertEquals(List.class, info.valueType);
		
		dump(map);
	}
	
	//-------------------------------------------------------------------
	@Test
	public void testList3() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ListPlainElement.class);
		assertNotNull(map);
		
		dump(map);
	}
	
	//-------------------------------------------------------------------
	@Test
	public void testListUnion1() {
		Map<String,ByNameInfo>  map = ClassHelperTools.getElementMap(ListUnionElement.class);
		assertNotNull(map);
		assertEquals(3, map.size());
		assertTrue(map.keySet().contains("basicattr"));
		assertTrue(map.keySet().contains("basicplain"));
		assertTrue(map.keySet().contains("simple"));
		
		dump(map);
	}


}
