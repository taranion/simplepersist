/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.FoldedPlainElement;

/**
 * @author prelle
 *
 */
public class FoldedPlain {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<foldedplain>"
			+SEP+"   <child/>"
			+SEP+"</foldedplain>"+SEP;
	final static FoldedPlainElement DATA = new FoldedPlainElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setChild(new BasicPlainElement());
	}

	//-------------------------------------------------------------------
	public FoldedPlain() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			FoldedPlainElement read = serializer.read(FoldedPlainElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
