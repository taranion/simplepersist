package org.prelle.simplepersist.tests;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	UnmarshallerBasics.class,
	BasicPlain.class, 
	BasicAttr.class, 
	Simple.class, 
	Derived.class, 
	FoldedPlain.class, 
	ListPlain.class, 
	ListInline.class, 
	FoldedList.class, 
	Mapped.class, 
	MappedWithConvert.class, 
	WrappedInlineList.class,
	ListUnion.class,
	ListUnionClass.class,
	ListUnionClassField.class,
	ValueConverterTest.class,
	SimpleEnum.class,
	ListWithConverter.class,
//	DeepRecursionTest.class
	})
public class TestSuite {
  //nothing
}

