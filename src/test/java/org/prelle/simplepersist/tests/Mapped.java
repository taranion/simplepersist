/**
 *
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.MappedElement;

/**
 * @author prelle
 *
 */
public class Mapped {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<mapped>"
			+SEP+"   <children>"
			+SEP+"      <element>"
			+SEP+"         <key>key1</key>"
			+SEP+"         <value type=\"org.prelle.simplepersist.example.BasicPlainElement\">"
			+SEP+"            <basicplain/>"
			+SEP+"         </value>"
			+SEP+"      </element>"
			+SEP+"      <element>"
			+SEP+"         <key>key2</key>"
			+SEP+"         <value type=\"org.prelle.simplepersist.example.BasicPlainElement\">"
			+SEP+"            <basicplain/>"
			+SEP+"         </value>"
			+SEP+"      </element>"
			+SEP+"   </children>"
			+SEP+"</mapped>"+SEP;
	final static MappedElement DATA = new MappedElement();

	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.getChild().put("key1", new BasicPlainElement());
		DATA.getChild().put("key2", new BasicPlainElement());
	}

	//-------------------------------------------------------------------
	public Mapped() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());

			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			MappedElement read = serializer.read(MappedElement.class, XML);
			System.out.println(read);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
