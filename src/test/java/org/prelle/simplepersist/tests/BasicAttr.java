/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicAttrElement;

/**
 * @author prelle
 *
 */
public class BasicAttr {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<basicattr text=\"Foo\" zahl=\"5\"/>"+SEP;
	final static BasicAttrElement DATA = new BasicAttrElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setText("Foo");
		DATA.setZahl(5);
	}

	//-------------------------------------------------------------------
	public BasicAttr() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			BasicAttrElement read = serializer.read(BasicAttrElement.class, XML);
			System.out.println(read);
			
			assertEquals(5, read.getZahl());
			assertEquals("Foo", read.getText());
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
