/**
 *
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.DerivedElement;
import org.prelle.simplepersist.example.ElementWithCData;

/**
 * @author prelle
 *
 */
public class ElementWithCDataTest {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<derived derzahl=\"6\">\nHello World</derived>"+SEP;
	final static ElementWithCData DATA = new ElementWithCData();

	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setCData("Hello World");
		DATA.setDerZahl(6);
	}

	//-------------------------------------------------------------------
	public ElementWithCDataTest() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());

			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			ElementWithCData read = serializer.read(ElementWithCData.class, XML);
			System.out.println(read);

			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
