/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.DerivedElement;

/**
 * @author prelle
 *
 */
public class Derived {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<derived derzahl=\"6\" zahl=\"5\">"
			+SEP+"   <dertxt>Blah</dertxt>"
			+SEP+"   <text>Foo</text>"
			+SEP+"</derived>"+SEP;
	final static DerivedElement DATA = new DerivedElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.setText("Foo");
		DATA.setZahl(5);
		DATA.setDerText("Blah");
		DATA.setDerZahl(6);
	}

	//-------------------------------------------------------------------
	public Derived() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() {
		try {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			DerivedElement read = serializer.read(DerivedElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
