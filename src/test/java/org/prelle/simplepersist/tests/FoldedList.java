/**
 * 
 */
package org.prelle.simplepersist.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.io.StringWriter;

import org.junit.Test;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Serializer;
import org.prelle.simplepersist.example.BasicPlainElement;
import org.prelle.simplepersist.example.FoldedListElement;

/**
 * @author prelle
 *
 */
public class FoldedList {

	final static String SEP = System.getProperty("line.separator");
	final static String HEAD = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>";
	final static String XML = HEAD
			+SEP+"<foldedlist>"
			+SEP+"   <text>Dummy</text>"
			+SEP+"   <basicplain/>"
			+SEP+"   <basicplain/>"
			+SEP+"</foldedlist>"+SEP;
	final static FoldedListElement DATA = new FoldedListElement();
	
	private Serializer serializer;

	//-------------------------------------------------------------------
	static {
		DATA.add(new BasicPlainElement());
		DATA.add(new BasicPlainElement());
		DATA.setText("Dummy");
	}

	//-------------------------------------------------------------------
	public FoldedList() throws Exception {
		serializer = new Persister();
	}

	//-------------------------------------------------------------------
	@Test
	public void serialize() throws SerializationException, IOException {
			StringWriter out = new StringWriter();
			serializer.write(DATA, out);
			System.out.println(out.toString());
			
			assertEquals(XML, out.toString());
	}

	//-------------------------------------------------------------------
	@Test
	public void deserialize() {
		try {
			FoldedListElement read = serializer.read(FoldedListElement.class, XML);
			System.out.println(read);
			
			assertEquals(DATA, read);
		} catch (Exception e) {
			e.printStackTrace();
			fail(e.toString());
		}
	}

}
