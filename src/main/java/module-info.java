/**
 * @author Stefan Prelle
 *
 */
module simple.persist {
	exports org.prelle.simplepersist.unmarshal;
	exports org.prelle.simplepersist.marshaller;
	exports org.prelle.simplepersist;

	requires java.xml;
}