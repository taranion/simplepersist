/**
 * 
 */
package org.prelle.simplepersist;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used by classes overriding a class where the parent declares an attribute 
 * as required, but the child class does not require it
 * @author prelle
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value=ElementType.TYPE)
@Inherited
@Documented
public @interface IgnoreMissingAttributes {

	/** Comma-separated list of attribute names */
	String value();
	
}
