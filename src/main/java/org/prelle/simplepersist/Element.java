/**
 * 
 */
package org.prelle.simplepersist;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author prelle
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value=ElementType.FIELD)
@Inherited
@Documented
public @interface Element {

	String name() default "";
	boolean required() default false;
	
}
