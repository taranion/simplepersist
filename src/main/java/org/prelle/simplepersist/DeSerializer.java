/**
 * 
 */
package org.prelle.simplepersist;

import java.io.IOException;

import javax.xml.stream.XMLEventReader;

/**
 * @author prelle
 *
 */
public interface DeSerializer {

	@SuppressWarnings("exports")
	public <T> T read(Class<T> cls, XMLEventReader evRd) throws IOException, SerializationException;
	
}
