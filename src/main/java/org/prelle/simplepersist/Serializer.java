/**
 * 
 */
package org.prelle.simplepersist;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Reader;
import java.io.Writer;

import javax.xml.transform.Source;

/**
 * @author prelle
 *
 */
public interface Serializer {

	//-------------------------------------------------------------------
	public void write(Object data, Writer out) throws SerializationException, IOException;

	//-------------------------------------------------------------------
	public void write(Object data, OutputStream out) throws SerializationException, IOException;

	//-------------------------------------------------------------------
	public void write(Object group, File file) throws SerializationException, IOException;;

	//-------------------------------------------------------------------
	public <T> T read(Class<T> cls, String xml) throws SerializationException, IOException;

	//-------------------------------------------------------------------
	public <T> T read(Class<T> cls, Reader xml) throws SerializationException, IOException;

	//-------------------------------------------------------------------
	public <T> T read(Class<T> cls, InputStream xml) throws SerializationException, IOException;

	//-------------------------------------------------------------------
	public <T> T read(Class<T> cls, Source xml) throws SerializationException, IOException;


}
