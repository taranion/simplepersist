/**
 * 
 */
package org.prelle.simplepersist;

import java.io.IOException;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
public class SerializationException extends IOException {
	
	private int line;
	private int column;

	//-------------------------------------------------------------------
	/**
	 */
	public SerializationException() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param message
	 */
	public SerializationException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param cause
	 */
	public SerializationException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	/**
	 * @param message
	 * @param cause
	 */
	public SerializationException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public int getLine() {
		return line;
	}

	public void setLine(int line) {
		this.line = line;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

}
