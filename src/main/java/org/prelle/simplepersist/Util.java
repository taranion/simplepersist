package org.prelle.simplepersist;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.List;

/**
 * @author prelle
 *
 */
public class Util {

	private final static Logger logger = System.getLogger("xml");

	//--------------------------------------------------------------------
	/**
	 * Create an instance of the converter class. If the class has
	 * an ConstructorParams annotation on type level, this is use to
	 * determine the parameters of the constructor. In this case
	 * each string in the constructor params is a key for the context
	 * keys within this Persister.
	 */
	public static StringValueConverter<?> createAttribConverter(Class<? extends StringValueConverter<?>> cls) throws SerializationException {
		// If annotation is missing, use empty constructor
		if (!cls.isAnnotationPresent(ConstructorParams.class)) {
			if (logger.isLoggable(Level.DEBUG))
				logger.log(Level.TRACE, "No @ConstructorParams in converter class. Use empty constructor.");
			try {
				return cls.getDeclaredConstructor().newInstance();
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error calling empty constructor of "+cls,e);
				throw new SerializationException("Error calling empty constructor of "+cls,e);
			}
		}

		ConstructorParams cParams = cls.getAnnotation(ConstructorParams.class);
		Class<?>[] classes = new Class[cParams.value().length];
		Object[] params = new Object[cParams.value().length];
		int i=0;
		for (String contextKey : cParams.value()) {
			Object obj = Persister.getKey(contextKey);
			logger.log(Level.DEBUG, "  Constructor parameter '"+contextKey+"' is "+obj);
			params[i] = obj;
			classes[i] = (obj!=null)?obj.getClass():Object.class;
			i++;
		}
		try {
			Constructor<? extends StringValueConverter<?>> cons = cls.getConstructor(classes);
			logger.log(Level.DEBUG, "  Call "+cons+" with "+Arrays.toString(params));
			return cons.newInstance(params);
		} catch (NoSuchMethodException e) {
			logger.log(Level.ERROR, "Missing constructor "+cls.getSimpleName()+"<init>("+Arrays.toString(params)+")");
			throw new SerializationException(e);
		} catch (Exception e) {
			throw new SerializationException(e);
		}
	}

	//--------------------------------------------------------------------
	/**
	 * Create an instance of the converter class. If the class has
	 * an ConstructorParams annotation on type level, this is use to
	 * determine the parameters of the constructor. In this case
	 * each string in the constructor params is a key for the context
	 * keys within this Persister.
	 */
	public static XMLElementConverter<?> createElementConverter(Class<? extends XMLElementConverter<?>> cls) throws SerializationException {
		// If annotation is missing, use empty constructor
		if (!cls.isAnnotationPresent(ConstructorParams.class)) {
			if (logger.isLoggable(Level.DEBUG))
				logger.log(Level.DEBUG, "No @ConstructorParams in converter class. Use empty constructor.");
			try {
				return cls.getDeclaredConstructor().newInstance();
			} catch (Exception e) {
				logger.log(Level.ERROR, "Error calling empty constructor of "+cls,e);
				throw new SerializationException("Error calling empty constructor of "+cls,e);
			}
		}

		ConstructorParams cParams = cls.getAnnotation(ConstructorParams.class);
		Class<?>[] classes = new Class[cParams.value().length];
		Object[] params = new Object[cParams.value().length];
		int i=0;
		for (String contextKey : cParams.value()) {
			Object obj = Persister.getKey(contextKey);
			logger.log(Level.DEBUG, "  Constructor parameter "+contextKey+" is "+obj);
			params[i] = obj;
			classes[i] = (obj!=null)?obj.getClass():Object.class;
			i++;
		}
		try {
			Constructor<? extends XMLElementConverter<?>> cons = cls.getConstructor(classes);
			logger.log(Level.DEBUG, "  Call "+cons+" with "+Arrays.toString(params));
			return cons.newInstance(params);
		} catch (NoSuchMethodException e) {
			logger.log(Level.ERROR, "Missing constructor cls<init>("+Arrays.toString(params)+")");
			throw new SerializationException(e);
		} catch (Exception e) {
			throw new SerializationException(e);
		}
	}

//	//-------------------------------------------------------------------
//	private static Replacer<Object,Object> createReplacer(Class<? extends Replacer<Object,Object>> cls) throws SerializationException {
//		// If annotation is missing, use empty constructor
//		if (!cls.isAnnotationPresent(ConstructorParams.class)) {
//			if (logger.isDebugEnabled())
//				logger.log(Level.DEBUG, "  No @ConstructorParams in converter class. Use empty constructor.");
//			try {
//				Constructor<? extends Replacer<Object,Object>> cons = cls.getConstructor();
//				cons.setAccessible(true);
//				return cons.newInstance();
//			} catch (Exception e) {
//				logger.log(Level.ERROR, "Error calling empty constructor of "+cls,e);
//				throw new SerializationException("Error calling empty constructor of "+cls,e);
//			}
//		}
//
//		ConstructorParams cParams = cls.getAnnotation(ConstructorParams.class);
//		Class<?>[] classes = new Class[cParams.value().length];
//		Object[] params = new Object[cParams.value().length];
//		int i=0;
//		for (String contextKey : cParams.value()) {
//			Object obj = Persister.getKey(contextKey);
//			logger.log(Level.DEBUG, "  Constructor parameter "+contextKey+" is "+obj);
//			params[i] = obj;
//			classes[i] = (obj!=null)?obj.getClass():Object.class;
//			i++;
//		}
//		try {
//			Constructor<? extends Replacer<Object,Object>> cons = cls.getConstructor(classes);
//			cons.setAccessible(true);
//			logger.log(Level.DEBUG, "  Call "+cons+" with "+Arrays.toString(params));
//			return cons.newInstance(params);
//		} catch (NoSuchMethodException e) {
//			logger.log(Level.ERROR, "Missing constructor cls<init>("+Arrays.toString(params)+")");
//			throw new SerializationException(e);
//		} catch (Exception e) {
//			throw new SerializationException(e);
//		}
//	}

	//-------------------------------------------------------------------
	public static Iterable<Field> getAttributeFields(Class<?> cls) {

		List<Field> fields = new ArrayList<Field>();
		for (Field field : Util.getAllFields(cls)) {
			if (field.isAnnotationPresent(Attribute.class)) {
				fields.add(field);
				continue;
			}
		}

		return fields;
	}

	//-------------------------------------------------------------------
	public static Iterable<Field> getElementFields(Class<?> cls) {

		List<Field> fields = new ArrayList<Field>();
		for (Field field : Util.getAllFields(cls)) {
			if (Modifier.isTransient(field.getModifiers()))
				continue;
			
			if (field.isAnnotationPresent(Element.class)) {
				fields.add(field);
				continue;
			}
			if (field.isAnnotationPresent(ElementList.class)) {
				fields.add(field);
				continue;
			}
			if (field.isAnnotationPresent(ElementListUnion.class)) {
				fields.add(field);
				continue;
			}
		}

		return fields;
	}

	//-------------------------------------------------------------------
	public static Iterable<Field> getAllFields(Class<?> startClass) {

		List<Field> currentClassFields = new ArrayList<Field>(Arrays.asList(startClass.getDeclaredFields()));
		Class<?> parentClass = startClass.getSuperclass();

		if (parentClass != Object.class && parentClass!=null) {
			List<Field> parentClassFields = (List<Field>) getAllFields(parentClass);
			currentClassFields.addAll(parentClassFields);
		}

		return currentClassFields;
	}

	//-------------------------------------------------------------------
	public static String convertToString(Object val) {
		if (val instanceof byte[]) {
			return Base64.getEncoder().encodeToString((byte[])val);
		}
		return String.valueOf(true);
	}

}
