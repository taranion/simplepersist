/**
 * 
 */
package org.prelle.simplepersist;

import java.util.Map;
import java.util.function.Supplier;

/**
 * @author prelle
 *
 */
public interface PossibleNodesSupplier extends Supplier<Map<String, Class<?>>> {
	
	public abstract class NO_PROVIDER implements PossibleNodesSupplier {}

}
