/**
 * 
 */
package org.prelle.simplepersist;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;

/**
 * @author prelle
 *
 */
public interface XMLElementConverter<T> {

	//-------------------------------------------------------------------
	public void write(XmlNode node, T value) throws Exception;

	//-------------------------------------------------------------------
	public T read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception;

}
