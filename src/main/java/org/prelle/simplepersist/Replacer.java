/**
 * 
 */
package org.prelle.simplepersist;

/**
 * @author prelle
 *
 */
public interface Replacer<T,U> {

	//-------------------------------------------------------------------
	public U write(T value) throws Exception;

	//-------------------------------------------------------------------
	public T read(U from) throws Exception;

}
