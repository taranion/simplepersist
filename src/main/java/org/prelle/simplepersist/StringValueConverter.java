/**
 * 
 */
package org.prelle.simplepersist;

/**
 * @author prelle
 *
 */
public interface StringValueConverter<T> {
	
	public static StringValueConverter<String> NONE = new StringValueConverter<String>() {

		@Override
		public String write(String value) throws Exception {
			return value;
		}

		@Override
		public String read(String v) throws Exception {
			return v;
		}
	};

	//-------------------------------------------------------------------
	public String write(T value) throws Exception;

	//-------------------------------------------------------------------
	public T read(String v) throws Exception;

//	//-------------------------------------------------------------------
//	public void write(XmlNode node, T value) throws Exception;
//
//	//-------------------------------------------------------------------
//	public T read(Persister.ParseNode node, StartElement ev) throws Exception;

}
