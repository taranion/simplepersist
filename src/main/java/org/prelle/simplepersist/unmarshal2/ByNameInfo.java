package org.prelle.simplepersist.unmarshal2;

import java.lang.reflect.Field;
import java.util.function.BiFunction;

import org.prelle.simplepersist.XMLElementConverter;

public class ByNameInfo {
	
	public enum AttrOrElem {
		ATTRIBUTE,
		ELEMENT
	}
	public enum Operation {
		SET,
		ADD,
		PUT,
		KEY,
		VAL,
		KEEP,
		NONE
	}

	public AttrOrElem attributeOrElement;
	public String elementName;
//	public Class<?> cls;
	public boolean required;
	/** Where to set/add value to */
	public Field field;
	/** What to do */
	public Operation oper = Operation.NONE;
	/** What type of value */
	public Class<?> valueType;
	public XMLElementConverter<?> elementConv;
    public BiFunction<Class<?>, String, ?> resolver;
	
	//-------------------------------------------------------------------
	public ByNameInfo() {
		
	}
	
	//-------------------------------------------------------------------
	public ByNameInfo(AttrOrElem type, String name, Class<?> cls) {
		this.attributeOrElement = type;
		this.elementName = name;
		this.valueType   = cls;
	}
	
	//-------------------------------------------------------------------
	public String toString() {
		String msg = String.format("  %12s  %3s %4s type '%20s' into field %s", elementName, oper, (attributeOrElement==AttrOrElem.ATTRIBUTE)?"ATTR":"ELEM", 
				(valueType!=null)?valueType.getSimpleName():"null", 
				(field!=null)?field.getName():"null");
		return "ByNameInfo("+msg+")";
	}
	
	//-------------------------------------------------------------------
	public String toOperationString() {
		return oper+" value into field "+field+" of parent";
	}
	
}