package org.prelle.simplepersist.unmarshal2;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.EntityReference;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.simplepersist.AfterLoadHook;
import org.prelle.simplepersist.CData;
import org.prelle.simplepersist.DeSerializer;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Resolver;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.Util;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.unmarshal2.ByNameInfo.AttrOrElem;
import org.prelle.simplepersist.unmarshal2.ByNameInfo.Operation;

/**
 * - on START_ELEMENT create an instance of the class and a new current element
 * - on END_ELEMENT either add instance to list or call setter on parent
 * @author prelle
 *
 */
public class Unmarshaller3 implements DeSerializer {

	private final static Logger logger = System.getLogger("xml");

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.DeSerializer#read(java.lang.Class, javax.xml.stream.XMLEventReader)
	 */
	@Override
	public <T> T read(Class<T> cls, XMLEventReader evRd) throws IOException, SerializationException {
		// Get root annotation from class to obtain expected tag name
		Root rootAnno = cls.getAnnotation(Root.class);
		if (rootAnno==null)
			throw new SerializationException("Class "+cls+" misses @Root");

		Context context = new Context(cls, rootAnno.name());
		context.eventReader = evRd;

		CurrentElement current = null;

		XMLEvent ev = null;
		try {
			while( evRd.hasNext() ) {
				ev = evRd.nextEvent();
				logger.log(Level.DEBUG, "------------------------------------------------------------------");
				switch (ev.getEventType()) {
				case XMLEvent.START_DOCUMENT:
					current = new CurrentElement(Type.ROOT, null);
					//					Map<String,ByNameInfo> rootMap = ClassHelperTools.getElementMap(cls);
					Map<String,ByNameInfo> rootMap = new HashMap<String, ByNameInfo>();
					rootMap.put(rootAnno.name(), new ByNameInfo(AttrOrElem.ELEMENT, rootAnno.name(), cls));
					current.setExpect(rootMap);
					context.push(current);
					logger.log(Level.DEBUG, "START_DOC "+context);
					continue;
				case XMLEvent.START_ELEMENT:
					String uri = ev.asStartElement().getName().getNamespaceURI();
					if (current.getType()==Type.ROOT) {
						if (uri!=null && !uri.isBlank()) {
							logger.log(Level.INFO, "Main namespace = {0}",  ev.asStartElement().getName().getNamespaceURI());
							context.setMainNamespace(ev.asStartElement().getName().getNamespaceURI());
						}
					} 
					if (uri!=null && !uri.isBlank() && !uri.equals(context.getMainNamespace())) {
						String tag = "<"+ev.asStartElement().getName().getLocalPart()+">";
						if (current.getInstance()==null || current.getInstance().getClass()!=String.class)
							current.setInstance(tag);
						else {
							current.setInstance( ((String)current.getInstance())+tag);
						}
					} else {
						if (ev.asStartElement().toString().contains("talker")) {
							logger.log(Level.WARNING, "DEBUG here");
						}
						current = processStartElement(context, ev.asStartElement(), evRd);
					}
					continue;
				case XMLEvent.END_ELEMENT:
					uri = ev.asEndElement().getName().getNamespaceURI();
					if (uri!=null && !uri.isBlank() && !uri.equals(context.getMainNamespace())) {
						String tag = "</"+ev.asEndElement().getName().getLocalPart()+">";
						if (current.getInstance()==null || current.getInstance().getClass()!=String.class)
							current.setInstance(tag);
						else {
							current.setInstance( ((String)current.getInstance())+tag);
						}
					} else {
						processEndElement(context, ev.asEndElement());
					}
					continue;
				case XMLEvent.CHARACTERS:
					current = context.getCurrentElement();
					if (logger.isLoggable(Level.DEBUG)) {
						logger.log(Level.DEBUG, "CHARACTERS "+ev);
						logger.log(Level.DEBUG, "  context: "+context);
					}
					switch (current.getType()) {
					case TEXT:
					case ENUM_TEXT:
					case BINARY:
					case MAP_ITEM_KEY:
						if (current.getInstance()==null || current.getInstance().getClass()!=String.class)
							current.setInstance(ev.asCharacters().getData());
						else {
							current.setInstance( ((String)current.getInstance())+ev.asCharacters().getData());
						}
						break;
					case ELEMENT:
						logger.log(Level.DEBUG, "class is "+current.getInstance());
						if (current.getInstance()==null || current.getInstance().getClass()!=String.class) {
							Field field = getFieldWithCData(current.getInstance().getClass());
							if (field!=null) {
								logger.log(Level.DEBUG, "field is "+field);
								field.setAccessible(true);
								String tmp = (String) field.get(current.getInstance());
								if (tmp==null)
									field.set(current.getInstance(), ev.asCharacters().getData().trim());
								else
									field.set(current.getInstance(), tmp+ev.asCharacters().getData());
							}
						} else {
							current.setInstance( ((String)current.getInstance())+ev.asCharacters().getData());
						}
						break;
					case INLINE_ELEMENT:
						// Search CDATA field annotation in instance
						if (current.getInstance()!=null) {
							Field field = getFieldWithCData(current.getInstance().getClass());
							if (field==null && current.getClazz()!=null) {
								field = getElementListWithString(current.getClazz());
							}
							if (field==null && current.getInstance()!=null) {
								field = getElementListWithString(current.getInstance().getClass());
							}
							String toSet = ev.asCharacters().getData().trim();
							// If field is of type list, use adder instead of setting
							if (List.class.isAssignableFrom( current.getFromField().getType()) && !toSet.isEmpty()) {
								if (current.getFromField().isAnnotationPresent(ElementList.class) &&  current.getFromField().getAnnotation(ElementList.class).inline()) {
									logger.log(Level.INFO, "Do something here "+current.getFromField());
									if (current.getInstance()!=null && String.class==current.getInstance().getClass()) {
										current.setInstance(toSet);
									}
								}
							}
							if (field!=null && !toSet.isEmpty()) {
								field.setAccessible(true);
								String exist = (String) field.get(current.getInstance());
								if (exist!=null)
									toSet = exist+toSet;
								field.set(current.getInstance(),toSet);
							} 
//							else {
//								logger.log(Level.WARNING, "Found no field with CData annotation in "+current.getInstance());
//							}
						} else
							logger.log(Level.WARNING, "Found no current instance to search field in");
						break;
					default:
						logger.log(Level.DEBUG, "Got CDATA while in "+current.getType()+" for "+current.getInstance());
					}
					continue;
				case XMLEvent.END_DOCUMENT:
					logger.log(Level.DEBUG, "END_DOCUMENT");
					current = context.getCurrentElement();
					return (T) current.getInstance();
				case XMLEvent.COMMENT:
					continue;
				case XMLEvent.DTD:
					continue;
				case XMLEvent.ENTITY_REFERENCE:
					EntityReference ref = (EntityReference)ev;
					String tag = "&"+ref.getName()+";";
					if (current.getInstance()==null || current.getInstance().getClass()!=String.class)
						current.setInstance(tag);
					else {
						current.setInstance( ((String)current.getInstance())+tag);
					}
					break;
				default:
					logger.log(Level.ERROR, "? "+ev.getEventType()) ;
					System.exit(0);
				}

			}
			//		} catch (InstantiationException |IllegalAccessException e) {
			//			throw new SerializationException(e);
		} catch (SerializationException e) {
			e.printStackTrace();
			logger.log(Level.ERROR, "Error in line "+ev.getLocation().getLineNumber()+", col "+ev.getLocation().getColumnNumber()+": "+e);
			e.setLine(ev.getLocation().getLineNumber());
			e.setColumn(ev.getLocation().getColumnNumber());
			logger.log(Level.ERROR, "Context: "+context);
			throw e;
		} catch (InstantiationException e) {
			logger.log(Level.ERROR, "Error in line "+ev.getLocation().getLineNumber()+", col "+ev.getLocation().getColumnNumber()+": Failed instantiating "+e, e.getCause());
			e.printStackTrace();
			SerializationException ee =  new SerializationException(e);
			ee.setLine(ev.getLocation().getLineNumber());
			ee.setColumn(ev.getLocation().getColumnNumber());
			throw ee;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Error in line "+ev.getLocation().getLineNumber()+", col "+ev.getLocation().getColumnNumber()+": "+e,e);
			SerializationException ee =  new SerializationException(e);
			ee.setLine(ev.getLocation().getLineNumber());
			ee.setColumn(ev.getLocation().getColumnNumber());
			throw ee;
		}
		return null;
	}

	//-------------------------------------------------------------------
	private static Type getNextNodeType(Class<?> type) {
		if (type==String.class) return Type.TEXT;
		if (type.isEnum()) return Type.ENUM_TEXT;
		if (List.class.isAssignableFrom(type)) {
			// TODO: handle inline lists
		}
		if (Map.class.isAssignableFrom(type)) return Type.MAP;
		return Type.ELEMENT;
	}

	//-------------------------------------------------------------------
	private static Type getNextNodeType(Field field) {
		logger.log(Level.DEBUG, "getNextNodeType() for "+field+" and type "+field.getType());
		Class<?> type = field.getType();
		if (type==String.class) return Type.TEXT;
		if (type.isEnum()) return Type.ENUM_TEXT;
		if (List.class.isAssignableFrom(type)) {
			ElementList listAnno = field.getAnnotation(ElementList.class);
			ElementListUnion listUnionAnno = field.getAnnotation(ElementListUnion.class);
			if (listAnno!=null) {
				if (listAnno.type().isEnum() && listAnno.inline())
					return Type.ENUM_TEXT;
				return listAnno.inline()?Type.INLINE_ELEMENT:Type.ELEMENT_LIST;
			}
			if (listUnionAnno!=null) {
				return listUnionAnno.inline()?Type.INLINE_ELEMENT:Type.ELEMENT_LIST;
			}
			if (field.isAnnotationPresent(Element.class)) {
				// Look into class itself for those annotations
				listAnno = type.getAnnotation(ElementList.class);
				listUnionAnno = type.getAnnotation(ElementListUnion.class);
				if (listAnno!=null) {
//					return listAnno.inline()?Type.INLINE_ELEMENT:Type.ELEMENT_LIST;
					return Type.ELEMENT_LIST;
				}
				if (listUnionAnno!=null) {
//					return listUnionAnno.inline()?Type.INLINE_ELEMENT:Type.ELEMENT_LIST;
					return Type.ELEMENT_LIST;
				}

			}
			throw new IllegalStateException("No @ElementList or @ElementListUnion for "+field);
		}
		if (Map.class.isAssignableFrom(type)) {
			if (field.isAnnotationPresent(ElementList.class)) {
				ElementList eList = field.getAnnotation(ElementList.class);
				if (eList.addMethod()!=null) {
					return Type.ELEMENT_LIST;
				}
			}
			return Type.MAP;
		}
		if (type==Boolean.class || type==boolean.class) {
			return Type.TEXT;
		} else if (type==Integer.class || type==int.class) {
			return Type.TEXT;
		} else if (type==Byte.class || type==byte.class) {
			return Type.TEXT;
		} else if (type==Long.class || type==long.class) {
			return Type.TEXT;
		} else if (type==Float.class || type==float.class) {
			return Type.TEXT;
		} else if (type==Double.class || type==double.class) {
			return Type.TEXT;
		} else if (type==byte[].class) {
			return Type.BINARY;
		} else if (type==UUID.class) {
			return Type.TEXT;
		}
		return Type.ELEMENT;
	}

	//-------------------------------------------------------------------
	private CurrentElement processStartElement(Context context, StartElement ev, XMLEventReader evRd) throws IOException, SerializationException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException, ClassNotFoundException {
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "START  "+ev);
			logger.log(Level.DEBUG, "  context: "+context);
		}
		CurrentElement current = context.getCurrentElement();
		Map<String, ByNameInfo> expected = current.getExpect();
		if (expected==null) {
			expected = ClassHelperTools.getElementMap(current.getClazz());
		}
		CurrentElement listParent = null;
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "  Class  : "+current.getClazz());
			logger.log(Level.DEBUG, "  Instan : "+current.getInstance());
			logger.log(Level.DEBUG, "  expect :\n"+ClassHelperTools.dump(expected));
			logger.log(Level.DEBUG, "  KeyConv: "+current.getKeyConverter());
			logger.log(Level.DEBUG, "  ValConv: "+current.getElemConverter());
		}


		String name = ev.getName().getLocalPart();
//		if (name.contains("talker")) {
//			logger.log(Level.WARNING, "DEBUG here");
//		}
		ByNameInfo info = (expected!=null)?expected.get(name):null;
		if (info==null && current.getClazz()!=null) {
			expected = ClassHelperTools.getElementMap(current.getClazz());
		}
		logger.log(Level.DEBUG, "  Using  : {0}",info);
		if (info==null) {
			logger.log(Level.ERROR, "Unexpected element: "+name+" in context "+context);
			logger.log(Level.WARNING, "Expectations:\n"+ClassHelperTools.dump(expected));
		} else

		switch (current.getType()) {
		case ROOT:
			if (info==null)
				throw new SerializationException("Root element expected '"+context.getRootElement()+"' but found '"+name+"'");
			// Is expected root element, so create an instance
			if (List.class.isAssignableFrom(info.valueType)) {
				List<?> list = (List<?>) info.valueType.getConstructor().newInstance();
				current = new CurrentElement(Type.ELEMENT_LIST, name);
				current.setInstance(list);
			} else {
				current = new CurrentElement(Type.ELEMENT, name);
				current.setInstance(context.getRootClass().getConstructor().newInstance());
			}
			current.setExpect(ClassHelperTools.getElementMap(info.valueType));
			ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
			context.push(current);
			return current;
		case ELEMENT:
			if (info.field==null)
				throw new SerializationException("Missing field for element: "+name+" in context "+context+"\ninfo="+info);
			Type nextType = getNextNodeType(info.field);
			logger.log(Level.DEBUG, "Expect "+nextType);
			CurrentElement parent = current;
			current = new CurrentElement(nextType, name);
			current.setFromField(info.field);
			switch (nextType) {
			case BINARY:
				break;
			case TEXT: case ENUM_TEXT:
				break;
			case ELEMENT_LIST:
				listParent = current;
				if (info.valueType==Map.class) {
					HashMap<?,?> map = new HashMap<>();
					current = new CurrentElement(Type.ELEMENT_LIST, name);
					current.setInstance(map);
					current.setFromField(info.field);
				} else {
					List<?> list = new ArrayList<>();
					if (info.valueType!=List.class && List.class.isAssignableFrom(info.valueType)) {
						list = (List<?>) info.valueType.getConstructor().newInstance();
					}
					current = new CurrentElement(Type.ELEMENT_LIST, name);
					current.setInstance(list);
				}
				Map<String,ByNameInfo> expect = ClassHelperTools.getElementMap(info.field, true);
				Type next2Type = getNextNodeType(info.field);
				for (ByNameInfo tmp : expect.values())
					tmp.oper = Operation.ADD;
				current.setExpect(expect);
				break;
			case MAP:
				Map<?,?> map = new HashMap<Object, Object>();
				if (info.valueType!=Map.class)
					map = (Map<?,?>)info.valueType.getConstructor().newInstance();
				current = new CurrentElement(Type.MAP, name);
				current.setInstance(map);
				if (info.field.isAnnotationPresent(MapConvert.class)) {
					MapConvert annoConv = info.field.getAnnotation(MapConvert.class);
					current.setElemConverter(Util.createElementConverter(annoConv.valConvert()));
					current.setKeyConverter(Util.createAttribConverter(annoConv.keyConvert()));
				}
				expect = new HashMap<String, ByNameInfo>();
				ParameterizedType type = (ParameterizedType) info.field.getGenericType();
				java.lang.reflect.Type valueType = type.getActualTypeArguments()[1];
				ByNameInfo expInfo = new ByNameInfo(AttrOrElem.ELEMENT, "element", (Class<?>) valueType);
				expInfo.field = info.field;
				expInfo.oper = Operation.PUT;
				expect.put("element", expInfo);
				//				expect.put("value", new ByNameInfo(AttrOrElem.ELEMENT, "value", Object.class));
				// Addition: If a "addEntry" method is present, allow attribute directly
//				if (info.field.isAnnotationPresent(ElementList.class)) {
//					ElementList eList = info.field.getAnnotation(ElementList.class);
//					if (eList.addMethod()!=null) {
//						logger.log(Level.WARNING, "ToDo: Add ByName");
//						expInfo = new ByNameInfo(AttrOrElem.ELEMENT, eList.entry(), (Class<?>) valueType);
//						expInfo.field = info.field;
//						expInfo.oper = Operation.ADD;
//						expect.put(eList.entry(), expInfo);
//					}
//				}


				current.setExpect(expect);
				break;
			case INLINE_ELEMENT:
				logger.log(Level.DEBUG, "Parent = "+parent.getInstance());
				logger.log(Level.DEBUG, "info.valueType = "+info.valueType);
				info.field.setAccessible(true);
				/*
				 * Create an instance of the field type.
				 * If the type is abstract, consult config for instance type
				 */
				try {
					if (Modifier.isAbstract(info.valueType.getModifiers())) {
						logger.log(Level.DEBUG, "Abstract "+info.valueType);
						String key = Persister.PREFIX_KEY_ABSTRACT+"."+info.valueType.getName();
                        if (Persister.getKey(key)==null && info.valueType.isInterface()) {
//                            logger.log(Level.WARNING, "ToDo: Check "+info.valueType);
                            try {
                                XMLEvent next = evRd.peek();
                                if (next.getEventType()==XMLEvent.CHARACTERS) {
                                    next = evRd.nextEvent();
                                    Object created = info.resolver.apply(info.valueType, next.asCharacters().getData());
                                    current.setInstance(created);
//                                    info.valueType = created.getClass();
//                                  logger.log(Level.WARNING, "Set value to "+current.getInstance());
                                }
                            } catch (XMLStreamException e) {
                                logger.log(Level.ERROR, "Error processing java.lang.String in an inside List<String> element",e);
                            } catch (IllegalArgumentException e) {
                                logger.log(Level.ERROR, "Error at "+ev.getLocation().getLineNumber()+": "+e.getMessage());
                                System.err.println("Error at "+ev.getLocation().getLineNumber()+": "+e);
                            }
                       } else {
                           if (Persister.getKey(key)==null) {
                               throw new SerializationException("Missing instructions how to create an instance abstract class '"+info.valueType+"'\n\nAdd\nPersister.putContext(Persister.PREFIX_KEY_ABSTRACT+\".\"+"+info.valueType.getSimpleName()+".class.getName(), <your class that extends it>);\nto your code");
                           }
                           Class<?> clazz = (Class<?>) Persister.getKey(key);
                           logger.log(Level.DEBUG, "Instantiate "+clazz+" instead of "+info.valueType);
                           info.valueType = clazz;
                           current.setInstance(clazz.getConstructor().newInstance());
                       }
					} else if (info.valueType.isInterface()) {
					    logger.log(Level.WARNING, "TODO: Interface "+info.valueType);
					    System.exit(1);
					} else {
						if (info.valueType==String.class) {
							try {
								XMLEvent next = evRd.peek();
								if (next.getEventType()==XMLEvent.CHARACTERS) {
									next = evRd.nextEvent();
									current.setInstance(next.asCharacters().getData());
//									logger.log(Level.WARNING, "Set value to "+current.getInstance());
								}
							} catch (XMLStreamException e) {
								logger.log(Level.ERROR, "Error processing java.lang.String in an inside List<String> element",e);
							}
						} else {
							current.setInstance(info.valueType.getConstructor().newInstance());
							current.setClazz(info.valueType);
						}
					}
				} catch (InstantiationException ie) {
					// TODO Auto-generated catch block
					throw new InstantiationException("Error instantiating "+info.valueType);
				}
				logger.log(Level.DEBUG, "  created "+current.getInstance());
//				}
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				current.setExpect(ClassHelperTools.getElementMap(info.valueType));
				break;
			case ELEMENT:
				// Is there a converter
				if (info.field.isAnnotationPresent(ElementConvert.class)) {
					ElementConvert conv = info.field.getAnnotation(ElementConvert.class);
					XMLElementConverter<?> converter = Util.createElementConverter(conv.value());
					try {
						Object converted = converter.read(null, ev, context.eventReader);
						current.setInstance(converted);
						current.setExpect(ClassHelperTools.getElementMap(converted.getClass()));
					} catch (Exception e) {
						throw new SerializationException("Failed calling converter "+conv.value(), e);
					}
				} else {
					current.setInstance(info.valueType.getConstructor().newInstance());
					current.setExpect(ClassHelperTools.getElementMap(info.field,  false));
				}
				logger.log(Level.DEBUG, "  created "+current.getInstance());
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				break;
			default:
				current.setInstance(info.valueType.getConstructor().newInstance());
				logger.log(Level.DEBUG, "  created "+current.getInstance());
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				current.setExpect(ClassHelperTools.getElementMap(info.field, false));
			}
			context.push(current);
			return current;
		case INLINE_ELEMENT:
			logger.log(Level.DEBUG, "Found inline element of "+info.valueType+" for list "+current.getInstance());
			if (info.field==null) {
				nextType = getNextNodeType(info.valueType);
//				throw new SerializationException("No field information for '"+name+"'\n"+ClassHelperTools.dump(expected)+"\nContext: "+context+"\nType  : "+info.valueType);
			} else {
				nextType = getNextNodeType(info.field);
			}
			logger.log(Level.DEBUG, "Expect "+nextType);
			parent = current;
			current = new CurrentElement(nextType, name);
			current.setFromField(info.field);

			switch (nextType) {
			case BINARY:
				break;
			case TEXT: case ENUM_TEXT:
				break;
			case ELEMENT_LIST:
				List<?> list = new ArrayList<>();
				if (info.valueType!=List.class)
					list = (List<?>) info.valueType.getConstructor().newInstance();
				current = new CurrentElement(Type.ELEMENT_LIST, name);
				current.setInstance(list);
				current.setExpect(ClassHelperTools.getElementMap(info.field, true));
				break;
			case MAP:
				Map<?,?> map = new HashMap<Object, Object>();
				if (info.valueType!=Map.class)
					map = (Map<?,?>)info.valueType.getConstructor().newInstance();
				current = new CurrentElement(Type.MAP, name);
				current.setInstance(map);
				if (info.field.isAnnotationPresent(MapConvert.class)) {
					MapConvert annoConv = info.field.getAnnotation(MapConvert.class);
					current.setElemConverter(Util.createElementConverter(annoConv.valConvert()));
					current.setKeyConverter(Util.createAttribConverter(annoConv.keyConvert()));
				}
				Map<String,ByNameInfo> expect = new HashMap<String, ByNameInfo>();
				ParameterizedType type = (ParameterizedType) info.field.getGenericType();
				java.lang.reflect.Type valueType = type.getActualTypeArguments()[1];
				ByNameInfo expInfo = new ByNameInfo(AttrOrElem.ELEMENT, "element", (Class<?>) valueType);
				expInfo.field = info.field;
				expect.put("element", expInfo);
				//				expect.put("value", new ByNameInfo(AttrOrElem.ELEMENT, "value", Object.class));
				current.setExpect(expect);
				break;
			case INLINE_ELEMENT:
				logger.log(Level.DEBUG, "Parent = "+parent.getInstance());
				info.field.setAccessible(true);
//				if (info.field.get(parent.getInstance())!=null) {
//					current.setInstance(info.field.get(parent.getInstance()));
//					logger.log(Level.DEBUG, "  reused "+current.getInstance());
//				} else {
				try {
					if (Modifier.isAbstract( info.valueType.getModifiers())) {
						Class<?> real = Persister.getRealClass(info.valueType);
						current.setInstance(real.getConstructor().newInstance());
					} else {
						current.setInstance(info.valueType.getConstructor().newInstance());
					}
					current.setClazz(info.field.getType()); // SHould be Talker
					logger.log(Level.DEBUG, "  created "+current.getInstance());
				} catch (InstantiationException e1) {
					logger.log(Level.ERROR, "Error instantiating "+info.valueType+" with empty constructor");
					throw e1;
				}
//				}
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				current.setExpect(ClassHelperTools.getElementMap(info.valueType));
				break;
			default:
				current = new CurrentElement(Type.ELEMENT, name);
				current.setInstance(info.valueType.getConstructor().newInstance());
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				current.setExpect(ClassHelperTools.getElementMap(info.valueType));
			}
			context.push(current);
			return current;
		case ELEMENT_LIST:
			if (info.valueType==String.class) {
				logger.log(Level.DEBUG, "  expect CDATA");
				current = new CurrentElement(Type.TEXT,name);
				current.setClazz(context.getCurrentElement().getClazz());
				current.setInstance(context.getCurrentElement().getInstance());
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				context.push(current);
			} else if (List.class.isAssignableFrom(info.valueType)) {
				logger.log(Level.DEBUG, "  expect LIST");
				if (info.valueType==List.class)
					info.valueType = ArrayList.class;
				List<?> list = (List<?>) info.valueType.getConstructor().newInstance();
				logger.log(Level.DEBUG, "  created list "+list);
				current = new CurrentElement(Type.ELEMENT_LIST,name);
				current.setInstance(list);
				ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
				logger.log(Level.DEBUG, "  after attributes list is "+list);
				current.setClazz(info.valueType);
				if (info.field!=null) {
					current.setExpect(ClassHelperTools.getElementMap(info.field, true));
				} else {
					current.setExpect(ClassHelperTools.getElementMap(info.valueType));
				}
				context.push(current);
			} else {
				logger.log(Level.DEBUG, "  expect ELEMENT");
				current = new CurrentElement(Type.ELEMENT, name);
				current.setClazz(info.valueType);
				// Is there an expected converter?
				if (info.elementConv!=null) {
					try {
						Object converted = info.elementConv.read(null, ev, context.eventReader);
						logger.log(Level.DEBUG, "  converted to "+converted);
						current.setInstance(converted);
						current.setExpect(ClassHelperTools.getElementMap(converted.getClass()));
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					if (Modifier.isAbstract(info.valueType.getModifiers())) {
						logger.log(Level.DEBUG, "Abstract "+info.valueType);
						String key = Persister.PREFIX_KEY_ABSTRACT+"."+info.valueType.getName();
						if (Persister.getKey(key)==null) {
							throw new SerializationException("Missing instructions how to create an instance abstract class '"+info.valueType+"'\n\nAdd\nPersister.putContext(Persister.PREFIX_KEY_ABSTRACT+\".\"+"+info.valueType.getSimpleName()+".class.getName(), <your class that extends it>);\nto your code");
						}
						Class<?> clazz = (Class<?>) Persister.getKey(key);
						logger.log(Level.DEBUG, "Instantiate "+clazz+" instead of "+info.valueType);
						info.valueType = clazz;
						try {
							current.setInstance(clazz.getConstructor().newInstance());
						} catch (InstantiationException e) {
							logger.log(Level.ERROR, "Failed instantiating "+clazz+" using the empty constructor: "+e);
							throw e;
						}
					} else {
						try {
							current.setInstance(info.valueType.getConstructor().newInstance());
						} catch (InstantiationException e) {
							logger.log(Level.ERROR, "Failed instantiating "+info.valueType+" using the empty constructor: "+e);
							throw e;
						}
					}
					try {
						ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
					} catch (IllegalStateException e) {
						logger.log(Level.ERROR, "Context: "+context);
						throw e;
					}
					current.setExpect(ClassHelperTools.getElementMap(info.valueType));
				}

				context.push(current);
			}
			return current;
		case MAP:
			logger.log(Level.DEBUG, "  Create new MAP_ITEM");
			listParent = current;
			current = new CurrentElement(Type.MAP_ITEM,name);;
			current.setKeyConverter(listParent.getKeyConverter());
			current.setElemConverter(listParent.getElemConverter());
			ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());

			Map<String,ByNameInfo> expect = new HashMap<String, ByNameInfo>();
			ParameterizedType type = (ParameterizedType) info.field.getGenericType();
			java.lang.reflect.Type valueType = type.getActualTypeArguments()[0];
			ByNameInfo expInfo = new ByNameInfo(AttrOrElem.ELEMENT, "key", (Class<?>) valueType);
			expInfo.field = info.field;
			expInfo.oper  = Operation.KEY;
			expect.put("key", expInfo);

			valueType = type.getActualTypeArguments()[1];
			expInfo = new ByNameInfo(AttrOrElem.ELEMENT, "value", (Class<?>) valueType);
			expInfo.field = info.field;
			expInfo.oper  = Operation.VAL;
			expect.put("value", expInfo);
			//			expect.put("value", new ByNameInfo(AttrOrElem.ELEMENT, "value", Object.class));
			current.setExpect(expect);

			context.push(current);
			return current;
		case MAP_ITEM:
			if ("key".equalsIgnoreCase(name)) {
				logger.log(Level.DEBUG, "  expect KEY/CDATA");
				current = new CurrentElement(Type.MAP_ITEM_KEY, name);
				current.setClazz(context.getCurrentElement().getClazz());
				current.setInstance(context.getCurrentElement().getInstance());
				context.push(current);
				return current;
			}
			if ("value".equalsIgnoreCase(name)) {
				logger.log(Level.DEBUG, "  expect ELEMENT (Map Value)");
				parent = current;
				current = new CurrentElement(Type.MAP_ITEM_VALUE, name);
				Class<?> toCreate = info.valueType;
				logger.log(Level.DEBUG, "  expected value type is "+toCreate);

				if (parent.getElemConverter()!=null) {
					logger.log(Level.DEBUG, "call element converter: "+parent.getElemConverter());
					try {
						Object converted = parent.getElemConverter().read(null, ev, context.eventReader);
						logger.log(Level.DEBUG, "conversion result= "+converted);
						current.setInstance(converted);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} else {
					// Get from the value the type to create
					javax.xml.stream.events.Attribute attrib = ev.getAttributeByName(new QName("type"));
					if (attrib!=null) {
						toCreate = Class.forName(attrib.getValue());
					}
					logger.log(Level.DEBUG, "  really create "+toCreate);
					current.setInstance(toCreate.getConstructor().newInstance());

					logger.log(Level.DEBUG, "Search for @Root");
					Root rootAnno = toCreate.getAnnotation(Root.class);
					if (rootAnno!=null) {
						expInfo = new ByNameInfo(AttrOrElem.ELEMENT, rootAnno.name(), toCreate);
						expInfo.oper = Operation.KEEP;
						expect = new HashMap<String, ByNameInfo>();
						expect.put(rootAnno.name(), expInfo);
						current.setExpect(expect);
					}
				}
				context.push(current);
				return current;
			}
			throw new RuntimeException("State "+name);
		case MAP_ITEM_VALUE:
			logger.log(Level.DEBUG, "  expect ELEMENT");
			current = new CurrentElement(Type.ELEMENT, name);
			current.setInstance(info.valueType.getConstructor().newInstance());
			current.setExpect(ClassHelperTools.getElementMap(info.valueType));
			ClassHelperTools.parseAttributes(info.valueType, ev, current.getInstance());
			context.push(current);
			return current;
		case ENUM_TEXT:
			nextType = getNextNodeType(info.field);
			logger.log(Level.DEBUG, "Expect "+nextType);
			parent = current;
			current = new CurrentElement(nextType, name);
			logger.log(Level.ERROR, "  expect CDATA   -->"+name+"//"+ev+" in "+current.getType());
		default:
			logger.log(Level.ERROR, "Unhandled "+current.getType());
			System.exit(1);
		}



		// Parse attributes
		if (current.getClazz()!=null) {
			ClassHelperTools.parseAttributes(current.getClazz(), ev, current.getInstance());
		}

		//		ByNameInfo info = current.getInfoForElement(name);
		//		if (info==null) {
		//			logger.log(Level.WARNING, "Found unexpected element '"+ev+"'. Ignore it and all children");
		//			System.err.println("Found unexpected element '"+ev+"'. Ignore it and all children");
		//			return new CurrentElement(Type.IGNORE);
		//		}
		////		logger.log(Level.DEBUG, "  Found expected element "+name+info.dump());
		//		current.expectedClose = info;

		return current;
	}
	//-------------------------------------------------------------------
	private void processEndElement(Context context, EndElement ev) throws SerializationException, IllegalArgumentException, IllegalAccessException {
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "STOP  "+ev);
			logger.log(Level.DEBUG, "  context: "+context);
		}
		CurrentElement current = context.pop();
		//		logger.log(Level.DEBUG, "  current stack is "+stck);
		//		logger.log(Level.DEBUG, "  current = "+stck.peek().dump());
		String name = ev.getName().getLocalPart();
		// As a safety measure, ensure that the closing element matches the current element
		if (!name.equalsIgnoreCase(current.getName()))
			throw new SerializationException("Parsing failed. Closing element '"+name+"' found, but expected '"+current.getName()+"'\nPath was "+context);

		CurrentElement parent = context.getCurrentElement();
		ByNameInfo info = parent.getExpect().get(name);

		Object value = current.getInstance();
		if (logger.isLoggable(Level.DEBUG)) {
			logger.log(Level.DEBUG, "  parent: "+parent);
			logger.log(Level.DEBUG, "  p.inst: "+parent.getInstance());
			logger.log(Level.DEBUG, "  value : "+value);
			logger.log(Level.DEBUG, "  info  : "+info);
		}

		logger.log(Level.DEBUG, "Action: "+info.toOperationString());

		// Return result
		if (parent.getType()==Type.ROOT) {
			parent.setInstance(value);
			return;
		}

		Object valToSet = current.getInstance();
		Field  toSet    = info.field;
		switch (info.oper) {
		case SET:
			logger.log(Level.DEBUG, "  set val= "+valToSet);
			logger.log(Level.DEBUG, "  set to = "+parent.getInstance());
			toSet.setAccessible(true);
			switch (current.getType()) {
			case TEXT:
				current.setInstance(ClassHelperTools.convertValue(toSet, (String)current.getInstance()));
				logger.log(Level.DEBUG, "  set "+current.getInstance()+" to "+toSet.getName()+" of "+parent.getInstance());
				toSet.set(parent.getInstance(), current.getInstance());
				break;
			case BINARY:
				// Convert String value as byte array
				try {
					byte[] convertedFromBase64 = Base64.getDecoder().decode( ((String)current.getInstance()));
					current.setInstance(convertedFromBase64);
					logger.log(Level.DEBUG, "  set "+current.getInstance()+" to "+toSet.getName()+" of "+parent.getInstance());
					toSet.set(parent.getInstance(), current.getInstance());
				} catch (IllegalArgumentException e) {
					logger.log(Level.ERROR, "Failed to decode Base64: "+current.getInstance(),e);
				}
				break;
			case ENUM_TEXT:
				Object conv = ClassHelperTools.convertValueToEnum(info.valueType, (String)value);
				logger.log(Level.DEBUG, "ENUM converted    = "+conv);
				logger.log(Level.DEBUG, "  set "+conv+" to "+toSet.getName()+" of "+parent.getInstance());
				toSet.set(parent.getInstance(), conv);
				break;
			default:
				toSet.set(parent.getInstance(), current.getInstance());
			}

			// Check for @AfterLoadHook
			AfterLoadHook annoHook = info.field.getAnnotation(AfterLoadHook.class);
			if (annoHook!=null) {
				try {
					annoHook.value().getDeclaredConstructor().newInstance().afterLoad(current.getInstance());
				} catch (Throwable e) {
					logger.log(Level.ERROR, "Failed calling AfterLoadHook "+annoHook.value(),e);
				}
			}
			break; // SET
		case ADD:
			logger.log(Level.DEBUG, "  add val= "+valToSet);
			logger.log(Level.DEBUG, "  add to = "+parent.getInstance()+"  (field = "+info.field+")");
			switch (current.getType()) {
			case BINARY:
				// Convert String value as byte array
				valToSet = Base64.getDecoder().decode( ((String)valToSet).getBytes());
				logger.log(Level.DEBUG, "  decoded Base64 binary");
				break;
			case ENUM_TEXT:
				valToSet = ClassHelperTools.convertValueToEnum(info.valueType, (String)value);
				logger.log(Level.DEBUG, "ENUM converted    = "+valToSet);
				break;
			default:
			}
			if (info.field!=null) {
				logger.log(Level.DEBUG, "add to list in field "+info.field.getName());
				logger.log(Level.DEBUG, "  parent is "+parent+"  with instance "+parent.getInstance());
				logger.log(Level.DEBUG, "  XXX type "+current.getType());
				toSet.setAccessible(true);
				List addTo = (List)info.field.get(parent.getInstance());
				if (addTo==null) {
					logger.log(Level.WARNING, "List in field "+info.field.getName()+" of "+parent.getInstance().getClass()+" is NULL");
					try {
						if (List.class==info.field.getType())
							addTo = new ArrayList<>();
						else
							addTo = (List) info.field.getType().getConstructor().newInstance();
						info.field.set(parent.getInstance(), addTo);
						logger.log(Level.DEBUG, "created new list for field "+info.field.getName()+" in "+parent.getInstance().getClass());
					} catch (Exception e) {
						logger.log(Level.ERROR, "Failed creating empty list",e);
					}
				}

				addTo.add(valToSet);

				// Check for @AfterLoadHook
				annoHook = info.field.getAnnotation(AfterLoadHook.class);
				if (annoHook!=null) {
					try {
						annoHook.value().getDeclaredConstructor().newInstance().afterLoad(current.getInstance());
					} catch (Throwable e) {
						logger.log(Level.ERROR, "Failed calling AfterLoadHook "+annoHook.value(),e);
					}
				}
			} else {
				logger.log(Level.DEBUG, "add to list which is the class itself");
				if (parent.getInstance() instanceof Map) {
					logger.log(Level.DEBUG, "shoehorning a map to a list");
					if (parent.getFromField()!=null && parent.getFromField().isAnnotationPresent(ElementList.class)) {
						ElementList eList = parent.getFromField().getAnnotation(ElementList.class);
						if (eList.addMethod()==null) {
							logger.log(Level.WARNING, "Cannot put a list into a map when no addMethod is specified");
						} else {
							try {
								for (Method method : context.getRootClass().getMethods()) {
									if (method.getName().equals(eList.addMethod()) && method.getParameterCount()==1) {
										method.invoke(context.getPreviousElement().getInstance(), valToSet);
										break;
									}
								}
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				} else {
					((List)parent.getInstance()).add(valToSet);
				}
			}

			break; // ADD
		case KEY:
			// Returned value is a map  key
			Object keyValue = current.getInstance();
			// Perhaps there is a key convert
			StringValueConverter<?> conv = parent.getKeyConverter();
			if (conv!=null) {
				try {
					keyValue = conv.read( (String)keyValue);
					logger.log(Level.DEBUG, "  converted Map key to "+keyValue);
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed converting Map key: "+e);
				}
			}
			logger.log(Level.DEBUG, "Map key = "+keyValue);
			parent.setKeyInstance(keyValue);
			break; // KEY
		case KEEP:
			// For Map key elements
			parent.setInstance(value);
			break; // KEEP
		case NONE:
			logger.log(Level.DEBUG, "Do nothing");
			break; // NONE
		case PUT:
			// Returned is a map element with key and value
			Object key = current.getKeyInstance();
			Object val = current.getInstance();
			logger.log(Level.DEBUG, " key/val = "+key+" = "+val);
			if (parent.getKeyConverter()!=null && ( key instanceof String)) {
				try {
					key = parent.getKeyConverter().read( (String)key);
				} catch (Exception e) {
					throw new SerializationException("Failed converting map key",e);
				}
			}
			// Value converter have been used during parsing
			((Map)parent.getInstance()).put(key, val);
			logger.log(Level.DEBUG, "  put into map: key="+key+"  val="+val);
			break; // PUT
		case VAL:
			// Returned value is a map value
			logger.log(Level.DEBUG, "Map value = "+value);
			parent.setInstance(value);
			break; // VAL
		default:
			throw new SerializationException("ToDo: "+info.oper);
		}

//		Field toSet = info.field;
//
//		// START OLD
//		switch (parent.getType()) {
//		case ROOT:
//			parent.setInstance(value);
//			break;
//		case ELEMENT_LIST:
//			if (info.field==null && (parent.getInstance() instanceof List)) {
//				logger.log(Level.DEBUG, "  add to parent, which is a list");
//				((List)parent.getInstance()).add(value);
//			} else
//
////			if ((info.field!=null && List.class.isAssignableFrom(info.field.getType())) || List.class.isAssignableFrom(info.valueType) ) {
//			if (List.class.isAssignableFrom(info.valueType) ) {
//				// Add to parent list
//				logger.log(Level.DEBUG, "Add to parent list: "+value);
//				logger.log(Level.DEBUG, "       parent list= "+parent.getInstance());
//				((List)parent.getInstance()).add(value);
//				break;
//			} else if (info.valueType==String.class) {
//				// Pass to element
//				logger.log(Level.DEBUG, "Treat as ELEMENT");
////			} else if (parent.getInstance() instanceof Map) {
////				// Don't do anything - the element has been added into
////				// the map, when the
//			} else  {
//				logger.log(Level.WARNING, "Unhandled situation");
////				throw new SerializationException("Huh?");
//			}
//		case ELEMENT:
//			// Set child in parent
//			toSet = info.field; // context.getPrepared(parent.getClazz(), name);
//			if (toSet==null) {
//				if (parent.getInstance() instanceof List) {
//					((List<Object>)parent.getInstance()).add(value);
//					break;
//				} else {
//					throw new SerializationException("Don't know how to set '"+name+"' in class "+parent.getClazz());
//				}
//			}
//			toSet.setAccessible(true);
//			// TODO: check for ENUM
//			logger.log(Level.DEBUG, "  closing "+current.getType());
//
//			try {
//			switch (current.getType()) {
//			case INLINE_ELEMENT:
////				if (current.getInstance() instanceof List) {
////					// Set
////					logger.log(Level.DEBUG, " don't overwrite list in "+parent.getInstance());
////
////				} else {
//					// Add
//					logger.log(Level.DEBUG, "  add "+parent.getInstance()+" to "+toSet);
//					try {
//						List<Object> list = (List<Object>) toSet.get(parent.getInstance());
//						if (list==current.getInstance())
//							throw new RuntimeException("Cannot add to self");
//						list.add(current.getInstance());
//					} catch (IllegalArgumentException | IllegalAccessException e1) {
//						// TODO Auto-generated catch block
//						e1.printStackTrace();
//					}
////				}
//				break;
//			case TEXT:
//					current.setInstance(ClassHelperTools.convertValue(toSet, (String)current.getInstance()));
//					logger.log(Level.DEBUG, "  set "+current.getInstance()+" to "+toSet.getName()+" of "+parent.getInstance());
//					toSet.set(parent.getInstance(), current.getInstance());
//				break;
//			case BINARY:
//				// Convert String value as byte array
//				byte[] convertedFromBase64 = Base64.getDecoder().decode( ((String)current.getInstance()).getBytes());
//				current.setInstance(convertedFromBase64);
//				logger.log(Level.DEBUG, "  set "+current.getInstance()+" to "+toSet.getName()+" of "+parent.getInstance());
//					toSet.set(parent.getInstance(), current.getInstance());
//				break;
//			case ENUM_TEXT:
//				Object conv = ClassHelperTools.convertValueToEnum(info.valueType, (String)value);
//				logger.log(Level.DEBUG, "ENUM converted    = "+conv);
//					logger.log(Level.DEBUG, "  set "+conv+" to "+toSet.getName()+" of "+parent.getInstance());
//					toSet.set(parent.getInstance(), conv);
//				break;
//			default:
//				logger.log(Level.DEBUG, "current.type = "+current.getType());
////				System.err.println("No special handling for "+current.getType());
//					logger.log(Level.DEBUG, "  set "+current.getInstance()+" to "+toSet.getName()+" of "+parent.getInstance());
//					toSet.set(parent.getInstance(), current.getInstance());
//			}
//			} catch (IllegalArgumentException | IllegalAccessException e) {
//				logger.log(Level.ERROR, "Set in obj  : "+parent.getInstance());
//				logger.log(Level.ERROR, "Set in class: "+parent.getInstance().getClass());
//				logger.log(Level.ERROR, "Field to set: "+toSet);
//				logger.log(Level.ERROR, "Obj to set  : "+current.getInstance());
//				logger.log(Level.ERROR, "Class to set: "+current.getInstance().getClass());
//				throw new SerializationException("Failed setting field "+toSet, e);
//			}
//
//			break; // ELEMENT
//		case INLINE_ELEMENT:
//			logger.log(Level.DEBUG, "found INLINE_ELEMENT");
//			// Set child in parent
//			toSet = info.field; // context.getPrepared(parent.getClazz(), name);
//			if (toSet==null) {
//				if (parent.getInstance() instanceof List) {
//					logger.log(Level.DEBUG, "Add inline element "+value+" to list "+parent.getInstance());
//					((List<Object>)parent.getInstance()).add(value);
//					break;
//				} else {
//					throw new SerializationException("Don't know how to set '"+name+"' in class "+parent.getClazz());
//				}
//			}
//			toSet.setAccessible(true);
//			try {
//				switch (current.getType()) {
//				case TEXT:
//					current.setInstance(ClassHelperTools.convertValue(toSet, (String)current.getInstance()));
//					logger.log(Level.DEBUG, "  set text '"+current.getInstance()+"' to "+toSet.getName()+" of "+parent.getInstance());
//					toSet.set(parent.getInstance(), current.getInstance());
//					break;
//				default:
//					// TODO: check for ENUM
//					logger.log(Level.WARNING, "  closing "+current.getType()+" for field "+toSet);
//					logger.log(Level.WARNING, "STOP  "+ev);
//					logger.log(Level.WARNING, "  context: "+context);
//					logger.log(Level.WARNING, "  parent: "+parent.getType());
//					logger.log(Level.WARNING, "  p.inst: "+parent.getInstance());
//					logger.log(Level.WARNING, "  value : "+value);
//					logger.log(Level.WARNING, "  info  : "+info);
//					System.exit(1);
//				}
//			} catch (IllegalArgumentException | IllegalAccessException e) {
//				logger.log(Level.ERROR, "Set in obj  : "+parent.getInstance());
//				logger.log(Level.ERROR, "Set in class: "+parent.getInstance().getClass());
//				logger.log(Level.ERROR, "Field to set: "+toSet);
//				logger.log(Level.ERROR, "Obj to set  : "+current.getInstance());
//				logger.log(Level.ERROR, "Class to set: "+current.getInstance().getClass());
//				throw new SerializationException("Failed setting field "+toSet, e);
//			}
//			break;
//		case MAP_ITEM:
//			logger.log(Level.DEBUG, "current.type = "+current.getType());
//			logger.log(Level.DEBUG, "parent.type = "+parent.getType());
//
//			if (current.getType()==Type.MAP_ITEM_KEY) {
//				logger.log(Level.DEBUG, "  Found finished Map key");
//				Object keyValue = current.getInstance();
//				// Perhaps there is a key convert
//				StringValueConverter<?> conv = parent.getKeyConverter();
//				if (conv!=null) {
//					try {
//						keyValue = conv.read( (String)keyValue);
//						logger.log(Level.DEBUG, "  converted Map key to "+keyValue);
//					} catch (Exception e) {
//						logger.log(Level.ERROR, "Failed converting Map key: "+e);
//					}
//				}
//				logger.log(Level.DEBUG, "Would set key to "+keyValue);
//				parent.setKeyInstance(keyValue);
//			} else if (current.getType()==Type.MAP_ITEM_VALUE) {
//				logger.log(Level.DEBUG, "  Found finished Map value "+current.getInstance());
//				parent.setInstance(current.getInstance());
//
//			} else
//				throw new IllegalStateException("Unexpected type "+current.getType());
//			break;
//		case MAP_ITEM_VALUE:
//			logger.log(Level.DEBUG, "End map value "+name+" with "+value);
//			parent.setInstance(value);
//			break;
//		case MAP:
//			// Assume  MAP_ITEM ended
//			Object key = current.getKeyInstance();
//			Object val = current.getInstance();
//			logger.log(Level.DEBUG, " key/val = "+key+" = "+val);
//			if (parent.getKeyConverter()!=null && ( key instanceof String)) {
//				try {
//					key = parent.getKeyConverter().read( (String)key);
//				} catch (Exception e) {
//					throw new SerializationException("Failed converting map key",e);
//				}
//			}
//			// Value converter have been used during parsing
//			((Map)parent.getInstance()).put(key, val);
//			logger.log(Level.DEBUG, "  put into map: key="+key+"  val="+val);
//			break;
//		case ENUM_TEXT:
//			logger.log(Level.DEBUG, "Finished item "+current.getClass()+" / "+parent.getClass());
//
//			break;
//		default:
//			logger.fatal("Not implemented yet: "+parent);
//			System.exit(1);
//		}
	}

	//-------------------------------------------------------------------
	private static Field getFieldWithCData(Class<?> clazz) {
		for (Field field : clazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(CData.class)) {
				return field;
			}
		}
		return null;
	}

	//-------------------------------------------------------------------
	private static Field getElementListWithString(Class<?> clazz) {
		for (Field field : clazz.getDeclaredFields()) {
			if (field.isAnnotationPresent(ElementList.class) && field.getAnnotation(ElementList.class).type()==String.class) {
				return field;
			}
		}
		return null;
	}
}
