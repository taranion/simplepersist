package org.prelle.simplepersist.unmarshal2;

enum Type {
	DOCUMENT,
	ROOT,
	ELEMENT,
	INLINE_ELEMENT,
	ELEMENT_LIST,
	TEXT, // CDATA value
	MAP,
	MAP_ITEM,
	MAP_ITEM_KEY,
	MAP_ITEM_VALUE,
	ENUM_TEXT,
	BINARY,
	INVALID, // Cannot be assigned to a field
}