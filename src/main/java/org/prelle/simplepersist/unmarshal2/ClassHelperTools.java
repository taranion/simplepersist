package org.prelle.simplepersist.unmarshal2;

import java.io.StringWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;
import java.util.function.BiFunction;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.EnumValue;
import org.prelle.simplepersist.IgnoreMissingAttributes;
import org.prelle.simplepersist.InterfaceResolver;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.PossibleNodesSupplier;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.Util;
import org.prelle.simplepersist.unmarshal2.ByNameInfo.AttrOrElem;
import org.prelle.simplepersist.unmarshal2.ByNameInfo.Operation;

/**
 * @author prelle
 *
 */
public class ClassHelperTools {

	private final static Logger logger = System.getLogger("xml");

	private final static DateFormat FORMAT2 = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.GERMANY);
	private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	private final static SimpleDateFormat FORMAT3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");
	
	private final static Map<Class<?>, BiFunction<Class<?>,String,?>> cachedResolver = new HashMap<>();
	private final static Map<Class<?>, PossibleNodesSupplier> cachedSupplier = new HashMap<>();

	//-------------------------------------------------------------------
	public static String decodeCDATA(String value) {
		return value
				.replaceAll("&lt;", "<")
				.replaceAll("&gt;", ">")
				.replaceAll("&#x003c;", "<")
				.replaceAll("&#x003e;", ">")
				.replaceAll("&amp;", "&")
				.replaceAll("&ldquo;", "\"")
				;
	}

	//-------------------------------------------------------------------
	public static String encodeCDATA(String value) {
		return value
				.replaceAll("&", "&amp;")
				.replaceAll("<", "&lt;")
				.replaceAll(">", "&gt;")
				.replaceAll("\"", "&#x0022;")
				;
	}

	//-------------------------------------------------------------------
	public static String dump(Map<String,ByNameInfo>  map) {
		if (map==null)
			return "NULLPOINTER";
		StringBuffer ret = new StringBuffer();
		List<String> sorted = new ArrayList<String>(map.keySet());
		Collections.sort(sorted);
		for (String key: sorted) {
			ByNameInfo info = map.get(key);
			String msg = String.format("  %12s  type %20s %s into field %s", info.elementName, info.valueType.getSimpleName(), info.oper,
					(info.field!=null)?info.field.getName():null);
			ret.append(msg+"\n");
		}
		return ret.toString();
	}

	//-------------------------------------------------------------------
	public static Object convertValueToEnum(Class<?> type, String sVal) throws SerializationException {
		Object toSet = null;
		/*
		 * If there is no @EnumValue annotation, just try the Enum.valueOf method
		 */
		Field[] flds = type.getDeclaredFields();
		boolean foundEnum = false;
		for (Field f : flds) {
			if (f.isEnumConstant() && f.isAnnotationPresent(EnumValue.class)) {
				EnumValue enumAnno = f.getAnnotation(EnumValue.class);
				logger.log(Level.DEBUG, "   Found @EnumValue annotation "+enumAnno);
				if (enumAnno.value().equals(sVal)) {
					logger.log(Level.DEBUG, "   Found enum by @EnumValue "+Enum.valueOf((Class<Enum>) type, f.getName()));
					toSet = Enum.valueOf((Class<Enum>) type, f.getName());
					foundEnum = true;
					break;
				}
			}
		}

		if (!foundEnum) {
			try {
				Method valueOf = type.getMethod("valueOf", String.class);
				toSet = valueOf.invoke(null, sVal);
				logger.log(Level.DEBUG, "  Found Enum by value: "+toSet);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed converting Enum value: "+e);
				throw new SerializationException("Illegal value '"+sVal+"' for enum constant in Enum "+type.getName(),e);
			}
		}
		return toSet;
	}

	//-------------------------------------------------------------------
	public static Object convertInterfaceValue(Class<?> type, String sVal) throws SerializationException {
		Object toSet = null;
		String key = Persister.PREFIX_KEY_INTERFACE+"."+type.getName();
//		logger.log(Level.WARNING, "key = "+key);
//		logger.log(Level.WARNING, "val = "+Persister.getKey(key));
		if (Persister.getKey(key)==null) {
			throw new SerializationException("Missing instructions how to convert value '"+sVal+"' to interface "+type+"\n\nAdd\nPersister.putContext(Persister.PREFIX_KEY_INTERFACE+\".\"+"+type.getSimpleName()+".class.getName(), <your ENUM that extends it>);\nto your code");
		}
		type = (Class<?>) Persister.getKey(key);
		/*
		 * If there is no @EnumValue annotation, just try the Enum.valueOf method
		 */
		Field[] flds = type.getDeclaredFields();
		boolean foundEnum = false;
		for (Field f : flds) {
			if (f.isEnumConstant() && f.isAnnotationPresent(EnumValue.class)) {
				EnumValue enumAnno = f.getAnnotation(EnumValue.class);
				logger.log(Level.DEBUG, "   Found @EnumValue annotation "+enumAnno);
				if (enumAnno.value().equals(sVal)) {
					logger.log(Level.DEBUG, "   Found enum by @EnumValue "+Enum.valueOf((Class<Enum>) type, f.getName()));
					toSet = Enum.valueOf((Class<Enum>) type, f.getName());
					foundEnum = true;
					break;
				}
			}
		}

		if (!foundEnum) {
			try {
				Method valueOf = type.getMethod("valueOf", String.class);
				toSet = valueOf.invoke(null, sVal);
				logger.log(Level.DEBUG, "  Found Enum by value: "+toSet);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed converting Enum value: "+e);
				throw new SerializationException("Illegal value '"+sVal+"' for enum constant in Enum "+type.getName(),e);
			}
		}
		return toSet;
	}

	//-------------------------------------------------------------------
	public static Object convertValue(Field field, String sVal) throws SerializationException {
		if (sVal==null || sVal.isEmpty())
			return null;

		field.setAccessible(true);
		if (field.getType()==String.class) {
			return decodeCDATA((String)sVal);
		} else if (field.getType()==char.class || field.getType()==Character.class) {
			return sVal.charAt(0);
		} else if (field.getType()==int.class || field.getType()==Integer.class) {
			return Integer.valueOf(sVal);
		} else if (field.getType()==double.class || field.getType()==Double.class) {
			return Double.valueOf(sVal);
		} else if (float.class==field.getType() || Float.class==field.getType()) {
			return Float.parseFloat(sVal);
		} else if (byte.class==field.getType() || Byte.class==field.getType()) {
			return Byte.valueOf(sVal);
		} else if (long.class==field.getType() || Long.class==field.getType()) {
			return Long.valueOf(sVal);
		} else if (boolean.class==field.getType() || field.getType()==Boolean.class) {
			if ("y".equalsIgnoreCase(sVal) || "yes".equalsIgnoreCase(sVal) || "1".equals(sVal))
				return  true;
			return Boolean.valueOf(sVal);
		} else if (UUID.class==field.getType()) {
			return UUID.fromString(sVal);
		} else if (Date.class==field.getType()) {
			try {
				return FORMAT.parse(sVal);
			} catch (ParseException e) {
				try {
					return FORMAT2.parse(sVal);
				} catch (ParseException ee) {
					try {
						return FORMAT3.parse(sVal);
					} catch (ParseException eee) {
						throw new SerializationException("Cannot parse as date: "+sVal,e);
					}
				}
			}
		} else if (byte[].class==field.getType()) {
			return Base64.getDecoder().decode( sVal.getBytes());
		} else if (field.getType().isEnum()) {
			// Enum found
			Object toSet = convertValueToEnum(field.getType(), sVal);
			logger.log(Level.DEBUG, "  set enum value: "+toSet);
			return toSet;
		} else if (field.getType().isInterface()) {
			// Assume this interface will be implemented by an Enum
			Object toSet = convertInterfaceValue(field.getType(), sVal);
			logger.log(Level.DEBUG, "  set interface value: "+toSet);
			return toSet;
		} else {
			throw new SerializationException("Don't know how to convert String to "+field.getType());
		}
	}

	//-------------------------------------------------------------------
	public static void setValue(Object obj, Field field, String sVal) throws SerializationException {
		field.setAccessible(true);
		try {
			field.set(obj,  convertValue(field, sVal));
		} catch (SerializationException e) {
			throw e;
		} catch (Exception e) {
			logger.log(Level.ERROR, "Failed setting field "+field.getName()+": "+e);
			convertValue(field, sVal);
			throw new SerializationException("Failed setting field "+field.getName()+": "+e,e);
		}
	}

	//-------------------------------------------------------------------
	public static void parseAttributes(Class<?> type, StartElement elem, Object obj) throws SerializationException {
		if (type==null)
			throw new NullPointerException();
		// Build a list of possible fields
		Map<String, Field> fieldsByAttribute = new HashMap<String, Field>();
		for (Field field : Util.getAttributeFields(type)) {
			Attribute attr = field.getAnnotation(Attribute.class);
			if (attr.name()!=null && attr.name().length()>0)
				fieldsByAttribute.put(attr.name(), field);
			else
				fieldsByAttribute.put(field.getName(), field);
		}


		Iterator<javax.xml.stream.events.Attribute> iter = elem.getAttributes();
		while( iter.hasNext() ) {
			javax.xml.stream.events.Attribute a = iter.next();
			if (logger.isLoggable(Level.TRACE))
				logger.log(Level.TRACE, "Attr "+a);
			String name = a.getName().getLocalPart();
			String sVal = a.getValue();

			Field field = fieldsByAttribute.get(name);
			if (field==null) {
				logger.log(Level.ERROR, "Found no field '"+name+"' in type "+type+" -  element was <"+elem.getName()+"> at line "+elem.getLocation().getLineNumber());
				System.err.println("Found no field '"+name+"' in type "+type+"  Line: "+elem.getLocation().getLineNumber());
				Persister.errorFound=true;
				continue;
			}
//			if (!field.getType().isAssignableFrom(obj.getClass()))
//				logger.log(Level.ERROR, "Error - expect obj to be of type "+field.getType()+" but got "+obj.getClass());

			if (logger.isLoggable(Level.TRACE))
				logger.log(Level.TRACE, "  write to "+field);
			field.setAccessible(true);

			StringValueConverter<?> conv = null;
			if (field.isAnnotationPresent(AttribConvert.class)) {
				Class<? extends StringValueConverter<?>> convClazz = field.getAnnotation(AttribConvert.class).value();
				try {
					conv = Util.createAttribConverter(convClazz);
				} catch (IllegalArgumentException | SecurityException e) {
					// TODO Auto-generated catch block
					throw new SerializationException("Cannot instantiate constructor of @AttribConvert "+convClazz,e);
				}
				logger.log(Level.DEBUG, "  Attribute converter is "+conv);
			}

			try {
				fieldsByAttribute.remove(name);
				if (conv!=null) {
					try {
						Object toSet = conv.read(sVal);
						logger.log(Level.DEBUG, "  Converted "+sVal+" to "+toSet);
						field.set(obj, toSet);
					} catch (Exception e) {
						System.err.println("Failed converting attribute '"+a+"' of "+elem+" with  '\"+sVal+\"': "+e);
						logger.log(Level.ERROR, "Failed converting attribute '"+a+"' of "+elem+" with  '\"+sVal+\"': "+e,e);
					}
				} else {
					logger.log(Level.DEBUG, "set attribute "+field.getName()+" to "+sVal);
					setValue(obj, field, sVal);
				}
			} catch (SerializationException e) {
				throw e;
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		/*
		 * Before checking unfulfilled attribute requirements, build a list
		 * of attributes where requirements can be explicitly ignored
		 */
		List<String> ignoreMissing = new ArrayList<>();
		if (type.isAnnotationPresent(IgnoreMissingAttributes.class)) {
			IgnoreMissingAttributes anIgn = type.getAnnotation(IgnoreMissingAttributes.class);
			String workOn = anIgn.value();
			if (workOn!=null) {
				for (String tmp : workOn.split(",")) {
					ignoreMissing.add(tmp.trim());
				}
			}
		}

		//		logger.log(Level.WARNING, "TODO: Unfilled attributes "+fieldsByAttribute);
		for (Field field : fieldsByAttribute.values()) {
			Attribute anno = field.getAnnotation(Attribute.class);
			String name = field.getName();
			if (anno!=null && anno.name()!=null && !anno.name().isBlank())
				name = anno.name();
			if (anno.required() && !ignoreMissing.contains(name)) {
				StringWriter wout = new StringWriter();
				try {
					elem.writeAsEncodedUnicode(wout);
				} catch (XMLStreamException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if (ignoreMissing!=null && !ignoreMissing.isEmpty())
					throw new IllegalStateException("Missing required attribute '"+name+"/"+field+"' of elem "+wout.toString()+" and "+name+" not in ingore list "+ignoreMissing);
				else
					throw new IllegalStateException("Missing required attribute '"+name+"/"+field+"' of elem "+wout.toString()+"");

			}
		}
	}

	//-------------------------------------------------------------------
	private static void addFromListUnion(List<ByNameInfo> ret, ElementListUnion anno) {
		for (ElementList listAnno : anno.value()) {
			ByNameInfo single = new ByNameInfo();
			single.valueType = listAnno.type();
			if (Modifier.isAbstract(single.valueType.getModifiers())) {
				Class<?> newCls = Persister.getRealClass(single.valueType);
				logger.log(Level.DEBUG, "Replace abstract class {0} with {1}",single.valueType.getSimpleName(), newCls.getSimpleName());
				single.valueType = newCls;
			}
			single.oper = Operation.ADD;
			if (listAnno.entry()!=null && !listAnno.entry().isEmpty()) {
				single.elementName = listAnno.entry();
				ret.add(single);
			} 
		}
	}

	//-------------------------------------------------------------------
	private static void addFromListUnion(Map<String,ByNameInfo> ret, ElementListUnion anno) {
		for (ElementList listAnno : anno.value()) {
			ByNameInfo single = new ByNameInfo();
			single.valueType = listAnno.type();
			if (Modifier.isAbstract(single.valueType.getModifiers())) {
				Class<?> newCls = Persister.getRealClass(single.valueType);
				logger.log(Level.DEBUG, "Replace abstract class {0} with {1}",single.valueType.getSimpleName(), newCls.getSimpleName());
				single.valueType = newCls;
			}
			single.oper = Operation.ADD;
			if (listAnno.entry()!=null && !listAnno.entry().isEmpty()) {
				single.elementName = listAnno.entry();
				ret.put(listAnno.entry(),single);
			} 
		}
	}

	//-------------------------------------------------------------------
	private static void addFromListSupplier(Map<String,ByNameInfo> ret, ElementList anno) {
		if (anno.inline() && anno.supplier()!=PossibleNodesSupplier.NO_PROVIDER.class) {
			try {
				PossibleNodesSupplier supplier = anno.supplier().getConstructor().newInstance();
				Map<String,Class<?>> toAdd = supplier.get();
				for (Entry<String, Class<?>> tmp : toAdd.entrySet()) {
					ByNameInfo single = new ByNameInfo();
					single.valueType = tmp.getValue();
					single.elementName = tmp.getKey();
					single.oper      = Operation.ADD;
					ret.put(tmp.getKey(),single);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	//-------------------------------------------------------------------
	public static List<ByNameInfo> convertField(Field field) {
		List<ByNameInfo> ret = new ArrayList<ByNameInfo>();
		ByNameInfo info = new ByNameInfo();
		info.field = field;
		// By default, the element name matches the field name
		info.elementName = field.getName();
		info.valueType   = field.getType();


		if (List.class.isAssignableFrom(field.getType())) {
			// This is a list
			if (field.getType().isAnnotationPresent(ElementListUnion.class)) {
				ElementListUnion anno = field.getType().getAnnotation(ElementListUnion.class);
				addFromListUnion(ret, anno);
			}
			
			if (field.isAnnotationPresent(ElementList.class)) {
				ElementList anno = field.getAnnotation(ElementList.class);
				if (anno.inline()) {
					// If it has a supplier, use it first
					if (anno.supplier()!=null && !Modifier.isAbstract(anno.supplier().getModifiers())) {
						// There is a supplier to consult
						PossibleNodesSupplier supplier = null;
                        if (cachedSupplier.containsKey(anno.supplier())) {
                        	supplier = cachedSupplier.get(anno.supplier());
                        } else {
                            try {
                            	supplier = anno.supplier().getConstructor().newInstance();
                                cachedSupplier.put(anno.supplier(), supplier);
                           } catch (Exception e) {
                               logger.log(Level.ERROR, "Failed instantiating supplier {0}", anno.supplier());
                               e.printStackTrace();
                               System.exit(1);
                           }
                        }
                        logger.log(Level.DEBUG, "Consult supplier {0}",supplier);
                        if (supplier!=null && supplier.get()!=null) {
    						try {
    							for (Entry<String, Class<?>> entry : supplier.get().entrySet()) {
    								ByNameInfo single = new ByNameInfo();
    								single.oper = Operation.ADD;
    								single.attributeOrElement = AttrOrElem.ELEMENT;
    								single.valueType = entry.getValue();
    								single.elementName = entry.getKey();
    								logger.log(Level.TRACE, "Add {0}={1}", single.elementName, single.valueType);
    								ret.add(single);
    							}
    						} catch (Exception e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
                        }
					}
					
					
					ByNameInfo single = new ByNameInfo();
					single.valueType = anno.type();
                    if (single.valueType.isInterface() && anno.resolver()!=InterfaceResolver.class) {
//                        logger.log(Level.WARNING, "Interface {0} in {1} use resolver {2}",single.valueType.getSimpleName(), field, anno.resolver());
                        if (cachedResolver.containsKey(anno.resolver())) {
                            single.resolver = cachedResolver.get(anno.resolver());
                        } else {
                            try {
                                single.resolver = anno.resolver().getConstructor().newInstance();
                               cachedResolver.put(anno.resolver(), single.resolver);
                           } catch (Exception e) {
                               // TODO Auto-generated catch block
                               e.printStackTrace();
                           }
                        }
                    } else if (Modifier.isAbstract(single.valueType.getModifiers())) {
                    	if (!ret.isEmpty())
                    		return ret;
                       	Class<?> newCls = Persister.getRealClass(single.valueType);
                       	logger.log(Level.DEBUG, "Replace abstract class {0} with {1} in field {2}",single.valueType.getSimpleName(), newCls.getSimpleName(), field);
                       	single.valueType = newCls;
					}
					single.oper = Operation.ADD;
					single.attributeOrElement = AttrOrElem.ELEMENT;
					if (anno.entry()!=null && !anno.entry().isEmpty()) {
						single.elementName = anno.entry();
					} else  {
						Root rootAnno = anno.type().getAnnotation(Root.class);
						if (rootAnno!=null) {
							single.elementName = rootAnno.name();
						} else
							throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementList's type "+field.getType());
					}
					ret.add(single);
				} else {
					// Not inline
					info.required = anno.required();
					info.oper = Operation.SET;
					info.attributeOrElement = AttrOrElem.ELEMENT;
					info.required = anno.required();
					ret.add(info);
				}
			} else if (field.isAnnotationPresent(ElementListUnion.class)) {
				ElementListUnion anno = field.getAnnotation(ElementListUnion.class);
				for (ElementList listAnno : anno.value()) {
					ByNameInfo single = new ByNameInfo();
					single.valueType = listAnno.type();
					if (Modifier.isAbstract(single.valueType.getModifiers())) {
						Class<?> newCls = Persister.getRealClass(single.valueType);
						logger.log(Level.DEBUG, "Replace abstract class {0} with {1} in field {2}",single.valueType.getSimpleName(), newCls.getSimpleName(), field);
						single.valueType = newCls;
					}
					single.oper = Operation.ADD;
					if (listAnno.entry()!=null && !listAnno.entry().isEmpty()) {
						single.elementName = listAnno.entry();
					} else  {
						Root rootAnno = listAnno.type().getAnnotation(Root.class);
						if (rootAnno!=null) {
							single.elementName = rootAnno.name();
						} else
							throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementList's type "+field.getType());
					}
					ret.add(single);
				}

			} else if (field.isAnnotationPresent(Element.class)) {
				Element anno = field.getAnnotation(Element.class);
				info.required = anno.required();
				if (anno.name()!=null && !anno.name().isBlank())
					info.elementName = anno.name();
				info.required = anno.required();
				info.valueType = field.getType();
				info.oper = Operation.SET;
				ret.add(info);
			}

		} else {
			// Not a list
			if (field.isAnnotationPresent(Element.class)) {
				Element anno = field.getAnnotation(Element.class);
				if (anno.name()!=null && !anno.name().isBlank())
					info.elementName = anno.name();
				info.required = anno.required();
				info.valueType = field.getType();
				info.attributeOrElement = AttrOrElem.ELEMENT;
				info.oper = Operation.SET;
			} else if (field.isAnnotationPresent(Attribute.class)) {
				Attribute anno = field.getAnnotation(Attribute.class);
				if (anno.name()!=null && !anno.name().isBlank())
					info.elementName = anno.name();
				info.required = anno.required();
				info.valueType = field.getType();
				info.attributeOrElement = AttrOrElem.ATTRIBUTE;
			}
			ret.add(info);
		}


		return ret;
	}

	//-------------------------------------------------------------------
	public static Map<String,ByNameInfo> getElementMap(Class<?> clazz) {
		Map<String, ByNameInfo> ret = new HashMap<>();
		// Abstract classes need to be replaced
		if (Modifier.isAbstract(clazz.getModifiers()) && !Collection.class.isAssignableFrom(clazz) && !Map.class.isAssignableFrom(clazz) && !clazz.isInterface()) {
			clazz = Persister.getRealClass(clazz);
		}


		// Special case: Root class is a list
		if (clazz.isAnnotationPresent(ElementList.class)) {
			ElementList anno = clazz.getAnnotation(ElementList.class);
			ByNameInfo info = new ByNameInfo();
			info.oper = Operation.ADD;
			// If the inline list has an "entry" attribut, this will be
			// the expected element name, otherwise the @Root of the
			// list "type" defines the name
			info.valueType = anno.type();
			if (Modifier.isAbstract(info.valueType.getModifiers()) && !info.valueType.isInterface()) {
				try {
					Class<?> newCls = Persister.getRealClass(info.valueType);
					logger.log(Level.DEBUG, "Replace abstract class {0} with {1} in class {2}",info.valueType.getSimpleName(), newCls.getSimpleName(), clazz);
					info.valueType = newCls;
				} catch (Exception e) {
					System.out.println("ClassHelperTool: Error in @ElementList for / "+info.valueType+"\n"+e);
					throw e;
				}
			}
			// Check if there is a dynamic supplier
			boolean hasSupplier = false;
			if (anno.supplier()!=PossibleNodesSupplier.NO_PROVIDER.class) {
				// Dynamic elements
				hasSupplier = true;
				try {
					PossibleNodesSupplier provider = anno.supplier().getConstructor().newInstance();
					for (Entry<String,Class<?>> entry : provider.get().entrySet()) {
						ByNameInfo dynInfo = new ByNameInfo();
						dynInfo.oper = Operation.ADD;
						dynInfo.valueType = entry.getValue();
						dynInfo.elementName = entry.getKey();
						ret.put(dynInfo.elementName, dynInfo);
					}
				} catch (Exception e) {
					logger.log(Level.ERROR, "Error calling "+anno.supplier(),e);
				}
			}
			
			if (anno.entry()!=null && !anno.entry().isEmpty()) {
				info.elementName = anno.entry();
			} else  {
				Root rootAnno = anno.type().getAnnotation(Root.class);
				if (rootAnno!=null) {
					info.elementName = rootAnno.name();
				} else if (!hasSupplier)
					throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementList's type "+anno.type());
			}
			// Converter?
			if (anno.convert()!=null && anno.convert()!=ElementConvert.NOELEMENTCONVERTER.class) {
				logger.log(Level.ERROR, "TOP");
				try {
					info.elementConv = anno.convert().getConstructor().newInstance();
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed instantiating "+anno.convert(),e);
				}
			}

			if (info.elementName!=null)
			ret.put(info.elementName, info);
		}

		// Special case: Root class is a list union
		if (clazz.isAnnotationPresent(ElementListUnion.class)) {
			ElementListUnion anno = clazz.getAnnotation(ElementListUnion.class);
			for (ElementList listAnno : anno.value()) {
				ByNameInfo info = new ByNameInfo();
				info.valueType = listAnno.type();
				info.oper = Operation.ADD;
				if (listAnno.entry()!=null && !listAnno.entry().isEmpty()) {
					info.elementName = listAnno.entry();
				} else  {
					Root rootAnno = listAnno.type().getAnnotation(Root.class);
					if (rootAnno!=null) {
						info.elementName = rootAnno.name();
					} else
						throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementListUnion's type "+clazz);
				}
				ret.put(info.elementName, info);
			}
		}


		for (Field field : Util.getElementFields(clazz)) {
			for (ByNameInfo info : convertField(field)) {
				info.field = field;
				ret.put(info.elementName, info);
			}
		}

		//		for (Field field : Util.getAttributeFields(clazz)) {
		//			ByNameInfo info = convertField(field);
		//				info.field = field;
		//				ret.put(info.elementName, info);
		//		}

		return ret;
	}

	//-------------------------------------------------------------------
	public static Map<String,ByNameInfo> getElementMap(Field field, boolean isListElement) {
		Map<String, ByNameInfo> ret = new HashMap<>();

		if (field.isAnnotationPresent(ElementListUnion.class)) {
			addFromListUnion(ret,  field.getAnnotation(ElementListUnion.class));
		}
		if (field.getType().isAnnotationPresent(ElementListUnion.class)) {
			addFromListUnion(ret,  field.getType().getAnnotation(ElementListUnion.class));
		}
		if (field.getType().isAnnotationPresent(ElementList.class)) {
			addFromListSupplier(ret,  field.getType().getAnnotation(ElementList.class));
		}

		if (field.isAnnotationPresent(ElementList.class)) {
			ElementList anno = field.getAnnotation(ElementList.class);
			ByNameInfo info = new ByNameInfo();
			// If the inline list has an "entry" attribut, this will be
			// the expected element name, otherwise the @Root of the
			// list "type" defines the name
			info.valueType = anno.type();
			info.field = anno.inline()?field:null;
			info.oper  = (anno.inline() || isListElement)?Operation.ADD:Operation.SET;
			if (anno.entry()!=null && !anno.entry().isEmpty()) {
				info.elementName = anno.entry();
			} else  {
				Root rootAnno = anno.type().getAnnotation(Root.class);
				if (rootAnno!=null) {
					info.elementName = rootAnno.name();
				} 
				else
					System.out.println("Hier");
//					throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementList's type "+anno.type());
			}
			// Converter?
			if (anno.convert()!=null && anno.convert()!=ElementConvert.NOELEMENTCONVERTER.class) {
				try {
					info.elementConv = Util.createElementConverter(anno.convert());
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed instantiating "+anno.convert(),e);
				}
			}

			if (info.elementName!=null)
				ret.put(info.elementName, info);
		}

		if (field.isAnnotationPresent(ElementListUnion.class)) {
			ElementListUnion anno = field.getAnnotation(ElementListUnion.class);
			for (ElementList listAnno : anno.value()) {
				ByNameInfo info = new ByNameInfo();
				info.valueType = listAnno.type();
				info.oper = Operation.ADD;
				if (listAnno.entry()!=null && !listAnno.entry().isEmpty()) {
					info.elementName = listAnno.entry();
				} else  {
					Root rootAnno = listAnno.type().getAnnotation(Root.class);
					if (rootAnno!=null) {
						info.elementName = rootAnno.name();
					} else
						throw new IllegalStateException("Missing @ElementList 'entry' or @Root in @ElementListUnion's type "+field);
				}
				ret.put(info.elementName, info);
			}
		}

		ret.putAll(getElementMap(field.getType()));
		return ret;
	}

}
