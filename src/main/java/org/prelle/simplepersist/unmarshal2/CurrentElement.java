/**
 *
 */
package org.prelle.simplepersist.unmarshal2;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.XMLElementConverter;

/**
 * @author prelle
 *
 */
public class CurrentElement {

	private Type type;
	private String name;
	private Class<?> clazz;
	private Object instance;
	private Object keyInstance;
	private Field fromField;

	private Map<String,ByNameInfo> expect;

	private XMLElementConverter<?>  elemConverter;
	private StringValueConverter<?> keyConverter;

	//-------------------------------------------------------------------
	public CurrentElement(Type type, String name) {
		this.type = type;
		this.name = name;
		expect = new HashMap<String, ByNameInfo>();
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("\n    "+type);
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String toString() {
		switch (type) {
		case DOCUMENT:
		case ROOT:
			return type.name();
		default:
			return type.name()+"("+name+")";
		}
	}

//	//-------------------------------------------------------------------
//	void addAttribute(String name, Class<?> cls, Field field, Object instance) {
//		ByNameInfo info = expectedAttributes.get(name);
//		if (info==null) {
//			info = new ByNameInfo();
//			expectedAttributes.put(name, info);
//		}
//		info.cls = cls;
//		info.field = field;
//		info.fieldInstance = instance;
//	}
//
//	//-------------------------------------------------------------------
//	void addElement(String name, Class<?> cls, Field field, Object instance) {
//		ByNameInfo info = expectedElements.get(name);
//		if (info==null) {
//			info = new ByNameInfo();
//			expectedElements.put(name, info);
//		}
//		info.cls = cls;
//		info.field = field;
//		info.fieldInstance = instance;
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public Class<?> getClazz() {
		return clazz;
	}

	public void setClazz(Class<?> clazz) {
		this.clazz = clazz;
	}

	public Object getInstance() {
		return instance;
	}

	public void setInstance(Object instance) {
		this.instance = instance;
	}

	public Map<String, ByNameInfo> getExpect() {
		return expect;
	}

	public void setExpect(Map<String, ByNameInfo> expect) {
		this.expect = expect;
	}

	public XMLElementConverter<?> getElemConverter() {
		return elemConverter;
	}

	public void setElemConverter(XMLElementConverter<?> elemConverter) {
		this.elemConverter = elemConverter;
	}

	public StringValueConverter<?> getKeyConverter() {
		return keyConverter;
	}

	public void setKeyConverter(StringValueConverter<?> keyConverter) {
		this.keyConverter = keyConverter;
	}

	public Object getKeyInstance() {
		return keyInstance;
	}

	public void setKeyInstance(Object keyInstance) {
		this.keyInstance = keyInstance;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the fromField
	 */
	public Field getFromField() {
		return fromField;
	}

	//-------------------------------------------------------------------
	/**
	 * @param fromField the fromField to set
	 */
	public void setFromField(Field fromField) {
		this.fromField = fromField;
	}

}
