/**
 *
 */
package org.prelle.simplepersist.unmarshal2;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;

/**
 * @author prelle
 *
 */
public class Context {

	private final static Logger logger = System.getLogger("xml");

	private Stack<CurrentElement>  stckCurrent = new Stack<CurrentElement>();

	private Class<?> rootClass;
	private String rootElement;
	private String mainNamespace;

	private Map<Class<?>, Map<String, ByNameInfo>> fieldsByElementByClass;

	private Map<Class<?>, Object> converterInstances;

	public XMLEventReader eventReader;

	//-------------------------------------------------------------------
	public Context(Class<?> cls, String rootName) {
		rootClass = cls;
		rootElement = rootName;

//		stckCurrent = new Stack<CurrentElement>();
//		stckCurrent.push(new CurrentElement(Type.ROOT));
		fieldsByElementByClass = new HashMap<>();
		converterInstances = new HashMap<Class<?>, Object>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return stckCurrent+"/"+stckCurrent.peek().getClazz();
	}

	//-------------------------------------------------------------------
	public void push(CurrentElement toAdd) {
		stckCurrent.push(toAdd);
	}

	//-------------------------------------------------------------------
	public CurrentElement getCurrentElement() {
		return stckCurrent.peek();
	}

	//-------------------------------------------------------------------
	public CurrentElement getPreviousElement() {
		return stckCurrent.get(stckCurrent.size()-2);
	}

	//-------------------------------------------------------------------
	public CurrentElement pop() {
		return stckCurrent.pop();
	}

	//-------------------------------------------------------------------
	public Class<?> getRootClass() {
		return rootClass;
	}

	//-------------------------------------------------------------------
	public String getRootElement() {
		return rootElement;
	}

	//-------------------------------------------------------------------
	public boolean isPrepared(Class<?> key) {
		return fieldsByElementByClass.containsKey(key);
	}

	//-------------------------------------------------------------------
	public Map<String, ByNameInfo> getElementInfo(Class<?> key) {
		Map<String, ByNameInfo> map = fieldsByElementByClass.get(key);
		if (map==null) {
			map = ClassHelperTools.getElementMap(key);
			fieldsByElementByClass.put(key, map);
		}
		return map;
	}

//	//-------------------------------------------------------------------
//	public void prepare(Class<?> key) {
//		if (key==null)
//			return;
//		if (key==String.class)
//			return;
//		if (key==List.class || key==ArrayList.class)
//			return;
//
//		Map<String, ByNameInfo> map = ClassHelperTools.getElementMap(key);
//		fieldsByElementByClass.put(key,  map);
////		// Prepare fields of the class
////		for (Field field : Util.getAllFields(key)) {
////			if (Modifier.isTransient(field.getModifiers()))
////				continue;
////
////			// Is it an attribute
////			if (field.isAnnotationPresent(Attribute.class)) {
////				Attribute attr = field.getAnnotation(Attribute.class);
////				if (attr.name()!=null && !attr.name().isEmpty()) {
////					map.put(attr.name(), field);
////				} else {
////					map.put(field.getName(), field);
////				}
////			}
////			// Is it an element
////			else if (field.isAnnotationPresent(Element.class)) {
////				Element attr = field.getAnnotation(Element.class);
////				if (attr.name()!=null && !attr.name().isEmpty()) {
////					map.put(attr.name(), field);
////				} else {
////					map.put(field.getName(), field);
////				}
////			}
////			// Is it an element list
////			else if (field.isAnnotationPresent(ElementList.class)) {
////				ElementList attr = field.getAnnotation(ElementList.class);
////				if (!attr.inline()) {
////					map.put(field.getName(), field);
////				}
////			}
////			// Other
////			else  {
////				logger.warn("Not implemented yet for field "+field+"   \t: "+Arrays.toString(field.getAnnotations()));
////			}
////		}
//
//	}
//
//	//-------------------------------------------------------------------
//	public Map<String,ByNameInfo> getPrepared(Class<?> key) {
//		return fieldsByElementByClass.get(key);
//	}

	//-------------------------------------------------------------------
	public ByNameInfo getPrepared(Class<?> key, String element) {
		if (!fieldsByElementByClass.containsKey(key))
			return null;
		return fieldsByElementByClass.get(key).get(element);
	}

//	//-------------------------------------------------------------------
//	public void prepateEventualInlineList(CurrentElement current) {
//		// TODO Auto-generated method stub
//
//	}

	//-------------------------------------------------------------------
	public <T> T getConverterInstance(Class<T> cls) {
		T inst = (T) converterInstances.get(cls);
		if (inst==null) {
			try {
				inst = cls.getConstructor().newInstance();
				converterInstances.put(cls,inst);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed to create converter: "+cls,e);
			}
		}
		return inst;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the mainNamespace
	 */
	public String getMainNamespace() {
		return mainNamespace;
	}

	//-------------------------------------------------------------------
	/**
	 * @param mainNamespace the mainNamespace to set
	 */
	public void setMainNamespace(String mainNamespace) {
		this.mainNamespace = mainNamespace;
	}

}
