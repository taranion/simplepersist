package org.prelle.simplepersist;

public enum AccessMethod {
	DIRECT,
	METHOD
}