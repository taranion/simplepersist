/**
 * 
 */
package org.prelle.simplepersist;

/**
 * @author prelle
 *
 */
public class MapEntry {
	
	private Object key;
	private Object value;

	//-------------------------------------------------------------------
	/**
	 */
	public MapEntry() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "MapEntry("+key+"="+value+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the key
	 */
	public Object getKey() {
		return key;
	}

	//-------------------------------------------------------------------
	/**
	 * @param key the key to set
	 */
	public void setKey(Object key) {
		this.key = key;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}

}
