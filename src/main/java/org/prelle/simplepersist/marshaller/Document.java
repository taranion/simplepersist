/**
 * 
 */
package org.prelle.simplepersist.marshaller;

/**
 * @author prelle
 *
 */
public class Document {

	private XmlNode root;
	
	//-------------------------------------------------------------------
	/**
	 */
	public Document() {
		// TODO Auto-generated constructor stub
	}

	//-------------------------------------------------------------------
	public XmlNode getRoot() {
		return root;
	}

	//-------------------------------------------------------------------
	public void setRoot(XmlNode root) {
		this.root = root;
	}
	

}
