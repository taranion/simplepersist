/**
 *
 */
package org.prelle.simplepersist.marshaller;

import java.lang.reflect.Field;

/**
 * @author prelle
 *
 */
public class PureCData extends XmlNode {

	private String value;

	//-------------------------------------------------------------------
	public PureCData(Field field, String val) {
		super(null, field);
		this.value = val;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
