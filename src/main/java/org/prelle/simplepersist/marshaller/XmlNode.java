/**
 *
 */
package org.prelle.simplepersist.marshaller;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author prelle
 *
 */
public class XmlNode {

	private Field field;
	private String name;
	private Map<String,String> attributes;
	private List<XmlNode> children;
	private String cData;

	//-------------------------------------------------------------------
	public XmlNode(String name, Field field) {
		this.field = field;
		this.name = name;
		attributes = new HashMap<String, String>();
		children  = new ArrayList<XmlNode>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return "XmlNode("+name+")";
	}

	//-------------------------------------------------------------------
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public boolean isLeaf() {
		return children.isEmpty();
	}

	//-------------------------------------------------------------------
	public List<XmlNode> getChildren() {
		return children;
	}

	//-------------------------------------------------------------------
	public void setAttribute(String key, String value) {
		attributes.put(key, value);
	}

	//-------------------------------------------------------------------
	public Map<String,String> getAttribute() {
		return attributes;
	}

	//-------------------------------------------------------------------
	public Field getField() {
		return field;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cData
	 */
	public String getcData() {
		return cData;
	}

	//-------------------------------------------------------------------
	/**
	 * @param cData the cData to set
	 */
	public void setcData(String cData) {
		this.cData = cData;
	}

}
