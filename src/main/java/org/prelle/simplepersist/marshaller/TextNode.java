/**
 * 
 */
package org.prelle.simplepersist.marshaller;

import java.lang.reflect.Field;

/**
 * @author prelle
 *
 */
public class TextNode extends XmlNode {
	
	private String value;

	//-------------------------------------------------------------------
	public TextNode(String name, Field field, String val) {
		super(name, field);
		this.value = val;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

}
