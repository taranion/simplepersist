package org.prelle.simplepersist.marshaller;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.CData;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.EnumValue;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.PossibleNodesSupplier;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.Util;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.unmarshal2.ClassHelperTools;

/**
 * @author prelle
 *
 */
public class Marshaller {

	private final static Logger logger = System.getLogger("xml");

	private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");

	//-------------------------------------------------------------------
	private static Iterable<Field> getAllFields(Class<?> startClass) {

		List<Field> currentClassFields = new ArrayList<Field>(Arrays.asList(startClass.getDeclaredFields()));
		Class<?> parentClass = startClass.getSuperclass();

		if (parentClass != Object.class && parentClass!=null) {
			List<Field> parentClassFields = (List<Field>) getAllFields(parentClass);
			currentClassFields.addAll(parentClassFields);
		}

		return currentClassFields;
	}

	//-------------------------------------------------------------------
	private static Iterable<Field> getAttributeFields(Class<?> cls) {

		List<Field> fields = new ArrayList<Field>();
		for (Field field : getAllFields(cls)) {
			if (field.isAnnotationPresent(Attribute.class)) {
				fields.add(field);
				continue;
			}
		}

		return fields;
	}

	//-------------------------------------------------------------------
	private static Iterable<Field> getElementFields(Class<?> cls) {

		List<Field> fields = new ArrayList<Field>();
		for (Field field : getAllFields(cls)) {
			if (field.isAnnotationPresent(Element.class)) {
				fields.add(field);
				continue;
			}
			if (field.isAnnotationPresent(ElementList.class)) {
				fields.add(field);
				continue;
			}
			if (field.isAnnotationPresent(ElementListUnion.class)) {
				fields.add(field);
				continue;
			}
		}

		return fields;
	}

	//-------------------------------------------------------------------
	private static Field getCDataField(Class<?> cls) {
		for (Field field : getAllFields(cls)) {
			if (field.isAnnotationPresent(CData.class)) {
				return field;
			}
		}

		return null;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createAttributes(Object data, XmlNode node) throws SerializationException {
		logger.log(Level.DEBUG, "writeAttributes "+data.getClass());
		Class<?> cls = data.getClass();

		for (Field field : getAttributeFields(cls)) {
			logger.log(Level.DEBUG, "  Attribute "+field);

			// Ignore transient fields
			if (Modifier.isTransient(field.getModifiers())) {
				continue;
			}


			// The annotation must be present to be recognized
			// as an attribute fie,d
			Attribute annoAttrib = field.getAnnotation(Attribute.class);
			// Determine attribute name
			String name = field.getName();
			if (!annoAttrib.name().isEmpty())
				name = annoAttrib.name();

			Object val = null;
			try {
				field.setAccessible(true);
				val = field.get(data);
			} catch (Exception e) {
				throw new SerializationException("Cannot access field "+field,e);
			}

			/*
			 * Don't do anything on empty data,
			 * unless it is required
			 */
			if (val==null || val==Boolean.FALSE || ((val instanceof Boolean) && ((boolean)val)==false)) {
				// No data in field
				if (annoAttrib.required()) {
					logger.log(Level.WARNING, "Required field "+field+" (of "+data+") not set");
//					throw new SerializationException("Required field "+field+" (of "+data+") not set");
				}
				continue;
			}
			if (field.getType()==int.class && ((int)val)==0) {
				// int field set to 0 - don't show it
				if (!annoAttrib.required())
					continue;
			}

			// Does a converter exist?
			logger.log(Level.DEBUG, "Converter of field "+field+" = "+field.isAnnotationPresent(AttribConvert.class));
			if (field.isAnnotationPresent(AttribConvert.class)) {
				AttribConvert convert =  field.getAnnotation(AttribConvert.class);
				logger.log(Level.DEBUG, "  use converter "+convert);
				try {
					StringValueConverter conv = Util.createAttribConverter(convert.value());
					node.setAttribute(name, conv.write(val));
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed calling converter "+convert+" on value "+val+" from field "+field+" on "+data);
					throw new SerializationException(e);
				}
				continue;
			}



			try {
				val = field.get(data);
				if (val instanceof Date) {
					logger.log(Level.DEBUG, "Convert date "+val+" to date formatted string");
					node.setAttribute(name, FORMAT.format((Date)val));
				} else if (field.getType().isEnum()) {
					EnumValue enumVal = field.getType().getField( ((Enum)val).name() ).getAnnotation(EnumValue.class);
					if (enumVal!=null)
						node.setAttribute(name, enumVal.value());
					else
						node.setAttribute(name, ((Enum)val).name());
				} else if (field.getType()==byte[].class) {
					node.setAttribute(name, Base64.getEncoder().encodeToString((byte[])val));
				} else if (field.getType()==String.class) {
					node.setAttribute(name, ClassHelperTools.encodeCDATA(String.valueOf(val)));
				} else if (val!=null)
					node.setAttribute(name, String.valueOf(val));
			} catch (Exception e) {
				throw new SerializationException("Cannot access "+field, e);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Create a new node using only the information stored in the type
	 * itself - e.g. @Root and other type level annotations
	 */
	private <T> XmlNode createNodeByType(T value, StringValueConverter<T> convert) throws SerializationException {
		Root rootAnno = value.getClass().getAnnotation(Root.class);
		if (rootAnno==null)
			throw new SerializationException("Missing @Root in "+value.getClass());

		XmlNode node = new XmlNode(rootAnno.name(), null);

		createAttributes(value, node);
		createElements(value, node);

		return node;
	}

	//-------------------------------------------------------------------
	private void createNormalField(Object val, Field field, XmlNode parent) throws SerializationException {
		String name = field.getName();

		Element elemAnno = field.getAnnotation(Element.class);
		if (elemAnno!=null && !elemAnno.name().isEmpty())
			name = field.getAnnotation(Element.class).name();

		if ((val instanceof Collection) && ((Collection<?>)val).isEmpty() && elemAnno!=null && !elemAnno.required()) {
			logger.log(Level.DEBUG, "  don't write empty un-required list");
			return;
		}

		XmlNode child = new XmlNode(name, field);
		parent.getChildren().add(child);
		write(val, child);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createListField(Iterable<?> val, Field field, XmlNode parent) throws SerializationException {
		logger.log(Level.DEBUG, "createListField "+field);
		if (val==null)
			return;
//			throw new NullPointerException("Field to serialize is NULL: "+field);
		ElementList eList = field.getAnnotation(ElementList.class);
		ElementListUnion eListUnion = field.getAnnotation(ElementListUnion.class);
		if (eListUnion==null)
			eListUnion = field.getType().getAnnotation(ElementListUnion.class);
		if (eList==null && eListUnion==null)
			throw new SerializationException("Missing @ElementList or @ElementListUnion at "+field);

		XmlNode listContainer = parent;

		if (eList!=null && eListUnion==null) {
			/*
			 * Converter
			 */
			XMLElementConverter converter = null;
			Class<? extends XMLElementConverter<?>> convClass = eList.convert();
			if (convClass!=ElementConvert.NOELEMENTCONVERTER.class) {
				try {
					converter = Util.createElementConverter(convClass);
				} catch (Exception e) {
					throw new SerializationException("Cannot instantiate converter "+convClass,e);
				}
			}


			// @ElementList
			if (!eList.inline()) {
				logger.log(Level.DEBUG, "  non-inline");
				if (!val.iterator().hasNext() && !eList.required()) {
					logger.log(Level.DEBUG, "Skip empty list: "+field);
					return;
				}
				XmlNode elemNode = new XmlNode(field.getName(), field);
				parent.getChildren().add(elemNode);
				listContainer = elemNode;
			}
			String name = eList.entry();
			if (name.isEmpty()) {
				Class<?> entryType = eList.type();
				if (!entryType.isAnnotationPresent(Root.class))
					throw new SerializationException("Field "+field+" requires either @EntryList(entry) attribute or "+entryType+" requires a @Root annotation");
				Root root = entryType.getAnnotation(Root.class);
				name = root.name();
			}

			for (Object elem : val) {
				logger.log(Level.DEBUG, "  list elem "+elem);
				if (elem==null)
					continue;
				/*
				 * If there was a converter in the @ElementList, it is used instead of implicit conversion
				 */
				if (converter!=null) {
					XmlNode elemNode = new XmlNode(name, field);
					listContainer.getChildren().add(elemNode);
					logger.log(Level.DEBUG, "  Call converter on "+elemNode);
					try {
						converter.write(elemNode, elem);
					} catch (Exception e) {
						logger.log(Level.ERROR, "Failed calling converter "+converter+" with value "+elem,e);
						throw new SerializationException("Failed calling converter "+converter+" with value "+elem,e);
					}
					continue;
				}

				if (elem.getClass().isEnum()) {
					logger.log(Level.DEBUG, "  Write "+elem+" as enum");

					TextNode elemNode = new TextNode(name, field, ((Enum<?>)elem).name() );
					listContainer.getChildren().add(elemNode);
				} else if (elem.getClass()==String.class) {
					listContainer.getChildren().add(new TextNode(name, field, (String)elem));
				} else if (elem.getClass()==int.class || elem.getClass()==Integer.class) {
					listContainer.getChildren().add(new TextNode(name, field, String.valueOf(elem)));
				} else {
					XmlNode elemNode = new XmlNode(name, field);
					listContainer.getChildren().add(elemNode);
					logger.log(Level.DEBUG, "  ListElement "+elem);
					write(elem, elemNode);
				}
			}
		} else {
			// @ElementListUnion
			for (Object elem : val) {
				String name = null;
				logger.log(Level.DEBUG, "Find matching @ElementList for "+elem.getClass()+" in "+Arrays.toString(eListUnion.value()) );
				boolean matchNotFound = true;
				for (ElementList tmpEList : eListUnion.value()) {
					if (elem.getClass().isAssignableFrom(tmpEList.type())) {
						logger.log(Level.DEBUG, "  use "+tmpEList+" for "+elem.getClass());
						name = tmpEList.entry();
						eList = tmpEList;
						if (name.isEmpty()) {
							Class<?> entryType = tmpEList.type();
							if (!entryType.isAnnotationPresent(Root.class))
								throw new SerializationException("Field "+field+" requires either @EntryList(entry) attribute or "+entryType+" requires a @Root annotation");
							Root root = entryType.getAnnotation(Root.class);
							name = root.name();
						}
						matchNotFound = false;
						break;
					}
				}
				if (matchNotFound) { 
					logger.log(Level.ERROR, "Check "+eList.supplier()+" / "+field);
					eList = field.getAnnotation(ElementList.class);
				}
				// Check supplier
				if (matchNotFound && eList!=null && eList.supplier()!=null && eList.supplier()!=PossibleNodesSupplier.NO_PROVIDER.class) {
					logger.log(Level.ERROR, "Found a supplier: "+eList.supplier());
					try {
						PossibleNodesSupplier supplier = eList.supplier().getConstructor().newInstance();
						Map<String,Class<?>> options = supplier.get();
						for (Entry<String,Class<?>> entry : options.entrySet()) {
							logger.log(Level.INFO, "Compare {0} and {1} ",  elem.getClass(), entry.getValue());
							if (elem.getClass().isAssignableFrom(entry.getValue())) {
								logger.log(Level.DEBUG, "  use "+entry.getKey()+" for "+elem.getClass());
								name = entry.getKey();
								if (name.isEmpty()) {
									Class<?> entryType = entry.getValue();
									if (!entryType.isAnnotationPresent(Root.class))
										throw new SerializationException("Field "+field+" requires either @EntryList(entry) attribute or "+entryType+" requires a @Root annotation");
									Root root = entryType.getAnnotation(Root.class);
									name = root.name();
								}
								matchNotFound = false;
								break;
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				if (matchNotFound) {
					logger.log(Level.ERROR, "No match for "+elem.getClass()+" in "+eListUnion);
					throw new SerializationException("No match for "+elem.getClass()+" in "+eListUnion);
				}

				XmlNode elemNode = new XmlNode(name, field);
				listContainer.getChildren().add(elemNode);
				logger.log(Level.DEBUG, "  ListElement "+elem+" with name "+name);
				write(elem, elemNode);
			}
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createMapField(Map<?,?> val, Field field, XmlNode parent) throws SerializationException {
		logger.log(Level.DEBUG, "createMapField "+field);

		/*
		 * List name
		 */
		String name = field.getName();
		Element elemAnno = field.getAnnotation(Element.class);
		if (!elemAnno.name().isEmpty())
			name = elemAnno.name();

		/*
		 * Converter
		 */
		StringValueConverter keyConverter = null;
		XMLElementConverter valConverter = null;
		if (field.isAnnotationPresent(MapConvert.class)) {
			MapConvert mapAnno = field.getAnnotation(MapConvert.class);
			Class<? extends StringValueConverter<?>> keyConvClass = mapAnno.keyConvert();
			Class<? extends XMLElementConverter<?>> valConvClass = mapAnno.valConvert();
			logger.log(Level.DEBUG, "  keyConv = "+keyConvClass);
			logger.log(Level.DEBUG, "  valConv = "+valConvClass);
			if (keyConvClass!=AttribConvert.NOVALUECONVERTER.class) {
				try {
					keyConverter = Util.createAttribConverter(keyConvClass);
				} catch (Exception e) {
					throw new SerializationException("Cannot instantiate converter "+keyConvClass,e);
				}
			}
			if (valConvClass!=ElementConvert.NOELEMENTCONVERTER.class) {
				try {
					valConverter = Util.createElementConverter(valConvClass);
				} catch (Exception e) {
					throw new SerializationException("Cannot instantiate converter "+valConvClass,e);
				}
			}
		}

		/*
		 * Prepare list for elements
		 */
		XmlNode elemNode = new XmlNode(name, field);
		parent.getChildren().add(elemNode);
		XmlNode listContainer = elemNode;

		for (Entry<?, ?> entry : val.entrySet()) {
			logger.log(Level.DEBUG, "  map elem "+entry);

			/*
			 * Entry element
			 */
			XmlNode entryNode = new XmlNode("element", field);
			listContainer.getChildren().add(entryNode);

			/*
			 * Key element
			 */
			Object keyVal = entry.getKey();
			try {
				if (keyConverter!=null)
					keyVal = keyConverter.write(keyVal);
			} catch (Exception e) {
				throw new SerializationException("Failed calling converter",e);
			}
			TextNode keyNode = new TextNode("key", field, String.valueOf(keyVal));
			entryNode.getChildren().add(keyNode);

			/*
			 * Value element
			 */
			XmlNode valNode = new XmlNode("value", field);
			valNode.setAttribute("type", entry.getValue().getClass().getName());
			entryNode.getChildren().add(valNode);
			if (valConverter!=null) {
				try {
					valConverter.write(valNode, entry.getValue());
				} catch (Exception e) {
					throw new SerializationException("Failed calling converter "+valConverter,e);
				}
			} else if (entry.getValue().getClass()==String.class){
				logger.log(Level.WARNING, "Was mach ich hier in MAP?");
				entryNode.getChildren().remove(valNode);
				TextNode child = new TextNode("value", field, entry.getValue().toString());
				child.setAttribute("type", entry.getValue().getClass().getName());
				//valNode.getChildren().add(child);
				entryNode.getChildren().add(child);
				write(entry.getValue(), child);
			} else if (entry.getValue().getClass()==Integer.class){
				logger.log(Level.WARNING, "Was mach ich hier in MAP?");
				entryNode.getChildren().remove(valNode);
				TextNode child = new TextNode("value", field, String.valueOf(entry.getValue()));
				child.setAttribute("type", entry.getValue().getClass().getName());
				//valNode.getChildren().add(child);
				entryNode.getChildren().add(child);
				write(entry.getValue(), child);
			} else {
				logger.log(Level.DEBUG, "  serialize element "+entry.getValue());
				Root rootAnno = entry.getValue().getClass().getAnnotation(Root.class);
				if (rootAnno==null)
					throw new SerializationException("When "+entry.getValue().getClass()+" is used as map value, it needs a @Root annotation (field was "+field+")\n(value was "+entry.getValue()+")");
				XmlNode child = new XmlNode(rootAnno.name(), field);
				valNode.getChildren().add(child);
				write(entry.getValue(), child);
			}


			/*
			 * If there was a converter in the @ElementList, it is used instead of implit conversion
			 */
//			if (valConverter!=null) {
//				XmlNode elemNode = new XmlNode(name, field);
//				listContainer.getChildren().add(elemNode);
//				logger.log(Level.DEBUG, "  Call converter on "+elemNode);
//				try {
//					valConverter.write(elemNode, elem);
//				} catch (Exception e) {
//					logger.log(Level.ERROR, "Failed calling converter "+valConverter+" with value "+elem,e);
//					throw new SerializationException("Failed calling converter "+valConverter+" with value "+elem,e);
//				}
//				continue;
//			}
//
//			if (elem.getClass().isEnum()) {
//				logger.log(Level.DEBUG, "  Write "+elem+" as enum");
//
//				TextNode elemNode = new TextNode(name, field, ((Enum<?>)elem).name() );
//				listContainer.getChildren().add(elemNode);
//			} else if (elem.getClass()==String.class) {
//				listContainer.getChildren().add(new TextNode(name, field, (String)elem));
//			} else if (elem.getClass()==int.class || elem.getClass()==Integer.class) {
//				listContainer.getChildren().add(new TextNode(name, field, String.valueOf(elem)));
//			} else {
//				XmlNode elemNode = new XmlNode(name, field);
//				listContainer.getChildren().add(elemNode);
//				logger.log(Level.DEBUG, "  ListElement "+elem);
//				write(elem, elemNode);
//			}

		}
	}

	//-------------------------------------------------------------------
	private void createTextField(String val, Field field, XmlNode parent) throws SerializationException {
		if (val==null) {
			if (!field.getAnnotation(Element.class).required())
				return;
		}

		String name = field.getName();
		if (field.isAnnotationPresent(Element.class) && !"".equals(field.getAnnotation(Element.class).name()))
			name = field.getAnnotation(Element.class).name();
		TextNode child = new TextNode(name, field, val);
		parent.getChildren().add(child);
	}

	//-------------------------------------------------------------------
	private void createByteArray(byte[] val, Field field, XmlNode parent) throws SerializationException {
		if (val==null) {
			if (!field.getAnnotation(Element.class).required())
				return;
		}

		String name = field.getName();
		byte[] encoded = Base64.getEncoder().encode(val);
		TextNode child = new TextNode(name, field, new String(encoded));
		parent.getChildren().add(child);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void createElements(Object data, XmlNode node) throws SerializationException {
		logger.log(Level.DEBUG, "writeElements "+data.getClass());
		Class<?> cls = data.getClass();
		for (Field field : getElementFields(cls)) {
			logger.log(Level.DEBUG, "  Element "+field);
			Object val = null;
			Element elem = field.getAnnotation(Element.class);

			field.setAccessible(true);
			try {
				val = field.get(data);
			} catch (NullPointerException e) {
				throw e;
			} catch (Exception e) {
				throw new SerializationException("Cannot access "+field, e);
			}

			if (field.isAnnotationPresent(ElementConvert.class)) {
				logger.log(Level.DEBUG, "    has XMLElementConverter");
				Class<? extends XMLElementConverter<?>> conv = field.getAnnotation(ElementConvert.class).value();
				try {
					XMLElementConverter convert = Util.createElementConverter(conv);
					convert.write(node, val);
				} catch (Exception e) {
					throw new SerializationException("Failed calling converter", e);
				}
			} else if (Map.class.isAssignableFrom(field.getType())) {
				logger.log(Level.DEBUG, "  create map field");
				createMapField( (Map<?,?>)val, field, node);
			} else if (Iterable.class.isAssignableFrom(field.getType()) && elem==null) {
				logger.log(Level.DEBUG, "  create list field");
				createListField((Iterable<?>) val, field, node);
			} else if (field.getType()==String.class) {
				if (val!=null || elem.required())
					createTextField((String)val, field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==int.class || field.getType()==Integer.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==double.class || field.getType()==Double.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==float.class || field.getType()==Float.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==long.class || field.getType()==Long.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==byte.class || field.getType()==Byte.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==boolean.class || field.getType()==Boolean.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==UUID.class) {
				if (val!=null || elem.required())
					createTextField(String.valueOf(val), field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType().isEnum()) {
				if (val==null && elem.required())
					throw new SerializationException("Missing value for "+field);
				if (val!=null || elem.required()) {
					String toWrite = ((Enum<?>)val).name();
					// If present, overwrite with @EnumValue's annotation name
					Class<Enum<?>> enumCls = (Class<Enum<?>>) field.getType();
					try {
						Field enumField = enumCls.getDeclaredField(toWrite);
						if (enumField.isAnnotationPresent(EnumValue.class))
							toWrite = enumField.getAnnotation(EnumValue.class).value();
					} catch (Exception e) {
						logger.log(Level.ERROR, "Failed finding a field named '"+toWrite+"' in enum "+enumCls);
					}
					createTextField(toWrite, field, node);
				} else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else if (field.getType()==byte[].class) {
				if (val!=null || elem.required())
					createByteArray((byte[])val, field, node);
				else
					logger.log(Level.DEBUG, "  ignored empty text field");
			} else {
				if (val==null) {
					if (elem.required())
						throw new SerializationException("Empty required field "+field);
					continue;
				} else
				createNormalField(val, field, node);
			}
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private <T> void write(Object data, XmlNode node) throws SerializationException {
		logger.log(Level.DEBUG, "write "+data.getClass());

		if (data instanceof List<?>) {
			// Attributes
			createAttributes(data, node);

			// Children
			createElements(data, node);
			Iterable<?> iter = (Iterable<?>)data;
			logger.log(Level.DEBUG, "Write iterable "+data+" from field "+node.getField());
			// @ElementList
			ElementList eList = null;
			if (node.getField()!=null)
				eList = node.getField().getAnnotation(ElementList.class);
			if (eList==null)
				eList = data.getClass().getAnnotation(ElementList.class);
			if (eList==null && node.getField()!=null)
				eList = node.getField().getType().getAnnotation(ElementList.class);
			// @ElementListUnion
			ElementListUnion eListUnion = null;
			if (node.getField()!=null)
				eListUnion = node.getField().getAnnotation(ElementListUnion.class);
			if (eListUnion==null)
				eListUnion = data.getClass().getAnnotation(ElementListUnion.class);
			logger.log(Level.DEBUG, "eListUnion = "+eListUnion);

			XmlNode listContainer = node;
			if (eList==null) {
				// Default behaviour
				logger.log(Level.DEBUG, "  No ElementList found  neither in "+node.getField()+" or "+data.getClass());
//			} else if (!eList.inline()) {
//				logger.log(Level.WARNING, "TODO: non-inline list "+node.getField()+" // "+node);
////				System.exit(0);
			}

			XMLElementConverter<T> convert = null;
			/*
			 * Either an @ElementList has been found, or at least an @ElementListUnion must be present
			 * to detect @ElementLists within element loop
			 */
			logger.log(Level.DEBUG, "  eList     = "+eList);
			logger.log(Level.DEBUG, "  eListUnion= "+eListUnion);
			if (eList!=null) {
				try {
					convert = (eList.convert()!=ElementConvert.NOELEMENTCONVERTER.class)?((XMLElementConverter<T>)eList.convert().getDeclaredConstructor().newInstance()):null;
				} catch (Exception e) {
					throw new SerializationException("Error instantiating converter",e);
				}
			} else if (eListUnion==null) {
				throw new SerializationException("Expect either @ElementList or @ElementListUnion in "+data.getClass()+" or "+node.getField());
			}

			outer:
			for (Object elem : iter) {
				if (elem==null) {
					logger.log(Level.WARNING, "List "+node.getField()+" contains a NullPointer");
					continue;
				}
//				logger.log(Level.ERROR, "check "+elem+"/"+elem.getClass());
				
				// Check with supplier override first
				if (eList!=null && eList.supplier()!=PossibleNodesSupplier.NO_PROVIDER.class) {
					logger.log(Level.TRACE, "Ask supplier");
					try {
						Map<String, Class<?>> map = eList.supplier().getConstructor().newInstance().get();
//						logger.log(Level.ERROR, "Supplier returns "+map);
						for (Entry<String,Class<?>> entry : map.entrySet()) {
							if (entry.getValue().isAssignableFrom(elem.getClass())) {
//								logger.log(Level.ERROR, "Pick "+entry.getKey()+"="+entry.getValue());
								XmlNode elemNode = new XmlNode(entry.getKey(), node.getField());
								listContainer.getChildren().add(elemNode);
//								logger.log(Level.ERROR, "recurse");
								write(elem, elemNode);
//								logger.log(Level.ERROR, "recurse done");
								continue outer;
							}
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
				ElementList innerEList = eList;
				// Check for @ElementListUnion
				if (eListUnion!=null) {
					innerEList = null;
					logger.log(Level.DEBUG, "Find matching innerEList");
					for (ElementList tmp : eListUnion.value()) {
						if (tmp.type()==elem.getClass()) {
							innerEList = tmp;
							break;
						}
					}
					// Ask supplier
					
					if (logger.isLoggable(Level.DEBUG))
						logger.log(Level.DEBUG, "Use @ElementList "+innerEList+" for "+elem.getClass());
					if (innerEList==null) {
						logger.log(Level.ERROR, "write "+data.getClass());
						for (ElementList tmp : eListUnion.value()) {
							if (!tmp.type().isAssignableFrom(elem.getClass())) {
								logger.log(Level.ERROR, tmp.type()+" is not assignable from "+elem.getClass());
							}
						}
						logger.log(Level.ERROR, "  elem      = "+elem.getClass());
						logger.log(Level.ERROR, "  eList     = "+eList);
						logger.log(Level.ERROR, "  innerEList= "+innerEList);
						logger.log(Level.ERROR, "  eListUnion= "+eListUnion);
						throw new SerializationException("  Missing @ElementList for "+elem.getClass()+" in @ElementListUnion "+eListUnion);
					}
				}


				if (innerEList!=null) {
					String name = innerEList.entry();
					if (name.isEmpty()) {
						Root root = elem.getClass().getAnnotation(Root.class);
						if (root==null)
							throw new SerializationException("No @ElementList/entry in "+node.getField()+" nor @Root in "+data.getClass());
						name = root.name();
					}
					logger.log(Level.DEBUG, "  write entry "+name);

					/*
					 * If a converter is present for list elements - use it
					 */
					if (convert!=null) {
						logger.log(Level.DEBUG, "  use converter in lists");
						try {
							XmlNode elemNode = new XmlNode(name, node.getField());
							convert.write(elemNode, (T)elem);
							listContainer.getChildren().add(elemNode);
						} catch (Exception e) {
							throw new SerializationException("Error calling converter",e);
						}
					} else {
						XmlNode elemNode = new XmlNode(name, node.getField());
						listContainer.getChildren().add(elemNode);
						write(elem, elemNode);
					}
				} else {
					logger.log(Level.WARNING, "  TODO: write list entry without @ElementList");
					logger.log(Level.ERROR, "  elem      = "+elem.getClass());
					logger.log(Level.ERROR, "  eList     = "+eList);
					logger.log(Level.ERROR, "  innerEList= "+innerEList);
					logger.log(Level.ERROR, "  eListUnion= "+eListUnion);
					throw new SerializationException("  TODO: write list entry without @ElementList");
//					XmlNode elemNode = new XmlNode(eList.entry(), node.getField());
//					listContainer.getChildren().add(elemNode);
//					write(elem, elemNode);
				}
			}
		} else if (data instanceof Map<?,?>) {
			Map<?,?> map = (Map<?,?>)data;
			MapConvert anno = node.getField().getAnnotation(MapConvert.class);
			logger.log(Level.DEBUG, "  @MapConvert = "+anno);
			for (Entry<?,?> elem : map.entrySet()) {
				logger.log(Level.DEBUG, "  TODO: "+elem);
				XmlNode elemNode = new XmlNode("element", node.getField());
				node.getChildren().add(elemNode);

				/*
				 * If no converter is given, use default behavior
				 */
				if (anno==null) {
					// Without @MapConvert
					TextNode keyNode = new TextNode("key", node.getField(), String.valueOf(elem.getKey()));
					elemNode.getChildren().add(keyNode);
					write(elem.getValue(), elemNode);
					Class<?> valType = elem.getValue().getClass();
					if (valType==Integer.class) {
						TextNode valNode = new TextNode("value", node.getField(), String.valueOf(elem.getValue()));
//						valNode.setAttribute("type", "Integer");
						elemNode.getChildren().add(valNode);
					} else {
						XmlNode valNode = new XmlNode("value", node.getField());
						valNode.setAttribute("type", elem.getValue().getClass().getName());
						elemNode.getChildren().add(valNode);
						valNode.getChildren().add(createNodeByType(elem.getValue(), null));
					}
				} else {
					// With @MapConvert
					try {
//						StringValueConverter<Object> keyConv = (StringValueConverter<Object>) anno.keyConvert().newInstance();
//						StringValueConverter<Object> valConv = (StringValueConverter<Object>) anno.valConvert().newInstance();

						XmlNode keyNode = new XmlNode("key", node.getField());
//						keyConv.write(keyNode, (Object)elem.getKey());
						XmlNode valNode = new XmlNode("value", node.getField());
//						valConv.write(valNode, (Object)elem.getValue());
						elemNode.getChildren().add(keyNode);
						elemNode.getChildren().add(valNode);
						logger.log(Level.ERROR, "Don't know how to convert here: "+node.getField());
						System.exit(0);
					} catch (Exception e) {
						throw new SerializationException("Failed using map converter "+anno,e);
					}
				}
			}
		} else {
			// Attributes
			createAttributes(data, node);

			// Children
			createElements(data, node);

			Field cDataField = getCDataField(data.getClass());
			if (cDataField!=null) {
				try {
					cDataField.setAccessible(true);
					String val = (String)cDataField.get(data);
					PureCData text = new PureCData(cDataField, ClassHelperTools.encodeCDATA(String.valueOf(val)));
					node.getChildren().add(text);
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed setting CDATA content in "+cDataField);
				}
			}
		}
	}

	//-------------------------------------------------------------------
	public Document writeToDocument(Object data) throws SerializationException {
		logger.log(Level.DEBUG, "writeToDoc "+data.getClass());
		Class<?> cls = data.getClass();

		Document doc = new Document();

		// Find the @Root in the class
		Root aRoot = cls.getAnnotation(Root.class);
		if (aRoot==null)
			throw new SerializationException("Missing @Root in "+cls);
		XmlNode root = new XmlNode(aRoot.name(), null);
		doc.setRoot(root);

		write(data, root);

		return doc;
	}

	//-------------------------------------------------------------------
	public void write(XmlNode node, int indent, PrintWriter out) throws IOException {
		if (node instanceof PureCData) {
			String raw = ((PureCData)node).getValue();
			if (raw==null)
				raw="";
			raw = ClassHelperTools.encodeCDATA(raw);
			out.print(raw);
			return;
		}

		char[] indentArray = new char[indent];
		Arrays.fill(indentArray, ' ');
		String indention = new String(indentArray);

		// Start
		out.print(indention);
		out.print("<");
		out.print(node.getName());
		// Attributes
		writeAttributes(node, out);

		// End or children
		if (node.isLeaf() && !(node instanceof TextNode)) {
			out.println("/>");
			return;
		}
		out.print(">");
		// Children
		if (node instanceof TextNode) {
			String raw = ((TextNode)node).getValue();
			if (raw==null)
				raw="";
			raw = ClassHelperTools.encodeCDATA(raw);
//			raw = raw.replace("&", "&amp;");
//			raw = raw.replace("<", "&lt;");
//			raw = raw.replace(">", "&gt;");
//			raw = raw.replace("\"", "&#x0022;");
			out.print(raw);
		} else {
			out.println();
			for (XmlNode child : node.getChildren())
				write(child, indent+3, out);
			out.print(indention);
		}

		// End
		out.print("</");
		out.print(node.getName());
		out.println(">");

	}

	//-------------------------------------------------------------------
	private void writeAttributes(XmlNode node, PrintWriter out) throws IOException {
		List<String> keys = new ArrayList<String>(node.getAttribute().keySet());
		Collections.sort(keys);
		for (String key : keys) {
			String val = node.getAttribute().get(key);
			out.print(" "+key+"=\""+val+"\"");
		}

	}

}
