/**
 * 
 */
package org.prelle.simplepersist;

/**
 * @author Stefan
 *
 */
public interface AfterLoadHookInterface {

	//--------------------------------------------------------------------
	/**
	 * Called after a new object instance has been instantiated
	 * @param data
	 */
	public void afterLoad(Object data);
	
}
