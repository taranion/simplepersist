/**
 * 
 */
package org.prelle.simplepersist;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author prelle
 *
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.FIELD,ElementType.PARAMETER,ElementType.TYPE})
@Inherited
@Documented
public @interface ReferenceList {

	String entry() default "";
	boolean inline() default false;
	Class<?> type();
	boolean required() default false;
	Class<? extends XMLElementConverter<?>> convert() default ElementConvert.NOELEMENTCONVERTER.class;

}
