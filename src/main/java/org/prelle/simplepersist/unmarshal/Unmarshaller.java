package org.prelle.simplepersist.unmarshal;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.DeSerializer;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Util;
import org.prelle.simplepersist.unmarshal.CurrentElement.Type;

;

/**
 * @author prelle
 *
 */
public class Unmarshaller implements DeSerializer {

	private final static Logger logger = System.getLogger("xml");

	//-------------------------------------------------------------------
	private <T> void parseAttributes(Class<T> type, XMLEventReader evRd, StartElement elem, T obj) throws SerializationException {
		// Build a list of possible fields
		Map<String, Field> fieldsByAttribute = new HashMap<String, Field>();
		for (Field field : Util.getAttributeFields(type)) {
			Attribute attr = field.getAnnotation(Attribute.class);
			if (attr.name()!=null && attr.name().length()>0)
				fieldsByAttribute.put(attr.name(), field);
			else
				fieldsByAttribute.put(field.getName(), field);
		}


		Iterator<javax.xml.stream.events.Attribute> iter = elem.getAttributes();
		while( iter.hasNext() ) {
			javax.xml.stream.events.Attribute a = iter.next();
			logger.log(Level.DEBUG, "Attr "+a);
			String name = a.getName().getLocalPart();
			String sVal = a.getValue();

			Field field = fieldsByAttribute.get(name);
			field.setAccessible(true);
			logger.log(Level.DEBUG, "  write to "+field);

			try {
				if (field.getType()==String.class) {
					field.set(obj, sVal);
					fieldsByAttribute.remove(name);
				} else if (field.getType()==int.class || field.getType()==Integer.class) {
					field.set(obj, Integer.valueOf(sVal));
					fieldsByAttribute.remove(name);
				} else if (field.getType().isEnum()) {
					Method valueOf = field.getType().getMethod("valueOf", String.class);
					field.set(obj, valueOf.invoke(null, sVal));
					fieldsByAttribute.remove(name);
				} else
					logger.log(Level.WARNING, "Cannot set "+field.getType());
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		logger.log(Level.WARNING, "TODO: Unfilled attributes "+fieldsByAttribute);
	}

	//-------------------------------------------------------------------
	private void fillFieldWithAttribute(Field field, Class<?> cls, String value, Object instance) throws SerializationException {
		logger.log(Level.DEBUG, "    Fill "+field+" with '"+value+"'");
		
		Object toSet = value;
		if (cls==Integer.class || cls==int.class) {
			toSet = Integer.parseInt(value);
		} else {
			if (cls!=String.class) {
				logger.log(Level.WARNING, "    Don't know how to convert from String to "+cls+"  (for "+field+")");
				return;
			}
		}
		
		field.setAccessible(true);
		
		/*
		 * Set field
		 */
		try {
			field.set(instance, toSet);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SerializationException("Cannot set field "+field+" of "+instance,e);
		}
	}

	//-------------------------------------------------------------------
	private void parseAttributes(StartElement ev,CurrentElement current) throws SerializationException {
		if (logger.isLoggable(Level.DEBUG))
			logger.log(Level.DEBUG, " parseAttributes of "+ev);
        
        Iterator<javax.xml.stream.events.Attribute> iter = ev.getAttributes();
        while( iter.hasNext() ) {
        	javax.xml.stream.events.Attribute a = iter.next();
           	String name = a.getName().getLocalPart();
           	String sVal = a.getValue();
           	
           	logger.log(Level.DEBUG, "  * "+name+" = "+sVal);
           	ByNameInfo info = current.getInfoForAttribute(name);
           	if (info==null) {
           		logger.log(Level.WARNING, "    Found unexpected attribute '"+name+"' in "+ev);
           		continue;
           	}
           	fillFieldWithAttribute(info.field, info.cls, sVal, info.fieldInstance);
        }
	}

	//-------------------------------------------------------------------
	private <T> void parseListField(XMLEventReader evRd, StartElement elem, Field field, List<Object> list) throws SerializationException {
		Class<?> type = field.getAnnotation(ElementList.class).type();
		logger.log(Level.DEBUG, "Parse "+elem+" as "+type+" and add it to list "+list+" in field "+field);

		//		logger.log(Level.DEBUG, "Item2 "+list.getClass().getGenericSuperclass());
		//		logger.log(Level.DEBUG, "Item2 "+((ParameterizedType)list.getClass().getGenericSuperclass()).getActualTypeArguments()[0] );
		//		Class<T> persistentClass = (Class<T>)
		//				   ((ParameterizedType)list.getClass().getGenericSuperclass())
		//				      .getActualTypeArguments()[0];

		Object listElem = parse(type, evRd, elem);
		logger.log(Level.DEBUG, "Add item "+listElem+" to "+field.getName());
		list.add(listElem);

		try {
			while (evRd.hasNext()) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					continue;
				case XMLEvent.END_ELEMENT:
					logger.log(Level.DEBUG, "Ending "+ev);
					return;
				default:
					logger.log(Level.ERROR, "Not handled_ "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (Exception e) {
			throw new SerializationException("Failed parsing", e);
		}
	}

	//-------------------------------------------------------------------
	private void parseText(XMLEventReader evRd, StartElement elem, Field field, Object obj) throws SerializationException {
		StringBuffer text = new StringBuffer();
		try {
			while (evRd.hasNext()) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					text.append(ev.asCharacters().getData());
					continue;
				case XMLEvent.END_ELEMENT:
					field.setAccessible(true);
					field.set(obj, text.toString());
					logger.log(Level.DEBUG, "Set "+field.getName()+" = "+text+"    EV="+ev );
					return;
				default:
					logger.log(Level.ERROR, "Not handled_ "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (Exception e) {
			throw new SerializationException("Failed parsing", e);
		}
	}

	//-------------------------------------------------------------------
	private <T> void parseElements(Class<T> type, XMLEventReader evRd, StartElement elem, T obj) throws SerializationException {
		// Keep a space for every list by field
		Map<Field, List<Object>> listByField = new HashMap<>();
		// Build a list of possible fields
		Map<String, Field> fieldsByElements = new HashMap<String, Field>();
		for (Field field : Util.getElementFields(type)) {
			if (field.getAnnotation(Element.class)!=null) {
				Element attr = field.getAnnotation(Element.class);
				logger.log(Level.DEBUG, "Field "+field);
				if (attr.name()!=null && attr.name().length()>0)
					fieldsByElements.put(attr.name(), field);
				else
					fieldsByElements.put(field.getName(), field);
			} else
				if (field.getAnnotation(ElementList.class)!=null) {
					ElementList attr = field.getAnnotation(ElementList.class);
					logger.log(Level.DEBUG, "Field "+field);
					if (attr.entry()!=null && attr.entry().length()>0)
						fieldsByElements.put(attr.entry(), field);
					else
						fieldsByElements.put(field.getName(), field);
					List<Object> list = new ArrayList<>();
					listByField.put(field, list);
				}
		}

		try {
			while( evRd.hasNext() ) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					continue;
				case XMLEvent.START_ELEMENT:
					logger.log(Level.DEBUG, "EV = "+ev+" // "+fieldsByElements);
					Field field = fieldsByElements.get(ev.asStartElement().getName().getLocalPart());
					logger.log(Level.DEBUG, "Start "+field);
					if (field.getAnnotation(ElementList.class)!=null) {
						parseListField(evRd, ev.asStartElement(), field, listByField.get(field));
					} else if  (field.getType()==String.class) {
						parseText(evRd, ev.asStartElement(), field, obj);
					} else {
						logger.log(Level.WARNING, "Cannot process "+field);
						System.exit(0);
					}
					break;
				case XMLEvent.END_ELEMENT:
					logger.log(Level.DEBUG, "END = "+ev);
					return;
				default:
					logger.log(Level.ERROR, "Not processed = "+ev+" / "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}        

		logger.log(Level.WARNING, "TODO: Unfilled attributes "+fieldsByElements);
	}

	//-------------------------------------------------------------------
	private <T> T parse(Class<T> type, XMLEventReader evRd, StartElement elem) throws SerializationException {
		// Create an object instance using the empty constructor
		T obj = null;
		try {
			obj = type.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			throw new SerializationException("Failed instantiating "+type,e);
		}
		logger.log(Level.DEBUG, "Class "+obj);

		parseAttributes(type, evRd, elem, obj);
		parseElements(type, evRd, elem, obj);

		return obj;
	}

//	//-------------------------------------------------------------------
//	private static Field getFieldForName(Class<?> type, String name) {
//		for (Field field : Util.getAttributeFields(type)) {
//			if (field.isAnnotationPresent(Attribute.class) && field.getAnnotation(Attribute.class).name().equals(name))
//				return field;
//			if (field.isAnnotationPresent(ElementList.class) && field.getAnnotation(ElementList.class).entry().equals(name))
//				return field;
//		}
//		for (Field field : Util.getElementFields(type)) {
//			if (field.isAnnotationPresent(Element.class) && field.getAnnotation(Element.class).name().equals(name))
//				return field;
//			if (field.isAnnotationPresent(ElementList.class) && field.getAnnotation(ElementList.class).entry().equals(name)) {
//				return field;
//			}
//			if (field.isAnnotationPresent(ElementList.class) && field.getName().equals(name))
//				return field;
//			if (field.isAnnotationPresent(ElementListUnion.class)) {
//				ElementListUnion eUnion = field.getAnnotation(ElementListUnion.class);
//				for (ElementList eList : eUnion.value()) {
//					if (name.equals(eList.entry()))
//						return field;
//				}
//			}
//		}
//		for (Field field : Util.getAllFields(type)) {
//			if (field.getName().equals(name))
//				return field;
//		}
//
//		return null;
//	}
//
//	//-------------------------------------------------------------------
//	private static <T extends Annotation>  T getAnnotationHelper(Class<T> cls, Field field) throws SerializationException {
//		if (!List.class.isAssignableFrom(field.getType()))
//			throw new SerializationException("Expect "+field+" to be a List");
//
//		T anno = field.getAnnotation(cls);
//		if (anno==null) 
//			anno = field.getType().getAnnotation(cls);
//
//		return anno;
//	}
//
//	//-------------------------------------------------------------------
//	/**
//	 * @return Array key/value
//	 */
//	@SuppressWarnings("unchecked")
//	private Object[] readMapElement(XMLEventReader evRd, Field mapField) throws SerializationException, XMLStreamException, IOException {
//		Object key   = null;
//		Object value = null;
//		final int IDLE = 0;
//		final int IN_KEY = 1;
//		final int WAIT_VALUE = 2;
//		final int IN_VALUE = 3;
//		final int WAIT_END = 4;
//		int state = IDLE;
//
//		StringValueConverter<Object> keyConv = null;
//		XMLElementConverter<Object> valConv = null;
//		if (mapField.isAnnotationPresent(MapConvert.class)) {
//			MapConvert anno = mapField.getAnnotation(MapConvert.class);
//			try {
//				if (anno.keyConvert()!=AttribConvert.NOVALUECONVERTER.class)
//					keyConv = (StringValueConverter<Object>) anno.keyConvert().newInstance();
//			} catch (Exception e) {
//				logger.log(Level.ERROR, "Failed creating "+anno.keyConvert());
//				throw new SerializationException("Failed creating map element converter for "+evRd, e);
//			}
//			try {
//				if (anno.valConvert()!=ElementConvert.NOELEMENTCONVERTER.class)
//					valConv = (XMLElementConverter<Object>) anno.valConvert().newInstance();
//			} catch (Exception e) {
//				logger.log(Level.ERROR, "Failed creating "+anno.valConvert());
//				throw new SerializationException("Failed creating map element converter for "+evRd, e);
//			}
//		}
//		logger.log(Level.DEBUG, "  keyConv="+keyConv+"    valConv="+valConv);
//
//		while( evRd.hasNext() ) {
//			XMLEvent ev = evRd.nextEvent();
//			switch (ev.getEventType()) {
//			case XMLEvent.START_ELEMENT:
//				logger.log(Level.DEBUG, "START "+ev);
//				if ("key".equals(ev.asStartElement().getName().getLocalPart())) {
//					state = IN_KEY;
//					if (keyConv!=null && keyConv.getClass()!=AttribConvert.NOVALUECONVERTER.class) {
//						logger.log(Level.ERROR, "Read value "+ev.asStartElement()+" with converter");
//						System.exit(0);
//						//						try {
//						//							key = keyConv.read(null, ev.asStartElement());
//						//						} catch (Exception e) {
//						//							throw new SerializationException("Error im Converter for map key",e);
//						//						}
//					}
//				} else if ("value".equals(ev.asStartElement().getName().getLocalPart())){
//					state = IN_VALUE;
//					if (valConv!=null && valConv.getClass()!=ElementConvert.NOELEMENTCONVERTER.class) {
//						try {
//							logger.log(Level.ERROR, "Read value "+ev.asStartElement()+" with converter");
//							System.exit(0);
//							//							value = valConv.read(null, ev.asStartElement());
//						} catch (Exception e) {
//							throw new SerializationException("Error im Converter for map key",e);
//						}
//					} else {
//						logger.log(Level.DEBUG, "Read value without converter");
//						javax.xml.stream.events.Attribute type_a = ev.asStartElement().getAttributeByName(new QName("type"));
//						try {
//							Class<?> type = null;
//							if (type_a==null) {
//								logger.log(Level.DEBUG, "  Check value type for "+mapField);
//								type = (Class<?>) ((ParameterizedType)mapField.getGenericType()).getActualTypeArguments()[1];
//								logger.log(Level.DEBUG, "  expect = "+type);
//								XMLEvent tmpEv = evRd.nextEvent();
//								if (!tmpEv.isCharacters())
//									throw new SerializationException("Expected "+type+" value for element with key "+key+" but found "+tmpEv);
//								value = tmpEv.asCharacters().getData();
//								if (type==Integer.class)
//									value = Integer.parseInt((String)value);
//								else if (type==Boolean.class)
//									value = Boolean.parseBoolean((String)value);
//								else if (type!=String.class) {
//									logger.log(Level.ERROR, "Don't know how to convert String to "+type+" for map values");
//								}
//							} else {
//								String type_s = ev.asStartElement().getAttributeByName(new QName("type")).getValue();
//								type = Class.forName(type_s);
//								value = read(type, evRd);
//							}
//							logger.log(Level.DEBUG, "  value = "+value);
//						} catch (ClassNotFoundException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//				} else {
//				}
//				continue;
//			case XMLEvent.END_ELEMENT:
//				logger.log(Level.DEBUG, "STOP  "+ev);
//				String endName = ev.asEndElement().getName().getLocalPart();
//				switch (state) {
//				case IN_KEY:
//					if ("key".equals(endName)) {
//						state = WAIT_VALUE;
//						break;
//					}
//					break;
//				case IN_VALUE:
//					if ("value".equals(endName)) {
//						state = WAIT_END;
//						break;
//					}
//					break;
//				case WAIT_END:
//					if ("element".equals(endName)) {
//						state = IDLE;
//						return new Object[]{key,value};
//					}
//					break;
//				default:
//					logger.log(Level.WARNING, "TODO: "+state);
//				}
//				continue;
//			case XMLEvent.CHARACTERS:
//				switch (state) {
//				case IDLE: 
//				case WAIT_VALUE: 
//					break;
//				case IN_KEY:
//					key = ev.asCharacters().getData();
//					break;
//				default:
//					logger.log(Level.DEBUG, "CHARS: "+ev.asCharacters().getData());
//				}
//				continue;
//			default:
//				throw new SerializationException("Unexpected event: "+ev.getEventType());
//			}
//		}
//
//		return null;
//	}

	//-------------------------------------------------------------------
	private void processEndElement(Stack<CurrentElement> stck, EndElement ev) throws SerializationException {
		logger.log(Level.DEBUG, "STOP  "+ev);
		logger.log(Level.DEBUG, "  current stack is "+stck);
		logger.log(Level.DEBUG, "  current = "+stck.peek().dump());
		String name = ev.getName().getLocalPart();
		
		CurrentElement current = stck.pop();
		if (current.getType()==Type.IGNORE) {
			logger.log(Level.DEBUG, "  Ignore end tag of unexpected tag");
			stck.peek().expectedClose = null;
			return;
		}
		
		/*
		 * Determine what is ending here
		 */
		ByNameInfo expInfoCalc = stck.peek().getInfoForElement(name);
		ByNameInfo expInfo = stck.peek().expectedClose;
		logger.log(Level.DEBUG, "  Info by expectation is "+expInfo);
		logger.log(Level.DEBUG, "  Info for tag '"+name+"' is "+expInfoCalc);
		if (expInfo==null) {
			logger.log(Level.ERROR, "Found an unexpected closing element '"+name+"'");
			throw new SerializationException("Unexpected closing element "+name);
		}
		if (expInfo!=expInfoCalc) {
			logger.log(Level.ERROR, "Found a closing element '"+name+"' that is unexpected in "+current.expectedElements);
			throw new SerializationException("Unexpected closing element "+name);
		}
		logger.log(Level.DEBUG, "  use end tag with "+expInfo.dump());
		
		Object toSet = expInfo.value;
		if (expInfo.cls==String.class)
			toSet = current.collectedText.toString();
		logger.log(Level.DEBUG, "  value to set = "+toSet);
		if (toSet!=null)
			logger.log(Level.DEBUG, "  type  to set = "+toSet.getClass());
		
		/*
		 * Determine field to set with value
		 */
		current = stck.peek();
		Field field = null;
		field = expInfo.field;
		logger.log(Level.DEBUG, "  Field to set: "+field);
		
		/*
		 * Determine instance to set field in
		 */
		Object instance = expInfo.fieldInstance;
		logger.log(Level.DEBUG, "  Instance to set field in: "+instance);
		
		/*
		 * If this is the root node, re-insert as final node
		 * Otherwise set field directly
		 */
		if (current.getType()==Type.ROOT) {
			CurrentElement result = new CurrentElement(Type.FINAL);
			result.addElement("", toSet.getClass(), null, null);
			result.expectedElements.get("").value=toSet;
			logger.log(Level.DEBUG, "Created final element "+result+" with "+toSet);
			stck.push(result);
			return;
		}
		
		/*
		 * Set directly
		 */
		try {
			logger.log(Level.DEBUG, "  Set field '"+field.getName()+"' value of instance "+instance.getClass()+" to "+toSet);
			field.setAccessible(true);
			field.set(instance, toSet);
		} catch (Exception e) {
			throw new SerializationException("Failed setting field "+field+" in "+instance.getClass(),e);
		}
		
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	
	public <T> T read(Class<T> cls, XMLEventReader evRd) throws IOException, SerializationException {
		// Get root annotation from class to obtain expected tag name
		Root rootAnno = cls.getAnnotation(Root.class);
		if (rootAnno==null)
			throw new SerializationException("Class "+cls+" misses @Root");
		
		String expectedTag = rootAnno.name();
		
		CurrentElement current = new CurrentElement(Type.ROOT);
		current.addElement(expectedTag, cls, null, null);
		Stack<CurrentElement>  stckCurrent = new Stack<CurrentElement>();		
		stckCurrent.push(current);
		
		try {
			while( evRd.hasNext() ) {
				XMLEvent ev = evRd.nextEvent();
				logger.log(Level.DEBUG, "------------------------------------------------------------------");
				switch (ev.getEventType()) {
				case XMLEvent.START_DOCUMENT:
					continue;
				case XMLEvent.START_ELEMENT:
					current = processStartElement(stckCurrent, ev.asStartElement());
					if (current!=null)
						stckCurrent.push(current);
					logger.log(Level.DEBUG, "  current stack is "+stckCurrent);
					continue;
				case XMLEvent.END_ELEMENT:
					processEndElement(stckCurrent, ev.asEndElement());
					continue;
				case XMLEvent.CHARACTERS:
					logger.log(Level.DEBUG, "CHARACTERS "+ev);
					current = stckCurrent.peek();
//					logger.log(Level.DEBUG, "  current is "+current.dump());
					current.collectedText.append(ev.asCharacters().getData());
//					logger.log(Level.DEBUG, "  current stack is = "+stckCurrent);
//					/*
//					 * If TEXT is expected, fill it
//					 */
//					if (current.getType()==Type.TEXT) {
//						logger.log(Level.DEBUG, "  add text: "+ev.asCharacters().getData());
//						ByNameInfo info = current.expectedElements.values().iterator().next();
//						((StringBuffer)info.instance).append(ev.asCharacters().getData());
//
//					}
////					if (current!=null)
////						current.text.append(ev.asCharacters().getData());
					continue;
				case XMLEvent.END_DOCUMENT:
					logger.log(Level.DEBUG, "END_DOCUMENT");
					current = stckCurrent.peek();
					logger.log(Level.DEBUG, "Return "+current.expectedElements.values().iterator().next().value);
//					CurrentElementFinal currentFinal = (CurrentElementFinal) stckCurrent.pop();
					return (T)current.expectedElements.values().iterator().next().value;
				case XMLEvent.COMMENT:
					continue;
				default:
					logger.log(Level.ERROR, "? "+ev.getEventType()) ;
					System.exit(0);
				}

			}
		} catch (InstantiationException |IllegalAccessException e) {
			throw new SerializationException(e);
		} catch (SerializationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SerializationException(e);
		}
		return null;
	}

//	//-------------------------------------------------------------------
//	private void determineExpectationForAttributeField(Field field, CurrentElement addTo, Object instance) {
//		Attribute attrAnno = field.getAnnotation(Attribute.class);
//		Class<?> cls = field.getType();
//		String  name = field.getName();
//
//		if (attrAnno!=null) {
//			if (attrAnno.name().length()>0)
//				name = attrAnno.name();
//			addTo.addAttribute(name, cls, field, instance);
//		}
//	}

	//-------------------------------------------------------------------
	public static Map<String,ByNameInfo> determineExpectedElements(Class<?> clazz) throws SerializationException {
		Map<String,ByNameInfo> ret = new HashMap<>();
		for (Field field : Util.getElementFields(clazz)) {
			logger.log(Level.DEBUG, "  * "+field);
			for (ByNameInfo info : determineExpectationForElementField(field))
				ret.put(info.elementName, info);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static ByNameInfo determineExpectationForAttributeField(Field field) {
		Attribute attrAnno = field.getAnnotation(Attribute.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;

		if (attrAnno!=null) {
			if (attrAnno.name().length()>0)
				name = attrAnno.name();
			req = attrAnno.required();
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;
		
		return info;
		
	}


	//-------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForElementField(Field field) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();
		
		Element elemAnno = field.getAnnotation(Element.class);
		ElementList eListAnno = field.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = field.getAnnotation(ElementListUnion.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;
		ArrayList<?> list = null;
		boolean inline = false;

		if (elemAnno!=null) {
			if (eListAnno!=null) {
				logger.log(Level.WARNING, "    @Element and @ElementList exclude one another. Prefer @Element");
			}
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @Element and @ElementListUnion exclude one another. Prefer @Element");
			}
			if (elemAnno.name().length()>0)
				name = elemAnno.name();
			req = elemAnno.required();
		} else if (eListAnno!=null) {
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			cls = ArrayList.class;
			if (eListAnno.inline()) {
				list = new ArrayList<>();
				inline = true;
				if (eListAnno.entry().length()>0) {
					name = eListAnno.entry();
				} else {
					java.lang.reflect.Type listType = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
					logger.log(Level.DEBUG, "search @Root for "+listType.getTypeName());
					try {
						cls = Class.forName(listType.getTypeName());
						Root rootAnno = cls.getAnnotation(Root.class);
						if (rootAnno==null)
							throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
						name = rootAnno.name();
					} catch (ClassNotFoundException e) {
						throw new SerializationException();
					}
				}
				if (eListAnno.type()!=null)
					cls = eListAnno.type();
			}
			req = eListAnno.required();
		} else if (elUnionAnno!=null) {
			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = field;
				info.required = false;
				info.listInstance = list;
				ret.add(info);
				
			}
			return ret;
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;
		info.listInstance = list;
		info.isInlineList = inline;
		ret.add(info);
		
		return ret;
		
	}


	//-------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForElementListField(Field field) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();
		
		ElementList eListAnno = field.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = field.getAnnotation(ElementListUnion.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;
		ArrayList<?> list = null;
		boolean inline = false;

		if (eListAnno!=null) {
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			cls = ArrayList.class;
			if (eListAnno.inline()) {
				list = new ArrayList<>();
				inline = true;
				if (eListAnno.entry().length()>0) {
					name = eListAnno.entry();
				} else {
					java.lang.reflect.Type listType = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
					logger.log(Level.DEBUG, "search @Root for "+listType.getTypeName());
					try {
						cls = Class.forName(listType.getTypeName());
						Root rootAnno = cls.getAnnotation(Root.class);
						if (rootAnno==null)
							throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
						name = rootAnno.name();
					} catch (ClassNotFoundException e) {
						throw new SerializationException();
					}
				}
				if (eListAnno.type()!=null)
					cls = eListAnno.type();
			}
			req = eListAnno.required();
		} else if (elUnionAnno!=null) {
			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = field;
				info.required = false;
				info.listInstance = list;
				ret.add(info);
				
			}
			return ret;
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;
		info.listInstance = list;
		info.isInlineList = inline;
		ret.add(info);
		
		return ret;
		
	}

	//------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForClassAttributes(Class<?> clazz) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();
		
		for (Field field : Util.getAttributeFields(clazz)) {
			ret.add(determineExpectationForAttributeField(field));
		}
		return ret;
		
	}

	//------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForClassElements(Class<?> clazz) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();
		
		ElementList eListAnno = (ElementList) clazz.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = (ElementListUnion) clazz.getAnnotation(ElementListUnion.class);
		Class<?> cls = null;
		String  name = null;
//		boolean req  = false;
		ArrayList<?> list = null;

		if (eListAnno!=null) {
			if (!List.class.isAssignableFrom(clazz)) 
				throw new SerializationException("@ElementList only valid for instances of java.util.List, but "+clazz+" isn't");
			
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			logger.log(Level.DEBUG, "-----@ElementList for "+clazz);
			cls = ArrayList.class;
			list = new ArrayList<>();
			if (eListAnno.entry().length()>0) {
				name = eListAnno.entry();
			} else {
//				java.lang.reflect.Type listType = ((ParameterizedType)clazz.g.getGenericType()).getActualTypeArguments()[0];
				logger.log(Level.DEBUG, "type params "+Arrays.toString(clazz.getTypeParameters()));
//				try {
//					cls = Class.forName(listType.getTypeName());
//					Root rootAnno = cls.getAnnotation(Root.class);
//					if (rootAnno==null)
//						throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
//					name = rootAnno.name();
//				} catch (ClassNotFoundException e) {
//					throw new SerializationException();
//				}
			}
			if (eListAnno.type()!=null)
				cls = eListAnno.type();
//			req = eListAnno.required();
		} else if (elUnionAnno!=null) {
			if (!List.class.isAssignableFrom(clazz)) 
				throw new SerializationException("@ElementListUnion only valids for instances of java.util.List, but "+clazz+" isn't");
			
			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = null;
				info.required = false;
				info.listInstance = list;
				ret.add(info);
			}
		}
		
		for (Field field : Util.getElementFields(clazz)) {
			logger.log(Level.DEBUG, "    field "+field);
			ret.addAll(determineExpectationForElementField(field));
		}
		
		return ret;
		
	}

//	//-------------------------------------------------------------------
//	private void determineExpectationForParentField(Field field, CurrentElement addTo, Object instance) {
//		if (field==null)
//			return;
//		ElementList eListAnno = field.getAnnotation(ElementList.class);
//		ElementListUnion elUnionAnno = field.getAnnotation(ElementListUnion.class);
//		Class<?> cls = field.getType();
//		String  name = null;
//
//		if (eListAnno!=null) {
//			if (elUnionAnno!=null) {
//				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
//			}
//			logger.log(Level.DEBUG, "-----@ElementList for "+field);
//			if (eListAnno.type().getAnnotation(Root.class)!=null)
//				name=eListAnno.type().getAnnotation(Root.class).name();
//			if (eListAnno.entry().length()>0)
//				name = eListAnno.entry();
//			cls = eListAnno.type();
//			addTo.addElement(name, cls, field, instance);
//		} else if (elUnionAnno!=null) {
//			for (ElementList tmp : elUnionAnno.value()) {
//				name = tmp.entry();
//				cls = tmp.type();
//				addTo.addElement(name, cls, field, instance);
//			}
//		}
//		
//	}
//
//	//-------------------------------------------------------------------
//	private CurrentElement processInlineListStartElement(Stack<CurrentElement> stck, StartElement ev) throws IOException, SerializationException, InstantiationException, IllegalAccessException {
//		return null;
//	}

	//-------------------------------------------------------------------
	CurrentElement processStartElement(Stack<CurrentElement> stck, StartElement ev) throws IOException, SerializationException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
		logger.log(Level.DEBUG, "START "+ev);
		CurrentElement current = stck.peek();
		logger.log(Level.DEBUG, "  current stack is "+stck);
		logger.log(Level.DEBUG, current.dump());
//		logger.log(Level.DEBUG, "  allowed/expected are: "+current.expectedElements);

		/*
		 * Compare found element with expected elements 
		 */
		String name = ev.getName().getLocalPart();
		ByNameInfo info = current.getInfoForElement(name);
		if (info==null) {
			logger.log(Level.WARNING, "Found unexpected element '"+ev+"'. Ignore it and all children");
			System.err.println("Found unexpected element '"+ev+"'. Ignore it and all children");
			return new CurrentElement(Type.IGNORE);
		}
		logger.log(Level.DEBUG, "  Found expected element "+name+info.dump());
		current.expectedClose = info;
		
		CurrentElement next = new CurrentElement(Type.VALID);
		next.expectedClose = info;
		
		/*
		 * Instantiate the required type
		 */
		logger.log(Level.DEBUG, "  create instance of "+info.cls);
		Object instance = info.cls.getDeclaredConstructor().newInstance();
		info.value = instance;
		
		/*
		 * Determine all expected attributes for the class
		 */
		logger.log(Level.DEBUG, "  Build list of expected attributes for "+info.cls);
		for (ByNameInfo expInfo : determineExpectationForClassAttributes(info.cls)) {
			expInfo.fieldInstance = instance;
			next.expectedAttributes.put(expInfo.elementName, expInfo);
		}
		
//		for (Field field : getAttributeFields(info.cls)) {
//			logger.log(Level.DEBUG, "  * "+field);
//			determineExpectationForAttributeField(field, next, instance);
//		}
		logger.log(Level.DEBUG, "    Expected attributes are: "+next.expectedAttributes);
		
		/*
		 * Determine all expected elements for the class
		 */
		logger.log(Level.DEBUG, "  Build list of expected elements for "+info.cls);
		for (ByNameInfo expInfo : determineExpectationForClassElements(info.cls)) {
			expInfo.fieldInstance = instance;
			next.expectedElements.put(expInfo.elementName, expInfo);
		}
		logger.log(Level.DEBUG, "    Expected elements are: "+next.expectedElements);
		
		/*
		 * Read and parse attributes
		 */
		parseAttributes(ev, next);
		
		/*
		 * Special handling for lists. Determine possible list contents
		 */
		if (List.class.isAssignableFrom(info.cls)) {
			logger.log(Level.DEBUG, "  Found a list"+info.dump());
			next.setType(Type.LIST);
			String expElem = null;
			Class<?> expCls= null;
			
			ParameterizedType pType = (ParameterizedType)info.field.getGenericType();
			expCls = getClass(pType.getActualTypeArguments()[0]);
			if (expCls.isAnnotationPresent(Root.class)) {
				expElem = expCls.getAnnotation(Root.class).name();
			}
			logger.log(Level.DEBUG, "  Without considering @ElementList, expected element would be '"+expElem+"' and instantiated type "+expCls);

			/*
			 * Now that the default behavior is known, see if it is overwritten by annotations
			 */
			ElementList eListAnno = info.field.getAnnotation(ElementList.class);
			ElementListUnion elUnionAnno = info.field.getAnnotation(ElementListUnion.class);
			if (eListAnno==null && elUnionAnno==null)
				throw new SerializationException("Missing @ElementList or @ElementListUnion at "+info.field);
			
			// @ElementList
			if (eListAnno!=null) {
				if (eListAnno.entry().length()>0)
					expElem = eListAnno.entry();
				if (eListAnno.type()!=null)
					expCls = eListAnno.type();
				logger.log(Level.DEBUG, "  After considering @ElementList, expected element would be '"+expElem+"' and instantiated type "+expCls);
				next.addElement(expElem, expCls, null, null);
			}
			
			logger.log(Level.DEBUG, "  next = "+next.dump());
		}
		
		/*
		 * List all possible child elements for the class
		 */
//		logger.log(Level.DEBUG, "  Build list of expected elements for "+info.field);
//		for (Field field : getElementFields(info.cls)) {
//			logger.log(Level.DEBUG, "  * "+field);
//			determineExpectationForElementField(field);
//		}
//		determineExpectationForParentField(info.field, next, instance);
		logger.log(Level.DEBUG, "  Expected next are: "+next.expectedElements);
		logger.log(Level.DEBUG, "  Expected close is: "+current.expectedClose);
		
//		if (next.expectedElements.isEmpty()) {
//			logger.log(Level.DEBUG, "  START ended without expecting more elements");
//			// Reset collected characters
//			current.collectedText = new StringBuffer();
//			next.setType(Type.TEXTONLY);
//		}
		logger.log(Level.DEBUG, "  START ended with "+next.dump());
		return next;
	}
	
	 
	//-------------------------------------------------------------------
	public static String getClassName(java.lang.reflect.Type type) {
		String TYPE_NAME_PREFIX = "class ";
	    if (type==null) {
	        return "";
	    }
	    String className = type.toString();
	    if (className.startsWith(TYPE_NAME_PREFIX)) {
	        className = className.substring(TYPE_NAME_PREFIX.length());
	    }
	    return className;
	}
	 
	//-------------------------------------------------------------------
	public static Class<?> getClass(java.lang.reflect.Type type) {
	    String className = getClassName(type);
	    if (className==null || className.isEmpty()) {
	        return null;
	    }
	    try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			return null;
		}
	}
}
