/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

/**
 * @author prelle
 *
 */
public class CurrentElement {
	
	enum Type {
		ROOT,
		VALID,
		LIST,
		IGNORE,
		FINAL, 
		TEXTONLY
	}

	private Type type;
	ByNameInfo expectedClose;
	StringBuffer collectedText;
	Map<String, ByNameInfo> expectedElements;
	Map<String, ByNameInfo> expectedAttributes;
	
	//-------------------------------------------------------------------
	@SuppressWarnings("exports")
	public CurrentElement(Type type) {
		this.type = type;
		collectedText      = new StringBuffer();
		expectedElements   = new HashMap<>();
		expectedAttributes = new HashMap<>();
	}

	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("\n    "+type);
		buf.append("\n    collected text = "+collectedText.length()+" characters");
		buf.append("\n    Expected elements are:");
		for (Entry<String, ByNameInfo> entry : expectedElements.entrySet()) {
			buf.append("\n    * "+entry.getKey()+" = "+entry.getValue().dump());
		}
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return type+"(expEle="+expectedElements.keySet()+"  expAtt="+expectedAttributes.keySet()+")";
	}

	//-------------------------------------------------------------------
	void addAttribute(String name, Class<?> cls, Field field, Object instance) {
		ByNameInfo info = expectedAttributes.get(name);
		if (info==null) {
			info = new ByNameInfo();
			expectedAttributes.put(name, info);
		}
		info.cls = cls;
		info.field = field;
		info.fieldInstance = instance;
	}

	//-------------------------------------------------------------------
	void addElement(String name, Class<?> cls, Field field, Object instance) {
		ByNameInfo info = expectedElements.get(name);
		if (info==null) {
			info = new ByNameInfo();
			expectedElements.put(name, info);
		}
		info.cls = cls;
		info.field = field;
		info.fieldInstance = instance;
	}

	//-------------------------------------------------------------------
	public ByNameInfo getInfoForElement(String name) {
		return expectedElements.get(name);
	}

	//-------------------------------------------------------------------
	public ByNameInfo getInfoForAttribute(String name) {
		return expectedAttributes.get(name);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the type
	 */
	@SuppressWarnings("exports")
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param type the type to set
	 */
	@SuppressWarnings("exports")
	public void setType(Type type) {
		this.type = type;
	}

}
