/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

import org.prelle.simplepersist.StringValueConverter;

/**
 * CharacterLeafs indicate that the characters here shall not be ignored
 * @author prelle
 *
 */
public class XMLTextLeaf extends XMLTreeItem {
	/**
	 * The element name
	 */
	private String name;
	private Class<?> cls;
	private boolean required;
	
	/**
	 * Not set by TreeBuilder, but set upon parsing document
	 */
	private String value;
	private StringValueConverter<?> converter;

	//-------------------------------------------------------------------
	/**
	 */
	public XMLTextLeaf(String name, Class<?> cls) {
		this.name = name;
		this.cls  = cls;
	}

	//-------------------------------------------------------------------
	String dump(int indent) {
		StringBuffer buf = new StringBuffer();
		buf.append("\n");
		for (int i=0; i<indent; i++)
			buf.append("  ");
		buf.append("<"+name);
		if (converter!=null)
			buf.append(" conv="+converter);
		buf.append(">TEXT:"+cls.getSimpleName()+"</"+name+">");
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	//-------------------------------------------------------------------
	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cls
	 */
	public Class<?> getType() {
		return cls;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the converter
	 */
	public StringValueConverter<?> getConverter() {
		return converter;
	}

	//-------------------------------------------------------------------
	/**
	 * @param converter the converter to set
	 */
	public void setConverter(StringValueConverter<?> converter) {
		this.converter = converter;
	}

}
