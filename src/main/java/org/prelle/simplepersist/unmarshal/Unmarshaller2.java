/**
 *
 */
package org.prelle.simplepersist.unmarshal;

import java.io.IOException;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

import org.prelle.simplepersist.AfterLoadHook;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.DeSerializer;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.EnumValue;
import org.prelle.simplepersist.MapEntry;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.Util;

/**
 * @author prelle
 *
 */
public class Unmarshaller2 implements DeSerializer {

	private final static Logger logger = System.getLogger("xml");

	private final static DateFormat FORMAT2 = DateFormat.getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM, Locale.GERMANY);
	private final static SimpleDateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX");
	private final static SimpleDateFormat FORMAT3 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX");


	//-------------------------------------------------------------------
	public static Iterable<Field> getAllFields(Class<?> startClass) {

		List<Field> currentClassFields = new ArrayList<Field>(Arrays.asList(startClass.getDeclaredFields()));
		Class<?> parentClass = startClass.getSuperclass();

		if (parentClass != Object.class && parentClass!=null) {
			List<Field> parentClassFields = (List<Field>) getAllFields(parentClass);
			currentClassFields.addAll(parentClassFields);
		}

		return currentClassFields;
	}

	//-------------------------------------------------------------------
	private <T> void parseAttributes(Class<T> type, XMLEventReader evRd, StartElement elem, T obj) throws SerializationException {
		// Build a list of possible fields
		Map<String, Field> fieldsByAttribute = new HashMap<String, Field>();
		for (Field field : Util.getAttributeFields(type)) {
			Attribute attr = field.getAnnotation(Attribute.class);
			if (attr.name()!=null && attr.name().length()>0)
				fieldsByAttribute.put(attr.name(), field);
			else
				fieldsByAttribute.put(field.getName(), field);
		}


		Iterator<javax.xml.stream.events.Attribute> iter = elem.getAttributes();
		while( iter.hasNext() ) {
			javax.xml.stream.events.Attribute a = iter.next();
			logger.log(Level.DEBUG, "Attr "+a);
			String name = a.getName().getLocalPart();
			String sVal = a.getValue();

			Field field = fieldsByAttribute.get(name);
			field.setAccessible(true);
			logger.log(Level.DEBUG, "  write to "+field);

			try {
				if (field.getType()==String.class) {
					field.set(obj, sVal);
					fieldsByAttribute.remove(name);
				} else if (field.getType()==int.class || field.getType()==Integer.class) {
					field.set(obj, Integer.valueOf(sVal));
					fieldsByAttribute.remove(name);
				} else if (field.getType().isEnum()) {
					Method valueOf = field.getType().getMethod("valueOf", String.class);
					field.set(obj, valueOf.invoke(null, sVal));
					fieldsByAttribute.remove(name);
				} else
					logger.log(Level.WARNING, "Cannot set "+field.getType());
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		logger.log(Level.WARNING, "TODO: Unfilled attributes "+fieldsByAttribute);
	}

	//-------------------------------------------------------------------
	private <T> void parseListField(XMLEventReader evRd, StartElement elem, Field field, List<Object> list) throws SerializationException {
		Class<?> type = field.getAnnotation(ElementList.class).type();
		logger.log(Level.DEBUG, "Parse "+elem+" as "+type+" and add it to list "+list+" in field "+field);

		//		logger.log(Level.DEBUG, "Item2 "+list.getClass().getGenericSuperclass());
		//		logger.log(Level.DEBUG, "Item2 "+((ParameterizedType)list.getClass().getGenericSuperclass()).getActualTypeArguments()[0] );
		//		Class<T> persistentClass = (Class<T>)
		//				   ((ParameterizedType)list.getClass().getGenericSuperclass())
		//				      .getActualTypeArguments()[0];

		Object listElem = parse(type, evRd, elem);
		logger.log(Level.DEBUG, "Add item "+listElem+" to "+field.getName());
		list.add(listElem);

		try {
			while (evRd.hasNext()) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					continue;
				case XMLEvent.END_ELEMENT:
					logger.log(Level.DEBUG, "Ending "+ev);
					return;
				default:
					logger.log(Level.ERROR, "Not handled_ "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (Exception e) {
			throw new SerializationException("Failed parsing", e);
		}
	}

	//-------------------------------------------------------------------
	private void parseText(XMLEventReader evRd, StartElement elem, Field field, Object obj) throws SerializationException {
		StringBuffer text = new StringBuffer();
		try {
			while (evRd.hasNext()) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					text.append(ev.asCharacters().getData());
					continue;
				case XMLEvent.END_ELEMENT:
					field.setAccessible(true);
					field.set(obj, text.toString());
					logger.log(Level.DEBUG, "Set "+field.getName()+" = "+text+"    EV="+ev );
					return;
				default:
					logger.log(Level.ERROR, "Not handled_ "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (Exception e) {
			throw new SerializationException("Failed parsing", e);
		}
	}

	//-------------------------------------------------------------------
	private <T> void parseElements(Class<T> type, XMLEventReader evRd, StartElement elem, T obj) throws SerializationException {
		// Keep a space for every list by field
		Map<Field, List<Object>> listByField = new HashMap<>();
		// Build a list of possible fields
		Map<String, Field> fieldsByElements = new HashMap<String, Field>();
		for (Field field : Util.getElementFields(type)) {
			if (field.getAnnotation(Element.class)!=null) {
				Element attr = field.getAnnotation(Element.class);
				logger.log(Level.DEBUG, "Field "+field);
				if (attr.name()!=null && attr.name().length()>0)
					fieldsByElements.put(attr.name(), field);
				else
					fieldsByElements.put(field.getName(), field);
			} else
				if (field.getAnnotation(ElementList.class)!=null) {
					ElementList attr = field.getAnnotation(ElementList.class);
					logger.log(Level.DEBUG, "Field "+field);
					if (attr.entry()!=null && attr.entry().length()>0)
						fieldsByElements.put(attr.entry(), field);
					else
						fieldsByElements.put(field.getName(), field);
					List<Object> list = new ArrayList<>();
					listByField.put(field, list);
				}
		}

		try {
			while( evRd.hasNext() ) {
				XMLEvent ev = evRd.nextEvent();
				switch (ev.getEventType()) {
				case XMLEvent.CHARACTERS:
					continue;
				case XMLEvent.START_ELEMENT:
					logger.log(Level.DEBUG, "EV = "+ev+" // "+fieldsByElements);
					Field field = fieldsByElements.get(ev.asStartElement().getName().getLocalPart());
					logger.log(Level.DEBUG, "Start "+field);
					if (field.getAnnotation(ElementList.class)!=null) {
						parseListField(evRd, ev.asStartElement(), field, listByField.get(field));
					} else if  (field.getType()==String.class) {
						parseText(evRd, ev.asStartElement(), field, obj);
					} else {
						logger.log(Level.WARNING, "Cannot process "+field);
						System.exit(0);
					}
					break;
				case XMLEvent.END_ELEMENT:
					logger.log(Level.DEBUG, "END = "+ev);
					return;
				default:
					logger.log(Level.ERROR, "Not processed = "+ev+" / "+ev.getEventType());
					System.exit(0);
				}
			}
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}

		logger.log(Level.WARNING, "TODO: Unfilled attributes "+fieldsByElements);
	}

	//-------------------------------------------------------------------
	private <T> T parse(Class<T> type, XMLEventReader evRd, StartElement elem) throws SerializationException {
		// Create an object instance using the empty constructor
		T obj = null;
		try {
			obj = type.getDeclaredConstructor().newInstance();
		} catch (Exception e) {
			throw new SerializationException("Failed instantiating "+type,e);
		}
		logger.log(Level.DEBUG, "Class "+obj);

		parseAttributes(type, evRd, elem, obj);
		parseElements(type, evRd, elem, obj);

		return obj;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private XMLTreeItem processEndElement(XMLTextLeaf current, EndElement ev) throws SerializationException {
		logger.log(Level.DEBUG, "STOP  "+ev);
		String name = ev.getName().getLocalPart();
		logger.log(Level.DEBUG, "  name = "+name);

		logger.log(Level.DEBUG, "  current = "+current);

		XMLTreeNode parent = current.getParent();
		logger.log(Level.DEBUG, "  parent  = "+parent);
		logger.log(Level.DEBUG, "  parent.elemConv  = "+parent.getElementConverter());

		/*
		 * Set value. Eventually convert, if other values than string are expected
		 */
		current.field.setAccessible(true);
		Class<?> expected = current.getField().getType();
		Object toSet = current.getValue();
		logger.log(Level.DEBUG, "toSet before conversion: "+toSet);
		if (current.getConverter()!=null) {
			logger.log(Level.DEBUG, "Calling converter "+current.getConverter()+" with value "+toSet);
			try {
				toSet = current.getConverter().read(String.valueOf(toSet));
			} catch (Exception e) {
				throw new SerializationException("Failed calling value converter",e);
			}
		}
		logger.log(Level.DEBUG, "toSet after  conversion: "+toSet);

		try {
			/*
			 * Check if conversion is necessary
			 */
			if (expected==String.class || expected==Object.class) {
				// No conversion required
			} else if (expected==int.class || expected==Integer.class) {
				// Conversion to integer
				toSet = Integer.parseInt(current.getValue());
			} else if (expected.isEnum()) {
				// Conversion to enum
				logger.log(Level.DEBUG, "  try to convert '"+current.getValue()+"' to enum field of "+expected);
				boolean foundEnum = false;
				for (Field enumField : expected.getDeclaredFields()) {
					if (enumField.isAnnotationPresent(EnumValue.class)) {
						EnumValue anno = enumField.getAnnotation(EnumValue.class);
						if (anno.value().equals(current.getValue())) {
							 logger.log(Level.DEBUG, "    Use "+Enum.valueOf((Class<Enum>) expected, enumField.getName()));
							toSet =  Enum.valueOf((Class<Enum>)expected, enumField.getName());
							foundEnum = true;
							break;
						}
					}
				}
				// If not found by @EnumValue, try default Enum.valueOf
				if (!foundEnum) {
					toSet = Enum.valueOf((Class<Enum>) expected, current.getValue());
					 logger.log(Level.DEBUG, "    Use default "+toSet);
				}
			} else if (expected==List.class) {
				logger.log(Level.DEBUG, "Add "+toSet+" to list "+current.getParent().getInstance());
				((List)current.getParent().getInstance()).add(toSet);
				// Clear
				current.setValue(null);
				return current.getParent();
			} else {
				logger.log(Level.ERROR, "  Don't know how to convert from String to "+expected);
				throw new SerializationException("  Don't know how to convert from String to "+expected);
			}
			logger.log(Level.DEBUG, "  set '"+toSet+"' for field "+current.getField()+" of instance "+parent.getInstance());
			current.field.set(parent.getInstance(), toSet);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SerializationException("Failed setting field "+current.getField()+" with "+current.getValue(),e);
		}

		// Clear
		current.setValue(null);

		return current.getParent();
	}

	//-------------------------------------------------------------------
	private XMLTreeItem processEndElement(XMLBinaryLeaf current, EndElement ev) throws SerializationException {
		logger.log(Level.DEBUG, "STOP  "+ev);
		String name = ev.getName().getLocalPart();
		logger.log(Level.DEBUG, "  name = "+name);

		logger.log(Level.DEBUG, "  current = "+current);

		XMLTreeNode parent = current.getParent();

		/*
		 * Decode
		 */
		byte[] decoded = Base64.getDecoder().decode(current.getValue().toString());

		if (logger.isLoggable(Level.DEBUG))
			logger.log(Level.DEBUG, "  set '"+decoded+"' for field "+current.getField()+" of instance "+parent.getInstance());
		current.field.setAccessible(true);
		try {
			current.field.set(parent.getInstance(), decoded);
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SerializationException("Failed setting field "+current.getField()+" with "+decoded,e);
		}

		return current.getParent();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private XMLTreeItem processEndElement(XMLTreeNode current, EndElement ev) throws SerializationException {
		logger.log(Level.DEBUG, "STOP  "+ev);
		String name = ev.getName().getLocalPart();
		logger.log(Level.DEBUG, "  name = "+name);


		logger.log(Level.TRACE, "  current = "+current.dump());
		logger.log(Level.TRACE, "  current = "+current.dumpData());
		logger.log(Level.TRACE, "  parent  = "+current.getParent().dumpData());

		/*
		 * For list items, there is no current.field, but a parent list instance to add to
		 */
		if (current.getField()==null && current.getParent().getInstance() instanceof List) {
			logger.log(Level.DEBUG, "  add "+current.getType()+"/"+current.getInstance()+" to parent list "+current.getParent().getInstance().getClass());
			((List)current.getParent().getInstance()).add(current.getInstance());
			return current.getParent();
		}

		/*
		 * For child elements of parent map entry values
		 */
		if (current.getField()==null && current.getParent().isMapEntryValue()) {
			logger.log(Level.DEBUG, "  set "+current.getInstance()+" as map entry value ");
			current.getParent().setInstance(current.getInstance());
			return current.getParent();
		}

		/*
		 * For closing map entry values
		 */
		if (current.isMapEntry()) {
			logger.log(Level.DEBUG, "  put "+current.getInstance()+" to map "+current.getParent().getInstance());
			MapEntry entry = (MapEntry)current.getInstance();
			((Map)current.getParent().getInstance()).put(entry.getKey(), entry.getValue());
			return current.getParent();
		}

		/*
		 * The root node
		 */
		if (current.getField()==null) {
			// This is the root node
			logger.log(Level.DEBUG, "  end of root node");
			return current;
		}

		XMLTreeNode parent = current.getParent();
		logger.log(Level.DEBUG, "  field = "+current.getField());
		logger.log(Level.DEBUG, "  instn = "+parent.getInstance());
		current.getField().setAccessible(true);
		try {
			/*
			 * Call add() for Lists and set directly for non-list
			 */
			if (List.class.isAssignableFrom(current.getField().getType()) && current.isInlineList()) {
				logger.log(Level.DEBUG, "  assume that elements for inline lists have already been added");
			} else {
				AfterLoadHook annoHook = current.getField().getAnnotation(AfterLoadHook.class);
				if (annoHook!=null) {
					try {
						annoHook.value().getDeclaredConstructor().newInstance().afterLoad(current.getInstance());
					} catch (Throwable e) {
						logger.log(Level.ERROR, "Failed calling AfterLoadHook "+annoHook.value(),e);
					}
//					logger.fatal("Stop for "+current.getInstance());
//					System.exit(0);
				}
				logger.log(Level.DEBUG, "  set field "+current.field+" to "+current.getInstance());
				current.field.set(parent.getInstance(), current.getInstance());
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			throw new SerializationException("Failed setting field "+current.getField()+" with "+current.getInstance(),e);
		}

		return current.getParent();
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked"})
	public <T> T read(Class<T> cls, XMLEventReader evRd) throws IOException, SerializationException {
		XMLTreeNode tree = TreeBuilder.convertToTree(cls);
		if (logger.isLoggable(Level.TRACE))
			logger.log(Level.TRACE, "tree = "+tree.dump());
		XMLRootNode root = new XMLRootNode();
		root.addChild(tree);
		XMLTreeItem current = root;

		try {
			while( evRd.hasNext() ) {
				XMLEvent ev = evRd.nextEvent();
				logger.log(Level.DEBUG, "------------------------------------------------------------------");
				switch (ev.getEventType()) {
				case XMLEvent.START_DOCUMENT:
					continue;
				case XMLEvent.START_ELEMENT:
					if (current instanceof XMLTreeNode)
						current = processStartElement((XMLTreeNode) current, ev.asStartElement(), evRd);
					else
						throw new SerializationException("Expect to be in XMLTreeNode but found "+current.getClass().getSimpleName());
					continue;
				case XMLEvent.END_ELEMENT:
					if (current instanceof XMLTextLeaf) {
						current = processEndElement((XMLTextLeaf)current, ev.asEndElement());
					} else if (current instanceof XMLBinaryLeaf) {
						current = processEndElement((XMLBinaryLeaf)current, ev.asEndElement());
					} else if (current instanceof XMLTreeNode)
						current = processEndElement((XMLTreeNode)current, ev.asEndElement());
					else
						throw new SerializationException("Expect to be in XMLTreeNode but found "+current.getClass());
					continue;
				case XMLEvent.CHARACTERS:
					logger.log(Level.DEBUG, "CHARACTERS "+ev);
					logger.log(Level.DEBUG, "  current = "+current+" /// "+current.getClass());
					String value = ev.asCharacters().getData();

					if (current instanceof XMLTextLeaf) {
						XMLTextLeaf textLeaf = (XMLTextLeaf) current;
						logger.log(Level.DEBUG, "  set '"+value+"' as field "+textLeaf.getField());
						if (textLeaf.getValue()==null)
							textLeaf.setValue(value);
						else
							textLeaf.setValue(textLeaf.getValue()+value);
					} else if (current instanceof XMLBinaryLeaf) {
						XMLBinaryLeaf textLeaf = (XMLBinaryLeaf) current;
						logger.log(Level.DEBUG, "  set '"+value+"' as field "+textLeaf.getField());
						if (textLeaf.getValue()==null)
							textLeaf.setValue(value);
						else
							textLeaf.setValue(textLeaf.getValue()+value);
					} else if (!(current instanceof XMLTreeNode)){
						logger.log(Level.WARNING, "Don't know what to do with CHARACTERS for type "+current.getClass());
					}
					continue;
				case XMLEvent.END_DOCUMENT:
					logger.log(Level.DEBUG, "END_DOCUMENT");
					if (current instanceof XMLTreeNode) {
						logger.log(Level.DEBUG, "  return "+((XMLTreeNode)current).getInstance());
						return (T)((XMLTreeNode)current).getInstance();
					} else
						throw new SerializationException("Expect to be in XMLTreeNode but found "+current.getClass().getSimpleName());
				case XMLEvent.COMMENT:
					continue;
				case XMLEvent.DTD:
					continue;
				default:
					logger.log(Level.ERROR, "? "+ev.getEventType()) ;
					System.exit(0);
				}

			}
		} catch (InstantiationException |IllegalAccessException e) {
			throw new SerializationException(e);
		} catch (SerializationException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
			throw new SerializationException(e+" in "+current);
		}
		return null;
	}

	//-------------------------------------------------------------------
	public static Map<String,ByNameInfo> determineExpectedElements(Class<?> clazz) throws SerializationException {
		Map<String,ByNameInfo> ret = new HashMap<>();
		for (Field field : Util.getElementFields(clazz)) {
			logger.log(Level.DEBUG, "  * "+field);
			for (ByNameInfo info : determineExpectationForElementField(field))
				ret.put(info.elementName, info);
		}
		return ret;
	}

	//-------------------------------------------------------------------
	public static ByNameInfo determineExpectationForAttributeField(Field field) {
		Attribute attrAnno = field.getAnnotation(Attribute.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;

		if (attrAnno!=null) {
			if (attrAnno.name().length()>0)
				name = attrAnno.name();
			req = attrAnno.required();
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;

		return info;

	}


	//-------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForElementField(Field field) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();

		Element elemAnno = field.getAnnotation(Element.class);
		ElementList eListAnno = field.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = field.getAnnotation(ElementListUnion.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;
		ArrayList<?> list = null;
		boolean inline = false;

		if (elemAnno!=null) {
			if (eListAnno!=null) {
				logger.log(Level.WARNING, "    @Element and @ElementList exclude one another. Prefer @Element");
			}
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @Element and @ElementListUnion exclude one another. Prefer @Element");
			}
			if (elemAnno.name().length()>0)
				name = elemAnno.name();
			req = elemAnno.required();
		} else if (eListAnno!=null) {
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			cls = ArrayList.class;
			if (eListAnno.inline()) {
				list = new ArrayList<>();
				inline = true;
				if (eListAnno.entry().length()>0) {
					name = eListAnno.entry();
				} else {
					java.lang.reflect.Type listType = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
					logger.log(Level.DEBUG, "search @Root for "+listType.getTypeName());
					try {
						cls = Class.forName(listType.getTypeName());
						Root rootAnno = cls.getAnnotation(Root.class);
						if (rootAnno==null)
							throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
						name = rootAnno.name();
					} catch (ClassNotFoundException e) {
						throw new SerializationException();
					}
				}
				if (eListAnno.type()!=null)
					cls = eListAnno.type();
			}
			req = eListAnno.required();
		} else if (elUnionAnno!=null) {
			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = field;
				info.required = false;
				info.listInstance = list;
				ret.add(info);

			}
			return ret;
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;
		info.listInstance = list;
		info.isInlineList = inline;
		ret.add(info);

		return ret;

	}


	//-------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForElementListField(Field field) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();

		ElementList eListAnno = field.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = field.getAnnotation(ElementListUnion.class);
		Class<?> cls = field.getType();
		String  name = field.getName();
		boolean req  = false;
		ArrayList<?> list = null;
		boolean inline = false;

		if (eListAnno!=null) {
			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			cls = ArrayList.class;
			if (eListAnno.inline()) {
				list = new ArrayList<>();
				inline = true;
				if (eListAnno.entry().length()>0) {
					name = eListAnno.entry();
				} else {
					java.lang.reflect.Type listType = ((ParameterizedType)field.getGenericType()).getActualTypeArguments()[0];
					logger.log(Level.DEBUG, "search @Root for "+listType.getTypeName());
					try {
						cls = Class.forName(listType.getTypeName());
						Root rootAnno = cls.getAnnotation(Root.class);
						if (rootAnno==null)
							throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
						name = rootAnno.name();
					} catch (ClassNotFoundException e) {
						throw new SerializationException();
					}
				}
				if (eListAnno.type()!=null)
					cls = eListAnno.type();
			}
			req = eListAnno.required();
		} else if (elUnionAnno!=null) {
			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = field;
				info.required = false;
				info.listInstance = list;
				ret.add(info);

			}
			return ret;
		}
		ByNameInfo info = new ByNameInfo();
		info.cls = cls;
		info.elementName = name;
		info.field = field;
		info.required = req;
		info.listInstance = list;
		info.isInlineList = inline;
		ret.add(info);

		return ret;

	}

	//------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForClassAttributes(Class<?> clazz) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();

		for (Field field : Util.getAttributeFields(clazz)) {
			ret.add(determineExpectationForAttributeField(field));
		}
		return ret;

	}

	//------------------------------------------------------------------
	public static List<ByNameInfo> determineExpectationForClassElements(Class<?> clazz) throws SerializationException {
		List<ByNameInfo> ret = new ArrayList<>();

		ElementList eListAnno = (ElementList) clazz.getAnnotation(ElementList.class);
		ElementListUnion elUnionAnno = (ElementListUnion) clazz.getAnnotation(ElementListUnion.class);
		Class<?> cls = null;
		String  name = null;
		ArrayList<?> list = null;

		if (eListAnno!=null) {
			if (!List.class.isAssignableFrom(clazz))
				throw new SerializationException("@ElementList only valid for instances of java.util.List, but "+clazz+" isn't");

			if (elUnionAnno!=null) {
				logger.log(Level.WARNING, "    @ElementList and @ElementLisUnion exclude one another. Prefer @ElementList");
			}
			logger.log(Level.DEBUG, "-----@ElementList for "+clazz);
			cls = ArrayList.class;
			list = new ArrayList<>();
			if (eListAnno.entry().length()>0) {
				name = eListAnno.entry();
			} else {
//				java.lang.reflect.Type listType = ((ParameterizedType)clazz.g.getGenericType()).getActualTypeArguments()[0];
				logger.log(Level.DEBUG, "type params "+Arrays.toString(clazz.getTypeParameters()));
//				try {
//					cls = Class.forName(listType.getTypeName());
//					Root rootAnno = cls.getAnnotation(Root.class);
//					if (rootAnno==null)
//						throw new SerializationException("Missing either @Root for "+listType.getTypeName()+" or @ElementList.entry() for "+field);
//					name = rootAnno.name();
//				} catch (ClassNotFoundException e) {
//					throw new SerializationException();
//				}
			}
			if (eListAnno.type()!=null)
				cls = eListAnno.type();
		} else if (elUnionAnno!=null) {
			if (!List.class.isAssignableFrom(clazz))
				throw new SerializationException("@ElementListUnion only valids for instances of java.util.List, but "+clazz+" isn't");

			list = new ArrayList<>();
			for (ElementList tmp : elUnionAnno.value()) {
				logger.log(Level.DEBUG, "+ "+tmp.entry());
				name = tmp.entry();
				cls = tmp.type();
				ByNameInfo info = new ByNameInfo();
				info.cls = cls;
				info.elementName = name;
				info.field = null;
				info.required = false;
				info.listInstance = list;
				ret.add(info);
			}
		}

		for (Field field : Util.getElementFields(clazz)) {
			logger.log(Level.DEBUG, "    field "+field);
			ret.addAll(determineExpectationForElementField(field));
		}

		return ret;

	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private XMLTreeItem processInlineListStartElement(XMLTreeNode node, StartElement ev, XMLEventReader evRd) throws IOException, SerializationException, InstantiationException, IllegalAccessException {
		logger.log(Level.DEBUG, "  is element of inline list");
		logger.log(Level.DEBUG, "  field = "+node.field);
		if (node.field!=null)
			logger.log(Level.DEBUG, "  list    type = "+node.field.getType());
		logger.log(Level.DEBUG, "  element type = "+node.getType());
		logger.log(Level.DEBUG, "  value = "+node.getInstance());

		/*
		 * Get hold on the list instance.
		 * Act differently for nodes where the inline list is a field
		 * and those where it is a class
		 */
		List list = null;
		if (node.getField()!=null) {
			/*
			 * Make sure there is a valid list instance. Use the one from the field
			 * if it already exists, create a new one otherwise
			 */
			logger.log(Level.DEBUG, "  Search for list in field "+node.getField()+" of instance "+node.getParent().getInstance());
			node.getField().setAccessible(true);
			list = (List) node.getField().get(node.getParent().getInstance());
			if (list!=null) {
				logger.log(Level.DEBUG, "   list already exists ("+list+")");
			} else {
				logger.log(Level.DEBUG, "   list must be created");
				try {
					list = (List) node.getField().getType().getDeclaredConstructor().newInstance();
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed calling empty constructor for "+node.getField().getType(),e);
				}
			}
		} else {
			/*
			 * For inline lists, adding to list is handled on End element
			 */
		}

		/*
		 * Create an instance. Distinguish between normal objects and Enum
		 * values
		 */
		Object instance = null;
		if (node.getType().isEnum()) {
			logger.log(Level.DEBUG, "  Type to create with "+ev+" is an enum");
			try {
				XMLEvent nextEv = evRd.nextEvent();
				String data = nextEv.asCharacters().getData();
				boolean foundEnum = false;
				for (Field enumField : node.getType().getDeclaredFields()) {
					if (enumField.isAnnotationPresent(EnumValue.class)) {
						EnumValue anno = enumField.getAnnotation(EnumValue.class);
						if (anno.value().equals(data)) {
							 logger.log(Level.DEBUG, "    Use "+Enum.valueOf((Class<Enum>) node.getType(), enumField.getName()));
							instance =  Enum.valueOf((Class<Enum>)node.getType(), enumField.getName());
							foundEnum = true;
							break;
						}
					}
				}
				// If not found by @EnumValue, try default Enum.valueOf
				if (!foundEnum) {
//					logger.log(Level.WARNING, "  TODO: check for @EnumValue here");
					instance = Enum.valueOf((Class<Enum>)node.getType(), data);
				}
			} catch (IllegalArgumentException e) {
				logger.log(Level.ERROR, "In node "+ev+": "+e.getMessage());
				throw new SerializationException("In node "+ev+": "+e.getMessage(),e);
			} catch (Exception e) {
				logger.log(Level.ERROR, "  Expected a CHARACTER event after "+ev,e);
				throw new SerializationException("Expected a CHARACTER event after "+ev+" / "+node);
			}
		} else {
			logger.log(Level.DEBUG, "  create "+node.getType()+" and add it to list "+list);
			try {
				instance = node.getType().getDeclaredConstructor().newInstance();
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed calling empty constructor for "+node.getType(),e);
			}
		}

		node.setInstance(instance);
		if (list!=null)
			list.add(instance);

		/*
		 * Attributes
		 * First reset all, to be able later to find those that are
		 * required and missing
		 */
		parseAttributes(node, ev);

		return node;
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void parseAttributes(XMLTreeNode node, StartElement ev) throws SerializationException, IllegalArgumentException, IllegalAccessException {
		/*
		 * Build a list of all attributes that must be set
		 */
		List<String> required = new ArrayList<>();
		// Reset
		for (XMLTreeItem check : node.getChildren()) {
			if (check instanceof XMLTreeAttributeLeaf) {
				if (((XMLTreeAttributeLeaf)check).isRequired())
				required.add( ((XMLTreeAttributeLeaf)check).getName() );
			}
		}

		// Now parse
		Iterator<javax.xml.stream.events.Attribute> iter = ev.getAttributes();
		while( iter.hasNext() ) {
			javax.xml.stream.events.Attribute a = iter.next();
			logger.log(Level.DEBUG, "   Attr "+a);
			String name = a.getName().getLocalPart();
			String sVal = a.getValue();
			Object toSet = null;
			// Not required anymore
			required.remove(name);

			// Find expected attribute by name
			XMLTreeItem foo = node.getChild(name);
			if (foo==null) {
				logger.log(Level.WARNING, "Unknown attribute '"+name+"' in "+ev+"\nCurrent node is "+node);
				continue;
			}
			if (!(foo instanceof XMLTreeAttributeLeaf)) {
				logger.log(Level.WARNING, "Attribute '"+name+"' in "+ev+" should be an XML element");
				continue;
			}
			XMLTreeAttributeLeaf leaf = (XMLTreeAttributeLeaf)foo;

			/*
			 * Use a converter, if it exists
			 */
			if (leaf.getValueConverter()!=null) {
				logger.log(Level.DEBUG, "  call converter "+leaf.getValueConverter()+" for attribute '"+name+"'");
				try {
					toSet = leaf.getValueConverter().read(sVal);
				} catch (Exception e) {
					logger.log(Level.ERROR, "Failed calling converter "+leaf.getValueConverter()+" with '"+sVal+"'",e);
					logger.log(Level.ERROR, "leaf was "+leaf.getName()+"/"+leaf.getType()+"/"+leaf.getField());
					logger.log(Level.ERROR, "Element was "+ev,e);
					throw new SerializationException("Failed calling converter "+leaf.getValueConverter()+" with '"+sVal+"': "+e);
				}
			} else {
				/*
				 * Do implicit conversions, if necessary
				 */
//				logger.log(Level.DEBUG, "  implicit convert "+sVal+" for attribute '"+name+"'");
				if (String.class==leaf.getType()) {
					// No conversion necessary
					toSet = sVal;
				} else if (int.class==leaf.getType() || Integer.class==leaf.getType()) {
					toSet = Integer.parseInt(sVal);
				} else if (double.class==leaf.getType() || Double.class==leaf.getType()) {
					toSet = Double.parseDouble(sVal);
				} else if (float.class==leaf.getType() || Float.class==leaf.getType()) {
					toSet = Float.parseFloat(sVal);
				} else if (boolean.class==leaf.getType() || Boolean.class==leaf.getType()) {
					toSet = Boolean.parseBoolean(sVal);
					if ("y".equalsIgnoreCase(sVal) || "yes".equalsIgnoreCase(sVal) || "1".equals(sVal))
						toSet = true;
//					logger.log(Level.ERROR, "Converting "+sVal+" to Boolean resulsts in "+toSet);
//					System.exit(0);
				} else if (UUID.class==leaf.getType()) {
					toSet = UUID.fromString(sVal);
				} else if (Date.class==leaf.getType()) {
					try {
						toSet = FORMAT.parse(sVal);
					} catch (ParseException e) {
						try {
							toSet = FORMAT2.parse(sVal);
						} catch (ParseException ee) {
							try {
								toSet = FORMAT3.parse(sVal);
							} catch (ParseException eee) {
								throw new SerializationException("Cannot parse data in "+ev,e);
							}
						}
					}
				} else if (leaf.getType().isEnum()) {
					// Enum found
					/*
					 * If there is no @EnumValue annotation, just try the Enum.valueOf method
					 */
					 Field[] flds = leaf.getType().getDeclaredFields();
					 boolean foundEnum = false;
					 for (Field f : flds) {
						 if (f.isEnumConstant() && f.isAnnotationPresent(EnumValue.class)) {
							 EnumValue enumAnno = f.getAnnotation(EnumValue.class);
							 logger.log(Level.DEBUG, "   Found @EnumValue annotation "+enumAnno);
							 if (enumAnno.value().equals(sVal)) {
								 logger.log(Level.DEBUG, "   Found enum by @EnumValue "+Enum.valueOf((Class<Enum>) leaf.getType(), f.getName()));
								 toSet = Enum.valueOf((Class<Enum>) leaf.getType(), f.getName());
							 }
							 foundEnum = true;
							 break;
						 }
					 }

					 if (!foundEnum) {
						 try {
							 Method valueOf = leaf.getType().getMethod("valueOf", String.class);
							 toSet = valueOf.invoke(null, sVal);
							 logger.log(Level.DEBUG, "  Found Enum by value: "+toSet);
						 } catch (NoSuchMethodException | SecurityException
								 | InvocationTargetException e) {
							 logger.log(Level.ERROR, "Failed converting Enum value: "+e);
							 throw new SerializationException("Illegal value '"+sVal+"' for enum constant in attribute "+leaf.getName()+" of "+leaf.getParent().getName(),e);
						 }
					 }
				} else
					throw new SerializationException("Don't know how to convert String '"+name+"' to "+leaf.getType()+" in "+ev);
			}

			/*
			 * Set the final value
			 */
			Field field = leaf.getField();
			logger.log(Level.DEBUG, "   set '"+toSet+"' in field "+field+" of instance "+node.getInstance());
			field.setAccessible(true);
			field.set(node.getInstance(), toSet);
		}

		/*
		 * Check if all required attributes are set
		 */
		if (!required.isEmpty()) {
			throw new SerializationException("Missing attributes "+required+" in "+ev);
		}

	}

	//-------------------------------------------------------------------
	XMLTreeItem processStartElement(XMLTreeNode current, StartElement ev, XMLEventReader evRd) throws IOException, SerializationException, InstantiationException, IllegalAccessException {
		logger.log(Level.DEBUG, "START "+ev);
		logger.log(Level.TRACE, "  expected = "+current.dump());
//		logger.log(Level.DEBUG, "  allowed/expected are: "+current.expectedElements);

		String name = ev.getName().getLocalPart();
		logger.log(Level.DEBUG, "  name = "+name);

		XMLTreeItem item = current.getChild(name);
		if (item==null) {
			logger.log(Level.WARNING, "  No element '"+name+"' expected in "+current);
			logger.log(Level.WARNING, "  allowed/expected are: "+current.dump());
			logger.log(Level.WARNING, "  Event was "+ev);
			throw new SerializationException("No element '"+name+"' expected in "+current+" (child of "+current.getParent()+")");
		}
		logger.log(Level.DEBUG, "  Found "+item);
		if (item instanceof XMLTreeNode) {
			XMLTreeNode node = (XMLTreeNode)item;
			if (node.isInlineList()) {
				return processInlineListStartElement(node, ev, evRd);
			}

			/*
			 * Create an instance. Do this by instantiating the class stored in the current node
			 * or - if present - by calling the converter
			 */
			Object instance = null;
			if (node.getElementConverter()!=null) {
				logger.log(Level.DEBUG, "  Converter found: "+node.getElementConverter());
				try {
					instance = node.getElementConverter().read(node, ev, evRd);
					logger.log(Level.DEBUG, "  converter returned: "+instance+" which will be written later (eventually to field "+item.getField()+" )");
					node.setInstance(instance);
					logger.log(Level.DEBUG, "  instance = "+node.getInstance());
					return node;
				} catch (Exception e) {
					throw new SerializationException("Failed calling converter",e);
				}
			} else {
				logger.log(Level.DEBUG, "  create "+node.getType()+" to be written later (eventually to field "+item.getField()+" )");
				try {
					instance = node.getType().getDeclaredConstructor().newInstance();
					if (instance==null)
						throw new NullPointerException("Creating instance of "+node.getType()+" returned Null");
				} catch (Exception e) {
					logger.log(Level.ERROR, node.dumpData());
					logger.log(Level.ERROR, "Failed instantiating "+node.getType()+" with empty constructor: "+e);
					throw new SerializationException("Failed instantiating "+node.getType()+" with empty constructor",e);
				}
			}
			node.setInstance(instance);
			logger.log(Level.DEBUG, "  instance = "+node.getInstance());

			if (node.isMapEntryValue()) {
				logger.log(Level.DEBUG, "  is map entry value");
				return node;
			}

			/*
			 * Attributes
			 * First reset all, to be able later to find those that are
			 * required and missing
			 */
			try {
				parseAttributes(node, ev);
			} catch (Exception e) {
				logger.log(Level.ERROR, "Failed parsing attributes of "+ev+": ",e);
			}

			logger.log(Level.DEBUG, "  instance = "+node.getInstance());
			logger.log(Level.TRACE, "Return "+node.dumpData());
			return node;
		} else if (item instanceof XMLTextLeaf) {
			return item;
		} else if (item instanceof XMLBinaryLeaf) {
			return item;
		} else {
			logger.log(Level.WARNING, "Don't know how to deal with "+item);
			System.exit(0);
		}

		return null;
	}


	//-------------------------------------------------------------------
	public static String getClassName(java.lang.reflect.Type type) {
		String TYPE_NAME_PREFIX = "class ";
	    if (type==null) {
	        return "";
	    }
	    String className = type.toString();
	    if (className.startsWith(TYPE_NAME_PREFIX)) {
	        className = className.substring(TYPE_NAME_PREFIX.length());
	    }
	    return className;
	}

	//-------------------------------------------------------------------
	public static Class<?> getClass(java.lang.reflect.Type type) {
	    String className = getClassName(type);
	    if (className==null || className.isEmpty()) {
	        return null;
	    }
	    try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			return null;
		}
	}
}
