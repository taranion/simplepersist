/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

import java.lang.reflect.Field;

/**
 * @author prelle
 *
 */
public abstract class XMLTreeItem {
	
	protected XMLTreeNode parent;
	protected Field field;

	//-------------------------------------------------------------------
	/**
	 * @return the parent
	 */
	public XMLTreeNode getParent() {
		return parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @param parent the parent to set
	 */
	void setParent(XMLTreeNode parent) {
		this.parent = parent;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the field
	 */
	public Field getField() {
		return field;
	}

	//-------------------------------------------------------------------
	/**
	 * @param field the field to set
	 */
	public void setField(Field field) {
		this.field = field;
	}

}
