/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

import org.prelle.simplepersist.StringValueConverter;

/**
 * @author prelle
 *
 */
public class XMLTreeAttributeLeaf extends XMLTreeItem {

	private String name;
	private Class<?> cls;
	private boolean required;
	private StringValueConverter<?> converter;
	
	//-------------------------------------------------------------------
	public XMLTreeAttributeLeaf(String name, Class<?> cls) {
		this.name = name;
		this.cls  = cls;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	//-------------------------------------------------------------------
	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the converter
	 */
	public StringValueConverter<?> getValueConverter() {
		return converter;
	}

	//-------------------------------------------------------------------
	/**
	 * @param converter the converter to set
	 */
	public void setValueConverter(StringValueConverter<?> converter) {
		this.converter = converter;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the cls
	 */
	public Class<?> getType() {
		return cls;
	}

}
