package org.prelle.simplepersist.unmarshal;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class ByNameInfo {
	public String elementName;
	public Class<?> cls;
	public boolean required;
	public Field field;
	public Object fieldInstance;
	public ArrayList<?> listInstance;
	public boolean isInlineList=false;
	/**
	 * When the start tag is read, this field contains a instance of the type declared in cls.
	 * When the end tag is read, this contains an instance ready to be put in 'field'
	 */
	Object value;
	//-------------------------------------------------------------------
	public boolean isList() {
		if (field==null) return false;
		return List.class.isAssignableFrom(field.getType());
	}
	//-------------------------------------------------------------------
	public String toString() {
		return String.valueOf(cls);
//		if (field==null)
//			return "type'"+cls.getSimpleName()+"' in instance "+fieldInstance;
//		return "field '"+field.getName()+"' of type '"+cls.getSimpleName()+"' in instance "+fieldInstance;
	}
	//-------------------------------------------------------------------
	public String dump() {
		StringBuffer buf = new StringBuffer();
		buf.append("\n        a "+(required?"required":"optional")+" object of type "+cls);
		if (isList())
			buf.append("\n        to add to list in field "+field+" of instance "+fieldInstance);
		else
			buf.append("\n        to set in field "+field+" of instance "+fieldInstance);
		buf.append("\n        The current value to set is "+value);
		return buf.toString();
	}
	
}