/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.XMLElementConverter;

/**
 * @author prelle
 *
 */
public class XMLTreeNode extends XMLTreeItem {
	
	/**
	 * The element name
	 */
	private String name;
	private Class<?> cls;
	private List<XMLTreeItem> children;
	private boolean required;
	
	private boolean inlineList;
	private boolean isMapEntry;
	private boolean isMapEntryValue;
	private XMLElementConverter<?> elementConverter;
	
	/**
	 * This is the instance, to which all children reference with their fields
	 * Not set by TreeBuilder, but set upon parsing document
	 */
	private Object instance;

	//-------------------------------------------------------------------
	/**
	 */
	public XMLTreeNode(String name, Class<?> cls) {
		this.name = name;
		this.cls = cls;
		children = new ArrayList<>();
	}

	//-------------------------------------------------------------------
	public String toString() {
		return getClass().getSimpleName()+"("+name+"="+instance+")";
	}

	//-------------------------------------------------------------------
	public String dumpData() {
		StringBuffer buf = new StringBuffer();
		buf.append("\n  type     = "+cls);
		buf.append("\n  name     = "+name);
		buf.append("\n  required = "+required);
		buf.append("\n  instance = "+instance);
		buf.append("\n  children = "+children);
		buf.append("\n  inlineLi = "+isInlineList());
		buf.append("\n  mapEntry = "+isMapEntry);
		buf.append("\n  mapEnVal = "+isMapEntryValue);
		buf.append("\n  convert  = "+elementConverter);
		return buf.toString();
	}

	//-------------------------------------------------------------------
	String dump(int indent) {
		StringBuffer buf = new StringBuffer();
		buf.append("\n");
		for (int i=0; i<indent; i++)
			buf.append("  ");
		
		if (cls!=null)
			buf.append("<"+name+":"+cls.getSimpleName());
		else
			buf.append("<"+name+":null");

		if (elementConverter!=null)
			buf.append(" conv="+elementConverter);
		
		if (isMapEntry)
			buf.append(" for entry");
		else if (isMapEntryValue)
			buf.append(" as value");
		else if (field!=null)
			buf.append(" in list "+field.getType());

		
		StringBuffer elements = new StringBuffer();
		for (XMLTreeItem item : children) {
			if (item instanceof XMLTreeAttributeLeaf) {
				buf.append(" "+((XMLTreeAttributeLeaf)item).getName());
			} else if (item instanceof XMLTextLeaf) {
				elements.append( ((XMLTextLeaf)item).dump(indent+2) );
			} else if (item instanceof XMLTreeNode) {
				elements.append( ((XMLTreeNode)item).dump(indent+2) );
			} else
				buf.append("\n* "+item.getClass());
		}
		buf.append(">");
		buf.append(elements);
		buf.append("\n");
		for (int i=0; i<indent; i++)
			buf.append("  ");
		buf.append("</"+name+">");
		
		return buf.toString();
	}

	//-------------------------------------------------------------------
	public String dump() {
		return dump(0);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	public void addChild(XMLTreeItem item) {
		if (!children.contains(item)) {
			children.add(item);
			item.setParent(this);
		}
	}

	//-------------------------------------------------------------------
	public List<XMLTreeItem> getChildren() {
		return children;
	}

	//-------------------------------------------------------------------
	public XMLTreeItem getChild(String name) {
		for (XMLTreeItem item : children) {
			if (item instanceof XMLTreeAttributeLeaf && ((XMLTreeAttributeLeaf)item).getName().equals(name))
				return item;
			if (item instanceof XMLTextLeaf && ((XMLTextLeaf)item).getName().equals(name))
				return item;
			if (item instanceof XMLTreeNode && ((XMLTreeNode)item).getName().equals(name))
				return item;
			if (item instanceof XMLBinaryLeaf && ((XMLBinaryLeaf)item).getName().equals(name))
				return item;
		}
		return null;	
	}

	//-------------------------------------------------------------------
	public Class<?> getType() {
		return cls;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the required
	 */
	public boolean isRequired() {
		return required;
	}

	//-------------------------------------------------------------------
	/**
	 * @param required the required to set
	 */
	public void setRequired(boolean required) {
		this.required = required;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the instance
	 */
	public Object getInstance() {
		return instance;
	}

	//-------------------------------------------------------------------
	/**
	 * @param instance the instance to set
	 */
	public void setInstance(Object instance) {
		this.instance = instance;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the inlineList
	 */
	public boolean isInlineList() {
		return inlineList;
	}

	//-------------------------------------------------------------------
	/**
	 * @param inlineList the inlineList to set
	 */
	public void setInlineList(boolean inlineList) {
		this.inlineList = inlineList;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the isMapEntry
	 */
	public boolean isMapEntry() {
		return isMapEntry;
	}

	//-------------------------------------------------------------------
	/**
	 * @param isMapEntry the isMapEntry to set
	 */
	public void setMapEntry(boolean isMapEntry) {
		this.isMapEntry = isMapEntry;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the isMapEntryValue
	 */
	public boolean isMapEntryValue() {
		return isMapEntryValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @param isMapEntryValue the isMapEntryValue to set
	 */
	public void setMapEntryValue(boolean isMapEntryValue) {
		this.isMapEntryValue = isMapEntryValue;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the elementConverter
	 */
	public XMLElementConverter<?> getElementConverter() {
		return elementConverter;
	}

	//-------------------------------------------------------------------
	/**
	 * @param elementConverter the elementConverter to set
	 */
	public void setElementConverter(XMLElementConverter<?> elementConverter) {
		this.elementConverter = elementConverter;
	}

}
