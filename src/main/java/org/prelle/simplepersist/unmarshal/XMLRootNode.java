/**
 * 
 */
package org.prelle.simplepersist.unmarshal;

/**
 * @author prelle
 *
 */
public class XMLRootNode extends XMLTreeNode {

	//-------------------------------------------------------------------
	/**
	 */
	public XMLRootNode() {
		super(null, void.class);
	}

	//-------------------------------------------------------------------
	String dump(int indent) {
		StringBuffer buf = new StringBuffer();
		buf.append("\n");
		for (int i=0; i<indent; i++)
			buf.append("  ");
		
		buf.append("ROOT");
		buf.append(super.dump(indent));
		
		return buf.toString();
	}

}
