/**
 *
 */
package org.prelle.simplepersist.unmarshal;

import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.Element;
import org.prelle.simplepersist.ElementConvert;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.simplepersist.MapConvert;
import org.prelle.simplepersist.MapEntry;
import org.prelle.simplepersist.Root;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.Util;
import org.prelle.simplepersist.XMLElementConverter;

/**
 * @author prelle
 *
 */
public class TreeBuilder {

	private final static int RECURSE_DEPTH = 1;
	
	private final static Logger logger = System.getLogger("xml");

	//-------------------------------------------------------------------
	public static XMLTreeNode convertToTree(Class<?> clazz) throws SerializationException {
		return convertToTree(clazz,0, new ArrayList<>());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("serial")
	public static XMLTreeNode convertToTree(Class<?> clazz, int depth, List<Class<?>> parents) throws SerializationException {
		Root aRoot = clazz.getAnnotation(Root.class);
		if (aRoot==null)
			throw new SerializationException("Missing @Root in "+clazz);

		XMLTreeNode root = new XMLTreeNode(aRoot.name(), clazz);
		fillAttributes(clazz, root);
		if (Collections.frequency(parents, clazz)>RECURSE_DEPTH) {
			logger.log(Level.ERROR, "Stop at depth "+depth+" for "+clazz);
			return root;
		}
		fillElements(clazz, root, depth, parents);
		List<Class<?>> newParents = new ArrayList<Class<?>>() { { addAll(parents); add(clazz); }};


		ElementList anno = clazz.getAnnotation(ElementList.class);
		ElementListUnion unionAnno = clazz.getAnnotation(ElementListUnion.class);
		/*
		 * Class may be an inline list
		 */
		// Now consult annotation
		if (anno!=null) {
			// Create child element for item class
			XMLTreeNode item = (anno.entry().length()>0)
					?
						convertToTree(anno.type(), anno.entry(), depth+1, newParents)
					:
						convertToTree(anno.type(), depth+1, newParents);
			if (anno.entry().length()>0)
				item.setName(anno.entry());
			item.setRequired(anno.required());
			if (anno.convert()!=ElementConvert.NOELEMENTCONVERTER.class)
				item.setElementConverter(Util.createElementConverter(anno.convert()));
//			item.setListField(field);

			root.addChild(item);
		}

		/*
		 * Class may be an inline list union
		 */
		else if (unionAnno!=null) {
			// Create child element for item class
			for (ElementList anno2 : unionAnno.value()) {
				XMLTreeNode item = convertToTree(anno2.type(), anno2.entry(), depth+1, newParents);
				if (anno2.entry().length()>0)
					item.setName(anno2.entry());
				item.setRequired(anno2.required());
//				item.setField(field);
				item.setInlineList(true);

				root.addChild(item);
			}
		}

		return root;
	}

	//-------------------------------------------------------------------
	public static XMLTreeNode convertToTree(Class<?> clazz, String name) throws SerializationException {
		return convertToTree(clazz,name,0, new ArrayList<>());
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("serial")
	public static XMLTreeNode convertToTree(Class<?> clazz, String name, int depth, List<Class<?>> parents) throws SerializationException {
		XMLTreeNode root = new XMLTreeNode(name, clazz);
		fillAttributes(clazz, root);
		// If the parents already contain this element 2 times or more
		if (Collections.frequency(parents, clazz)>(RECURSE_DEPTH-1)) {
			logger.log(Level.DEBUG, "Stop at depth "+depth+" for "+clazz);
			return root;
		}
		fillElements(clazz, root, depth, parents);

		ElementList anno = clazz.getAnnotation(ElementList.class);
		ElementListUnion unionAnno = clazz.getAnnotation(ElementListUnion.class);
		/*
		 * Class may be an inline list
		 */
		List<Class<?>> newParents = new ArrayList<Class<?>>() { { addAll(parents); add(clazz); }};
		// Now consult annotation
		if (anno!=null) {
			// Create child element for item class
			XMLTreeNode item = (anno.entry().length()>0)
					?convertToTree(anno.type(), anno.entry(), depth+1, newParents):convertToTree(anno.type(), depth+1, newParents);
			if (anno.entry().length()>0)
				item.setName(anno.entry());
			item.setRequired(anno.required());
//			item.setListField(field);

			root.addChild(item);
		}

		/*
		 * Class may be an inline list union
		 */
		else if (unionAnno!=null) {
			// Create child element for item class
			for (ElementList anno2 : unionAnno.value()) {
				XMLTreeNode item = convertToTree(anno2.type(), anno2.entry(), depth+1, newParents);
				if (anno2.entry().length()>0)
					item.setName(anno2.entry());
				item.setRequired(anno2.required());
//				item.setField(field);
				item.setInlineList(true);

				root.addChild(item);
			}
		}

		return root;
	}

	//-------------------------------------------------------------------
	private static void fillAttributes(Class<?> clazz, XMLTreeNode element) throws SerializationException {
		for (Field field : Util.getAttributeFields(clazz)) {
			Attribute attrAnno = field.getAnnotation(Attribute.class);
			Class<?> cls = field.getType();
			String  name = field.getName();
			boolean req  = false;

			if (attrAnno!=null) {
				if (attrAnno.name().length()>0)
					name = attrAnno.name();
				req = attrAnno.required();
			}

			XMLTreeAttributeLeaf leaf = new XMLTreeAttributeLeaf(name, cls);
			leaf.setRequired(req);
			leaf.setField(field);

			/*
			 * Check if a converter is required
			 */
			AttribConvert convAnno = field.getAnnotation(AttribConvert.class);
			if (convAnno!=null) {
				StringValueConverter<?> converter = Util.createAttribConverter(convAnno.value());
				leaf.setValueConverter(converter);
			}
			element.addChild(leaf);
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("serial")
	private static void fillElements(Class<?> clazz, XMLTreeNode element, int depth, List<Class<?>> parents) throws SerializationException {
		List<Class<?>> newParents = new ArrayList<Class<?>>() { { addAll(parents); add(clazz); }};
		for (Field field : Util.getElementFields(clazz)) {
			if (field.isAnnotationPresent(Element.class))
				fillElement(field, element, depth+1, newParents);
			else if (field.isAnnotationPresent(ElementList.class)) {
				if (field.getAnnotation(ElementList.class).inline()) {
					fillInlineElementList(field, element, depth+1, newParents);
				} else {
					fillNonInlineElementList(field, element, depth+1, newParents);
				}
			} else if (field.isAnnotationPresent(ElementListUnion.class))
				fillElementListUnion(field, element, depth+1, newParents);
		}
	}

	//-------------------------------------------------------------------
	private static void fillElement(Field field, XMLTreeNode element, int depth, List<Class<?>> newParents) throws SerializationException {
		// Default values
		String  name = field.getName();
		Class<?> cls = field.getType();

		// Now consult annotation
		Element anno = field.getAnnotation(Element.class);
		ElementConvert elemConvAnno = field.getAnnotation(ElementConvert.class);
		if (anno.name().length()>0)
			name = anno.name();

		// Start by creating the node representing the child
		if (cls==String.class) {
			XMLTextLeaf next = new XMLTextLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else if (cls==int.class || cls==Integer.class) {
			XMLTextLeaf next = new XMLTextLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else if (cls==boolean.class || cls==Boolean.class) {
			XMLTextLeaf next = new XMLTextLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else if (cls==double.class || cls==Double.class) {
			XMLTextLeaf next = new XMLTextLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else if (cls==byte[].class) {
			XMLBinaryLeaf next = new XMLBinaryLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else if (cls==Map.class) {
			XMLTreeNode next = new XMLTreeNode(name, HashMap.class);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
			XMLTreeNode element2 = new XMLTreeNode("element", MapEntry.class);
			element2.setMapEntry(true);
			fillMap((ParameterizedType) field.getGenericType(), element2, field.getAnnotation(MapConvert.class), depth, newParents);
			next.addChild(element2);
		} else if (cls.isEnum()) {
			XMLTextLeaf next = new XMLTextLeaf(name, cls);
			next.setRequired(anno.required());
			next.setField(field);
			element.addChild(next);
		} else {
			XMLTreeNode next = convertToTree(cls,name, depth+1, newParents);
			next.setName(name);
			next.setField(field);
			if (elemConvAnno!=null)
				try {
					next.setElementConverter(elemConvAnno.value().getDeclaredConstructor().newInstance());
				} catch (Exception e) {
					throw new SerializationException("Failed instantiating converter",e);
				}
			element.addChild(next);
		}
	}

	//-------------------------------------------------------------------
	@SuppressWarnings("rawtypes")
	private static void fillNonInlineElementList(Field field, XMLTreeNode element, int depth, List<Class<?>> newParents) throws SerializationException {
//		logger.log(Level.DEBUG, "fillNonInlineElementList("+field+",  "+element+")");
		// Default values
		String  name = field.getName();
		Class<?> cls = field.getType();
		if (List.class==cls)
			cls = ArrayList.class;

		XMLTreeNode next = new XMLTreeNode(name, cls);
		next.setField(field);
		try {
			next.setInstance(cls.getDeclaredConstructor().newInstance());
		} catch (Exception e) {
			throw new SerializationException("Failed initializing "+cls,e);
		}

		element.addChild(next);

		// Now consult annotation
		ElementList anno = field.getAnnotation(ElementList.class);
		next.setRequired(anno.required());

		if (anno.type()==String.class) {
			XMLTextLeaf next2 = new XMLTextLeaf(anno.entry(), anno.type());
			next2.setRequired(anno.required());
			next2.setField(field);
			next.addChild(next2);
			return;
		}

		// Create child element for item class
		XMLTreeNode item = (anno.entry().length()>0)?convertToTree(anno.type(), anno.entry(), depth+1, newParents):convertToTree(anno.type(), depth+1, newParents);
		if (anno.entry().length()>0)
			item.setName(anno.entry());

		/*
		 * Check for converters
		 */
		Class<? extends XMLElementConverter<?>> convClass = field.getAnnotation(ElementList.class).convert();
		XMLElementConverter converter = null;
		if (convClass!=ElementConvert.NOELEMENTCONVERTER.class) {
			try {
				converter = Util.createElementConverter(convClass);
				item.setElementConverter(converter);
			} catch (Exception e) {
				throw new SerializationException("Cannot instantiate converter "+convClass,e);
			}
		}


		next.addChild(item);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void fillInlineElementList(Field field, XMLTreeNode element, int depth, List<Class<?>> newParents) throws SerializationException {
		// Default values
		Class<? extends List> cls = (Class<? extends List>) field.getType();
		if (List.class==cls)
			cls = ArrayList.class;

		// Now consult annotation
		ElementList anno = field.getAnnotation(ElementList.class);

		// Create child element for item class
		XMLTreeNode item = (anno.entry().length()>0)
				?convertToTree(anno.type(), anno.entry(), depth+1, newParents):convertToTree(anno.type(), depth+1, newParents);

		if (anno.entry().length()>0)
			item.setName(anno.entry());
		item.setField(field);
		item.setRequired(anno.required());
		item.setInlineList(true);

		element.addChild(item);
	}

	//-------------------------------------------------------------------
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static void fillElementListUnion(Field field, XMLTreeNode element, int depth, List<Class<?>> newParents) throws SerializationException {
		// Default values
		Class<? extends List> cls = (Class<? extends List>) field.getType();
		if (List.class==cls)
			cls = ArrayList.class;

		// Now consult annotation
		ElementListUnion unionAnno = field.getAnnotation(ElementListUnion.class);

		// Create child element for each item class
		for (ElementList anno : unionAnno.value()) {
			XMLTreeNode item = convertToTree(anno.type(), anno.entry(), depth+1, newParents);
			if (anno.entry().length()>0)
				item.setName(anno.entry());
			item.setRequired(anno.required());
			item.setField(field);
			item.setInlineList(true);

			element.addChild(item);
		}
	}

	//-------------------------------------------------------------------
	private static void fillMap(ParameterizedType pType, XMLTreeNode element, MapConvert convertAnno, int depth, List<Class<?>> newParents) throws SerializationException {
		logger.log(Level.DEBUG, "  type params = "+Arrays.toString(pType.getActualTypeArguments()));
		logger.log(Level.DEBUG, "  conv = "+convertAnno);

		try {
			StringValueConverter<?> keyConv = null;
			XMLElementConverter<?>  valConv = null;
			if (convertAnno!=null && convertAnno.keyConvert()!=AttribConvert.NOVALUECONVERTER.class)
				keyConv = Util.createAttribConverter(convertAnno.keyConvert());
			if (convertAnno!=null && convertAnno.valConvert()!=ElementConvert.NOELEMENTCONVERTER.class)
				valConv = Util.createElementConverter(convertAnno.valConvert());

			Class<?> keyClass = Class.forName(pType.getActualTypeArguments()[0].getTypeName());
			Class<?> valClass = Class.forName(pType.getActualTypeArguments()[1].getTypeName());

			/*
			 * Key
			 */
			String name = "key";
			Class<?> cls= keyClass;

			if (keyClass!=String.class && cls!=int.class && cls!=Integer.class && keyConv==null) {
				logger.log(Level.ERROR, "Don't know how to handle map keys of type "+cls+" and a converter is not defined");
				throw new SerializationException("Don't know how to handle map keys of type "+cls+" and a converter is not defined");
			}

			XMLTextLeaf nextKey = new XMLTextLeaf(name, cls);
			nextKey.setRequired(true);
			nextKey.setField(MapEntry.class.getDeclaredField("key"));
			nextKey.setConverter(keyConv);
			element.addChild(nextKey);

			/*
			 * Key
			 */
			name = "value";
			cls  = valClass;
			XMLTreeNode next = new XMLTreeNode(name, cls);
			next.setRequired(true);
			next.setField(MapEntry.class.getDeclaredField("value"));
			next.setMapEntryValue(true);
			element.addChild(next);
			next.addChild(new XMLTreeAttributeLeaf("type", String.class));

			if (valConv==null) {
				logger.log(Level.DEBUG, "  call convertToTree for "+cls);
				XMLTreeNode foo = convertToTree(cls, depth+1, newParents);
				next.addChild(foo);
			} else {
				next.setElementConverter(valConv);
			}

		} catch (ClassNotFoundException | NoSuchFieldException | SecurityException e) {
			logger.log(Level.ERROR, e.toString());
			throw new SerializationException(e);
		}


	}

}
