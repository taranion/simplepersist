package org.prelle.simplepersist.unmarshal;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.AccessMethod;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.XMLElementConverter;

public class ParseNode {
	public Field parentField;
	public Object parentObj;
	
	public StartElement event;
	public Class<?> type;
	public StringValueConverter<?> attribConverter;
	public XMLElementConverter<?> elementConverter;
	public Object value;
	public AccessMethod access;
	public Method method;
	public StringBuffer text = new StringBuffer();
	public boolean isList;
	public boolean isInline;
	
	public String toString() {
		if (parentObj==null)
			return String.format("Root (type=%s, value=%s)", String.valueOf(type), String.valueOf(value));
		if (isList) {
			if (isInline)
				return String.format("InlineList (type=%s, value=%s, parent=%s, parentField=%s)", String.valueOf(type), String.valueOf(value), String.valueOf(parentObj), String.valueOf(parentField));
			return String.format("List (type=%s, value=%s, parent=%s, parentField=%s)", String.valueOf(type), String.valueOf(value), String.valueOf(parentObj), String.valueOf(parentField));
		}
		return String.format("Element (type=%s, value=%s, parent=%s, parentField=%s)", String.valueOf(type), String.valueOf(value), String.valueOf(parentObj), String.valueOf(parentField));
	}
}