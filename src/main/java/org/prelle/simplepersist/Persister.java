/**
 *
 */
package org.prelle.simplepersist;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.lang.System.Logger;
import java.lang.System.Logger.Level;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;

import javax.xml.catalog.CatalogResolver;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLResolver;
import javax.xml.stream.XMLStreamException;
import javax.xml.transform.Source;

import org.prelle.simplepersist.marshaller.Document;
import org.prelle.simplepersist.marshaller.Marshaller;
import org.prelle.simplepersist.unmarshal2.Unmarshaller3;
import org.w3c.dom.ls.LSInput;
import org.xml.sax.InputSource;

/**
 * @author prelle
 *
 */
public class Persister implements Serializer {

    private final static Logger logger = System.getLogger("xml");

	public final static String PREFIX_KEY_INTERFACE = "org.prelle.simplepersist.interfaceconverter";
	public final static String PREFIX_KEY_ABSTRACT  = "org.prelle.simplepersist.abstractconverter";
	public final static Map<Class<?>,BiFunction<Class<?>,String,Class<?>>> INTERFACE_RESOLVER = new HashMap<>();

	private static Map<String, Object> keys = new HashMap<>();
	private Marshaller marshaller;
	private DeSerializer unmarshaller;

	public static boolean errorFound=false;

	//-------------------------------------------------------------------
	public Persister() {
		marshaller = new Marshaller();
		unmarshaller = new Unmarshaller3();
	}

	//--------------------------------------------------------------------
	public static void putContext(String key, Object data) {
		keys.put(key, data);
	}

	//--------------------------------------------------------------------
	public static Object getKey(String key) {
		return keys.get(key);
	}

    //--------------------------------------------------------------------
	public static Optional<Class<?>> resolveInterfaceWithValue(Class<?> ifClass, String strValue) {
	    logger.log(Level.WARNING, "resolveInterfaceWithValue("+ifClass+", "+strValue+")");
//	    if (ifClass.isEnum()) {
//	        return Enum.valueOf(ifClass, strValue);
//	    }
	    
	    BiFunction<Class<?>,String,Class<?>> resolver = INTERFACE_RESOLVER.get(ifClass);
	    if (resolver!=null) {
	        return Optional.ofNullable( resolver.apply(ifClass, strValue) );
	    }
	    //throw new IllegalArgumentException("Cannot resolve '"+strValue+"' for type "+ifClass);
	    return Optional.empty();
	}

	//--------------------------------------------------------------------
	public static Class<?> getRealClass(Class<?> type) {
	    Class<?> ret = null;
//	    if (type.isInterface()) {
//	        resolveInterfaceWithValue(type, "?");
//	        String key = PREFIX_KEY_INTERFACE+"."+type.getName();
//	        ret = (Class<?>) keys.get(key);
//            throw new RuntimeException("Missing instructions how to create an instance abstract class '"+type+"'\n\nAdd\nPersister.putContext(Persister.PREFIX_KEY_ABSTRACT+\".\"+"+type.getSimpleName()+".class.getName(), <your class that extends it>);\nto your code");
//	    } else {
	        String key = PREFIX_KEY_ABSTRACT+"."+type.getName();
	        ret = (Class<?>) keys.get(key);
//	    }
            
		if (ret==null)
			throw new RuntimeException("Missing instructions how to create an instance abstract class '"+type+"'\n\nAdd\nPersister.putContext(Persister.PREFIX_KEY_ABSTRACT+\".\"+"+type.getSimpleName()+".class.getName(), <your class that extends it>);\nto your code");
		return ret;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#write(java.lang.Object, java.io.Writer)
	 */
	@Override
	public void write(Object data, Writer out) throws SerializationException, IOException {
		Document doc = marshaller.writeToDocument(data);

		PrintWriter pOut = new PrintWriter(out);
		pOut.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		marshaller.write(doc.getRoot(), 0, pOut);
		pOut.close();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#write(java.lang.Object, java.io.OutputStream)
	 */
	@Override
	public void write(Object data, OutputStream out) throws SerializationException, IOException {
		Document doc = marshaller.writeToDocument(data);

		PrintWriter pOut = new PrintWriter(new OutputStreamWriter(out));
		pOut.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		marshaller.write(doc.getRoot(), 0, pOut);
		if (out!=System.out)
			pOut.close();
		else
			pOut.flush();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#write(java.lang.Object, java.io.File)
	 */
	@Override
	public void write(Object data, File file) throws SerializationException, IOException {
		Document doc = marshaller.writeToDocument(data);

		PrintWriter pOut = new PrintWriter(new FileWriter(file));
		pOut.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>");
		marshaller.write(doc.getRoot(), 0, pOut);
		pOut.close();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#read(java.lang.Class, java.lang.String)
	 */
	@Override
	public <T> T read(Class<T> cls, String xml) throws SerializationException, IOException {
		errorFound=false;
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
			inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
			XMLEventReader  evRd = inputFactory.createXMLEventReader( new StringReader( xml ) );

			return unmarshaller.read(cls, evRd);
		} catch (FactoryConfigurationError e) {
			throw new SerializationException(e);
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#read(java.lang.Class, java.io.Reader)
	 */
	@Override
	public <T> T read(Class<T> cls, Reader xml) throws SerializationException, IOException {
		errorFound=false;
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
			inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
			XMLEventReader  evRd = inputFactory.createXMLEventReader( xml );

			return unmarshaller.read(cls, evRd);
		} catch (FactoryConfigurationError e) {
			throw new SerializationException(e);
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#read(java.lang.Class, java.io.InputStream)
	 */
	@Override
	public <T> T read(Class<T> cls, InputStream xml) throws SerializationException, IOException {
		if (xml==null)
			throw new NullPointerException("InputStream is null");
		errorFound=false;
		try {
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
			inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
			XMLEventReader  evRd = inputFactory.createXMLEventReader( xml );

			return unmarshaller.read(cls, evRd);
		} catch (FactoryConfigurationError e) {
			throw new SerializationException(e);
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Serializer#read(java.lang.Class, javax.xml.transform.Source)
	 */
	@Override
	public <T> T read(Class<T> cls, Source xml) throws SerializationException, IOException {
		try {
			errorFound=false;
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			inputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
			inputFactory.setProperty(XMLInputFactory.IS_REPLACING_ENTITY_REFERENCES, false);
			XMLEventReader  evRd = inputFactory.createXMLEventReader( xml );

			return unmarshaller.read(cls, evRd);
		} catch (FactoryConfigurationError e) {
			throw new SerializationException(e);
		} catch (XMLStreamException e) {
			throw new SerializationException(e);
		}
	}

}
